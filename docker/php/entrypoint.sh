#!/usr/bin/env sh
#set -e

HOST_IP=$(/sbin/ip route|awk '/default/ { print $3 }')
USER=www-data
usermod -u 1000 $USER
groupmod -g 1000 $USER

touch /var/www/.bashrc

# PRINT ENV VARS WHICH CAN BE USED IN CRON PROCESS
printenv | sed 's/^\([^=]*\)=\(.*\)$/export \1=\"\2\"/g' > /var/www/.project_env.sh

sed -i "s|xdebug.remote_host.*|xdebug.remote_host=$HOST_IP|" /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
echo export PHP_IDE_CONFIG="serverName=scrapper.localhost" >> /var/www/.bashrc
echo export PHP_IDE_CONFIG="serverName=scrapper.localhost" >> /var/www/.project_env.sh

make build

chown "$USER":"$USER" /var/www -R
chown "$USER":"$USER" /tmp -R
chown "$USER":"$USER" /usr/src/app -R
chmod 0777 -R /usr/src/app/var

chown "$USER":"$USER" /usr/local/composer/cache -R
chmod 0777 -R /usr/local/composer/cache

chown -R $USER ~/.composer/

exec "$@"
