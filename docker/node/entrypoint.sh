#!/usr/bin/env sh
#set -e

yarn install

if [ "$NODE_ENV" = "dev" ]; then
  yarn watch;
else
  yarn build;
fi
