## **SCRAPPER**

**Scrapper on-line:** [www.httpscrapper.com](http://httpscrapper.com/)

#### **App description:**

My scrapper, based on the url or file path, as well as a regular expression, obtains the titles of articles from news services such as BBC or The Times. 

The titles of articles are downloaded every hour using cron, which allowed to accumulate over 160 thousand of them in less than a year.

#### **Technology used:**

**PHP 7.4, Symfony 4.4, mysql, Docker Compose, git, GitLab CI**

Along with the development of the project, other important elements were added:
* data validation, 
* [error handling](https://gitlab.com/msmoczynska/scrapper/-/blob/master/app/src/EventSubscriber/ExceptionSubscriber.php) (**EventSubscriber**, throwing exceptions), 
* security: registration, login, password change, [sending e-mails](https://gitlab.com/msmoczynska/scrapper/-/blob/master/app/src/SecurityService/SecurityMailer.php) (using **Swift Mailer**) to the user to confirm registration or reset a forgotten password, etc, 
* **[dockerization](https://gitlab.com/msmoczynska/scrapper/-/blob/master/docker-compose.yml.dist)** and configuration my best best friend - **Xdebug**, 
* pipeline (**[gitlabCI](https://gitlab.com/msmoczynska/scrapper/-/blob/master/.gitlab-ci.yml)**) and automatic deploy (to which the manual option was added later for security), 
* migrations - including changing the usual [auto-incremented id to **Ramsey UUID**](https://gitlab.com/msmoczynska/scrapper/-/blob/master/app/src/DoctrineMigrations/Version20210407123239.php), 
* **Messenger** (which allows, among others, to correctly create objects from the forms data and open the way for the implementation of **CQRS**) 
* **PhpSpreadsheet** for [generate xlsx or csv export file](https://gitlab.com/msmoczynska/scrapper/-/tree/master/app/src/ExportGenerator) 
* and finally [tests](https://gitlab.com/msmoczynska/scrapper/-/tree/master/app/tests) - unit, integration and functional with the use of **PHPUnit**.

##### Symfony Components used: 
* Console
* DependencyInjection
* EventDispatcher
* HttpFoundation
* Messenger
* PHPUnit
* Security
* Serializer
* Webpack Encore

##### Consciously used design patterns:
* Creational: [Factory](https://gitlab.com/msmoczynska/scrapper/-/blob/master/app/src/Factory/CreateSourceFactory.php)
* Structural: [Decorator](https://gitlab.com/msmoczynska/scrapper/-/blob/master/app/src/Migrations/Factory/MigrationFactoryDecorator.php), Dependency Injection
* Behavioral: [Strategy](https://gitlab.com/msmoczynska/scrapper/-/tree/master/app/src/Scrapper)
* Other: [Repository](https://gitlab.com/msmoczynska/scrapper/-/tree/master/app/src/Repository)

#### **How it works:**

* The titles of articles are extracted from the website code by means of the **[CLI command](https://gitlab.com/msmoczynska/scrapper/-/blob/master/app/src/Command/GetAllNewsCommand.php)**, run with the help of **[CRON](https://gitlab.com/msmoczynska/scrapper/-/blob/master/docker/php/conf.d/crontab)**, automatically every full hour for each information service that is in the database. 
* The use of a **[logger](https://gitlab.com/msmoczynska/scrapper/-/blob/master/app/src/Service/NewsUpdater.php)** in this process allows to obtain information (at individual stages) about how many "news" have been downloaded for a given website, that they have been saved in the database, etc.
* The possibility of obtaining "news" based on the url address or the path to the html file required the use of a **strategy pattern**. A provider has been created that returns an appropriate scrapper based on the source type (url/filepath).
* [Displaying the titles of "news"](https://gitlab.com/msmoczynska/scrapper/-/blob/master/app/assets/js/newsList.js) is handled by **javascript**, with the help of which **[pagination](https://gitlab.com/msmoczynska/scrapper/-/blob/master/app/assets/js/utils/pagination.js)** is also done, because more then 160 thousands titles in the database had to be broken down into smaller batches. 
* The controllers were divided into those responsible for [synchronous and asynchronous actions](https://gitlab.com/msmoczynska/scrapper/-/tree/master/app/src/Controller). The result is a **monolithic application with its own internal API** to which asynchronous queries are directed.

**Author:** Marta Smoczyńska  
**e-mail:** [kontakt@martasmoczynska.pl](mailto:kontakt@martasmoczynska.pl)  
**LinkedIn:** [Marta Smoczyńska](https://www.linkedin.com/in/martasmoczynska/)
