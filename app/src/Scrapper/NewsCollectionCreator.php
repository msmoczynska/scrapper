<?php
declare(strict_types=1);

namespace App\Scrapper;

use App\Entity\News;
use App\Entity\Section;
use App\Entity\Source;
use App\EntityCollection\NewsCollection;
use App\Exception\MissingScrapperException;
use DateTime;
use Psr\Log\LoggerInterface;

class NewsCollectionCreator
{
    private ScrapperProvider $scrapperProvider;
    private LoggerInterface $logger;

    /**
     * NewsCollectionCreator constructor.
     * @param ScrapperProvider $scrapperProvider
     * @param LoggerInterface $cronLogger
     */
    public function __construct(ScrapperProvider $scrapperProvider, LoggerInterface $cronLogger)
    {
        $this->scrapperProvider = $scrapperProvider;
        $this->logger = $cronLogger;
    }

    /**
     * @param Source $source
     * @return NewsCollection
     * @throws MissingScrapperException
     */
    public function create(Source $source): NewsCollection
    {
        $scrapper = $this->scrapperProvider->provideScrapper($source);

        $sourceNewsCollection = [];

        /** @var Section $section */
        foreach ($source->getSections() as $section) {
            $this->logger->notice("Starting for the section [id: {$section->getId()}] \"{$section->getName()}\" in source [id: {$source->getId()}] \"{$source->getTitle()}\".");
            
            $recentNewsCollection = $scrapper->getNews($source->getPath(), $section);
            $this->logger->notice("{$recentNewsCollection->count()} news found for [id: {$section->getId()}] \"{$section->getName()}\" section.");
            $this->notifyAboutRecentNews($recentNewsCollection);

            $this->markOutdatedNews($section, $recentNewsCollection);
            $this->logger->notice("{$recentNewsCollection->getOutdatedNews()->count()} news marked as outdated.");

            $section->appendNews($recentNewsCollection);
            $this->logger->notice("Recent news added to the [id: {$section->getId()}] \"{$section->getName()}\" section.");

            $this->countNews($section);

            $sourceNewsCollection = array_merge(
                $sourceNewsCollection,
                $section->getNews()->toArray()
            );
            $this->logger->notice("News from [id: {$section->getId()}] \"{$section->getName()}\" section added to the [id: {$source->getId()}] \"{$source->getTitle()}\" source.");
            $this->logger->notice("The [id: {$source->getId()}] \"{$source->getTitle()}\" Source News Collection counts now " . count($sourceNewsCollection) . " news items.");
        }

        return new NewsCollection($sourceNewsCollection);
    }

    /**
     * @param Section $section
     * @param NewsCollection $recentNewsCollection
     */
    public function markOutdatedNews(Section $section, NewsCollection $recentNewsCollection): void
    {
        /** @var News $news */
        foreach ($section->getEnabledNews() as $news) {
            if (!$recentNewsCollection->contains($news)) {
                $news
                    ->setEnabled(false)
                    ->setUpdatedAt(new DateTime());

                $this->logger->notice("News \"{$news->getTitle()}\" marked as outdated.");
            }
        }
    }

    /**
     * @param Section $section
     */
    public function countNews(Section $section): void
    {
        $sectionNewsCollection = new NewsCollection($section->getNews()->toArray());

        $section->setRecentNewsCount($sectionNewsCollection->getRecentNews()->count());
        $section->setActiveNewsCount($sectionNewsCollection->getActiveNews()->count());
        $section->setOutdatedNewsCount($sectionNewsCollection->getOutdatedNews()->count());
        $section->setAllNewsCount($sectionNewsCollection->count());
    }

    /**
     * @param NewsCollection $recentNewsCollection
     */
    private function notifyAboutRecentNews(NewsCollection $recentNewsCollection): void
    {
        /** @var News $recentNews */
        foreach ($recentNewsCollection as $recentNews) {
            $newsIdentifier = $recentNews->getId() ?? "none";
            $this->logger->notice("News [id: {$newsIdentifier}] - \"{$recentNews->getTitle()}\" found.");
        }
    }
}
