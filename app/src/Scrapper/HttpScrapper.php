<?php
declare(strict_types=1);

namespace App\Scrapper;

use App\Entity\Source;

class HttpScrapper extends AbstractScrapper implements ScrapperInterface
{
    /**
     * @param Source $source
     * @return bool
     */
    public function support(Source $source): bool
    {
        return Source::TYPE_HTTP === $source->getType();
    }
}
