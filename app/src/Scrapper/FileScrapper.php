<?php
declare(strict_types=1);

namespace App\Scrapper;

use App\Entity\Source;

class FileScrapper extends AbstractScrapper implements ScrapperInterface
{
    /**
     * @param Source $source
     * @return bool
     */
    public function support(Source $source): bool
    {
        return Source::TYPE_FILE === $source->getType();
    }
}
