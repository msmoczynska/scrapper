<?php
declare(strict_types=1);

namespace App\Scrapper;

use App\Entity\Source;
use App\Exception\MissingScrapperException;

class ScrapperProvider
{
    private iterable $scrappers;

    public function __construct(iterable $scrappers)
    {
        $this->scrappers = $scrappers;
    }

    /**
     * @param Source $source
     * @return ScrapperInterface
     * @throws MissingScrapperException
     */
    public function provideScrapper(Source $source): ScrapperInterface
    {
        /** @var ScrapperInterface $scrapper */
        foreach ($this->scrappers as $scrapper) {
            if ($scrapper->support($source)) {
                return $scrapper;
            }
        }

        throw new MissingScrapperException();
    }
}
