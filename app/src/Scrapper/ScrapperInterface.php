<?php
declare(strict_types=1);

namespace App\Scrapper;

use App\Entity\Section;
use App\Entity\Source;
use App\EntityCollection\NewsCollection;

interface ScrapperInterface
{
    /**
     * @param string $path
     * @param Section $section
     * @return NewsCollection
     */
    public function getNews(string $path, Section $section): NewsCollection;

    /**
     * @param Source $source
     * @return bool
     */
    public function support(Source $source): bool;
}
