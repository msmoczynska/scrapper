<?php
declare(strict_types=1);

namespace App\Scrapper;

use App\Entity\News;
use App\Entity\Section;
use App\EntityCollection\NewsCollection;
use App\Exception\WrongRegexException;
use App\Repository\NewsRepository;
use App\Resolver\RegexResolver;
use App\Resolver\RegexResults;
use App\Resolver\RegexResultsCleaner;
use App\Service\ContentFetcherInterface;
use DateTime;
use Ramsey\Uuid\Uuid;

abstract class AbstractScrapper implements ScrapperInterface
{
    private ContentFetcherInterface $contentFetcher;
    private RegexResolver $regexResolver;
    private RegexResultsCleaner $regexResultsCleaner;
    private NewsRepository $newsRepository;

    /**
     * @param ContentFetcherInterface $contentFetcher
     * @param RegexResolver $regexResolver
     * @param RegexResultsCleaner $regexResultsCleaner
     * @param NewsRepository $newsRepository
     */
    public function __construct(
        ContentFetcherInterface $contentFetcher,
        RegexResolver $regexResolver,
        RegexResultsCleaner $regexResultsCleaner,
        NewsRepository $newsRepository
    )
    {
        $this->contentFetcher = $contentFetcher;
        $this->regexResolver = $regexResolver;
        $this->regexResultsCleaner = $regexResultsCleaner;
        $this->newsRepository = $newsRepository;
    }

    /**
     * @param string $path
     * @param Section $section
     * @return NewsCollection
     * @throws WrongRegexException
     */
    public function getNews(string $path, Section $section): NewsCollection
    {
        $sourceContent = $this->contentFetcher->fetch($path);

        $results = $this->regexResolver->resolve($section->getRegex(), $sourceContent);
        $cleanedResults = $this->regexResultsCleaner->clean($results);

        return $this->prepareNewsCollection($cleanedResults, $section);
    }

    /**
     * @param RegexResults $results
     * @param Section $section
     * @return NewsCollection
     */
    private function prepareNewsCollection(RegexResults $results, Section $section): NewsCollection
    {
        $newsCollection = new NewsCollection();

        foreach ($results->getUniqueResults() as $title) {
            /** @var News $news */
            $news = $this->newsRepository->findOneBy(['title' => $title, 'section' => $section]);

            if (null === $news) {
                $news = new News(Uuid::uuid4(), $title, $section);
            } else {
                $news->setUpdatedAt(new DateTime());
            }

            $newsCollection->add($news);
        }

        return $newsCollection;
    }
}
