<?php
declare(strict_types=1);

namespace App\Controller\Web;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class MenuController extends AbstractController
{
    /**
     * @return Response
     */
    public function renderMenuView(): Response
    {
        return $this->render("web/menu/menu.html.twig");
    }
}
