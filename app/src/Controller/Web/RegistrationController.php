<?php
declare(strict_types=1);

namespace App\Controller\Web;

use App\Message\User\ConfirmUser;
use App\Repository\UserRepository;
use App\SecurityService\AuthenticationService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\HandleTrait;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;

class RegistrationController extends AbstractController
{
    use HandleTrait;

    private UserRepository $userRepository;

    /**
     * RegistrationController constructor.
     * @param UserRepository $userRepository
     * @param MessageBusInterface $messageBus
     */
    public function __construct(UserRepository $userRepository, MessageBusInterface $messageBus)
    {
        $this->userRepository = $userRepository;
        $this->messageBus = $messageBus;
    }

    /**
     * @Route("/register", name="register")
     * @return Response
     */
    public function renderRegistrationView(): Response
    {
        return $this->render('web/registration/register.html.twig');
    }

    /**
     * @Route("/confirm-registration/{token}", name="confirm_registration")
     * @param string $token
     * @param AuthenticationService $authenticationService
     * @return Response
     */
    public function renderConfirmRegistrationView(string $token, AuthenticationService $authenticationService): Response
    {
        $user = $this->handle(ConfirmUser::create($token));

        $authenticationService->logInUser($user);

        return $this->render('web/registration/confirmRegistration.html.twig');
    }
}
