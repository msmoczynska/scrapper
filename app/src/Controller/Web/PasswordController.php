<?php
declare(strict_types=1);

namespace App\Controller\Web;

use App\Exception\ResetPasswordTokenNotFoundException;
use App\Repository\UserRepository;
use App\SecurityService\AuthenticationService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Enum\Roles;

class PasswordController extends AbstractController
{
    private UserRepository $userRepository;

    /**
     * PasswordController constructor.
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @Route("/forgot-password", name="forgot_password")
     * @return Response
     */
    public function renderForgotPasswordView(): Response
    {
        return $this->render('web/password/forgotPassword.html.twig');
    }

    /**
     * @Route("/reset-password/{token}", name="reset_password")
     * @param string $token
     * @param AuthenticationService $authenticationService
     * @return Response
     * @throws ResetPasswordTokenNotFoundException
     */
    public function renderResetPasswordViewForUser(string $token, AuthenticationService $authenticationService): Response
    {
        $user = $this->userRepository->findOneBy(['resetPasswordToken' => $token]);

        if (null === $user) {
            throw new ResetPasswordTokenNotFoundException();
        }

        $authenticationService->logInUser($user);

        return $this->render('web/password/resetPassword.html.twig');
    }

    /**
     * @Route("/change-password", name="change_password")
     * @IsGranted(Roles::ROLE_USER)
     *
     * @return Response
     */
    public function renderChangePasswordViewForUser(): Response
    {
        return $this->render('web/password/changePassword.html.twig');
    }
}
