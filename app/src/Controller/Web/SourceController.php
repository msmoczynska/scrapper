<?php
declare(strict_types=1);

namespace App\Controller\Web;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SourceController extends AbstractController
{

    /**
     * @Route("/add-new-source", name="add_new_source")
     *
     * @return Response
     */
    public function addNewSource(): Response
    {
        return $this->render("web/source/addNewSource.html.twig");
    }

    /**
     * @Route("/add-new-http", name="add_new_http")
     *
     * @return Response
     */
    public function createFromHttp(): Response
    {
        return $this->render("web/source/addHttp.html.twig");
    }

    /**
     * @Route("/add-new-file", name="add_new_file")
     *
     * @return Response
     */
    public function createFromFile(): Response
    {
        return $this->render("web/source/addFile.html.twig");
    }

    /**
     * @Route("/source", name="source_list")
     *
     * @return Response
     */
    public function list(): Response
    {
        return $this->render("web/source/list.html.twig");
    }
}
