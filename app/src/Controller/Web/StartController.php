<?php
declare(strict_types=1);

namespace App\Controller\Web;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Enum\Roles;

class StartController extends AbstractController
{
    /**
     * @Route("/", name="scrapper_index")
     *
     * @return Response
     */
    public function homePage(): Response
    {
//        TODO: correct the names and structure of templates from login and auth directories
        return $this->render("web/start/index.html.twig");
    }

    /**
     * @Route("/after-login", name="after_login")
     * @IsGranted(Roles::ROLE_USER)
     *
     * @return Response
     */
    public function afterLogin(): Response
    {
        return $this->render("web/start/afterLogin.html.twig");
    }
}
