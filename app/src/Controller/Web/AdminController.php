<?php
declare(strict_types=1);

namespace App\Controller\Web;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Enum\Roles;

class AdminController extends AbstractController
{
    /**
     * @Route("/admin-panel", name="admin_panel")
     * @IsGranted(Roles::ROLE_ADMIN)
     *
     * @return Response
     */
    public function adminPanel(): Response
    {
        return $this->render("web/admin/adminPanel.html.twig");
    }
}
