<?php
declare(strict_types=1);

namespace App\Controller\Api;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class AuthController extends AbstractController
{
    /**
     * @Route("/api/login", name="api_login", methods={"POST"})
     * @return JsonResponse
     */
    public function login(): JsonResponse
    {
        return new JsonResponse(['message' => 'You are successfully logged in!'], JsonResponse::HTTP_OK);
    }
}
