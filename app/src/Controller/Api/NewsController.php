<?php
declare(strict_types=1);

namespace App\Controller\Api;

use App\Pagination\NewsPaginator;
use App\Pagination\PaginationParams;
use Doctrine\ORM\NonUniqueResultException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class NewsController extends AbstractController
{
    /**
     * @Route("/api/news", name="api_news_list", methods={"GET"})
     * @ParamConverter("pagination_params")
     *
     * @param PaginationParams $paginationParams
     * @param NewsPaginator $newsPaginator
     * @return Response
     * @throws NonUniqueResultException
     */
    public function list(PaginationParams $paginationParams, NewsPaginator $newsPaginator): Response
    {
        return new JsonResponse($newsPaginator->getPaginatedList($paginationParams));
    }
}
