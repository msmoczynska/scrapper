<?php
declare(strict_types=1);

namespace App\Controller\Api;

use App\Message\Export\GenerateExportFile;
use PhpOffice\PhpSpreadsheet\Shared\File;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Messenger\HandleTrait;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;

class ExportController extends AbstractController
{
    use HandleTrait;

    /**
     * ExportController constructor.
     * @param MessageBusInterface $messageBus
     */
    public function __construct(MessageBusInterface $messageBus)
    {
        $this->messageBus = $messageBus;
    }

    /**
     * @Route("/source/{format}/{type}", name="source_export", methods={"GET"})
     * @ParamConverter("message")
     *
     * @param GenerateExportFile $message
     * @return Response
     */
    public function exportSourceList(GenerateExportFile $message): Response
    {
        $filename = $this->handle($message);

        $response = new BinaryFileResponse(File::sysGetTempDir() . '/' . $filename);

        return $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, $filename);
    }
}
