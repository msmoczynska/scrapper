<?php
declare(strict_types=1);

namespace App\Controller\Api;

use App\Exception\FileNotFoundException;
use App\Exception\FileValidationException;
use App\Factory\CreateSourceFactory;
use App\Pagination\PaginationParams;
use App\Pagination\SourcePaginator;
use Doctrine\ORM\NonUniqueResultException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;
use App\Enum\Roles;

class SourceController extends AbstractController
{
    private CreateSourceFactory $createSourceFactory;
    private MessageBusInterface $messageBus;

    /**
     * SourceController constructor.
     * @param CreateSourceFactory $createSourceFactory
     * @param MessageBusInterface $messageBus
     */
    public function __construct(CreateSourceFactory $createSourceFactory, MessageBusInterface $messageBus)
    {
        $this->createSourceFactory = $createSourceFactory;
        $this->messageBus = $messageBus;
    }

    /**
     * @Route("/api/add-new-http", name="api_add_new_http", methods={"POST"})
     * @IsGranted(Roles::ROLE_ADMIN)
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function createFromHttp(Request $request): JsonResponse
    {
        $message = $this->createSourceFactory->createFromHttp($request);

        $this->messageBus->dispatch($message);

        return new JsonResponse(['message' => 'Source created'], JsonResponse::HTTP_CREATED);
    }

    /**
     * @Route("/api/add-new-file", name="api_add_new_file", methods={"POST"})
     * @IsGranted(Roles::ROLE_ADMIN)
     *
     * @param Request $request
     * @return JsonResponse
     * @throws FileNotFoundException
     * @throws FileValidationException
     */
    public function createFromFile(Request $request): JsonResponse
    {
        $message = $this->createSourceFactory->createFromFile($request);

        $this->messageBus->dispatch($message);

        return new JsonResponse(['message' => 'Source created'], JsonResponse::HTTP_CREATED);
    }

    /**
     * /**
     * @Route("/api/source", name="api_source_list", methods={"GET"})
     * @ParamConverter("pagination_params")
     *
     * @param PaginationParams $paginationParams
     * @param SourcePaginator $sourcePaginator
     * @return Response
     * @throws NonUniqueResultException
     */
    public function list(PaginationParams $paginationParams, SourcePaginator $sourcePaginator): Response
    {
        return new JsonResponse($sourcePaginator->getPaginatedList($paginationParams));
    }
}
