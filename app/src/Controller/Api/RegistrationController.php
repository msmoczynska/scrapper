<?php
declare(strict_types=1);

namespace App\Controller\Api;

use App\Message\User\CreateUser;
use Ramsey\Uuid\Uuid;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;

class RegistrationController extends AbstractController
{
    /**
     * @Route("/api/register", name="api_register", methods={"POST"})
     * @ParamConverter("message", class="App\Message\User\CreateUser")
     *
     * @param CreateUser $message
     * @param MessageBusInterface $messageBus
     * @return JsonResponse
     */
    public function register(CreateUser $message, MessageBusInterface $messageBus): JsonResponse
    {
        $message->id = Uuid::uuid4();

        $messageBus->dispatch($message);

        return new JsonResponse(['message' => 'User created'], JsonResponse::HTTP_CREATED);
    }
}
