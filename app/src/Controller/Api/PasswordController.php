<?php
declare(strict_types=1);

namespace App\Controller\Api;

use App\Message\User\ChangePassword;
use App\Message\User\ResetPassword;
use App\Message\User\ResetPasswordRequest;
use App\Repository\UserRepository;
use App\SecurityService\SecurityMailer;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class PasswordController extends AbstractController
{
    private UserRepository $userRepository;
    private SecurityMailer $securityMailer;
    private UserPasswordEncoderInterface $passwordEncoder;
    private MessageBusInterface $messageBus;

    /**
     * PasswordController constructor.
     * @param UserRepository $userRepository
     * @param SecurityMailer $securityMailer
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param MessageBusInterface $messageBus
     */
    public function __construct(
        UserRepository $userRepository,
        SecurityMailer $securityMailer,
        UserPasswordEncoderInterface $passwordEncoder,
        MessageBusInterface $messageBus
    )
    {
        $this->userRepository = $userRepository;
        $this->securityMailer = $securityMailer;
        $this->passwordEncoder = $passwordEncoder;
        $this->messageBus = $messageBus;
    }

    /**
     * @Route("/api/forgot-password/{email}", name="api_forgot_password", methods={"GET"})
     * @param ResetPasswordRequest $message
     * @return JsonResponse
     */
    public function sendResetPasswordEmail(ResetPasswordRequest $message): JsonResponse
    {
        $this->messageBus->dispatch($message);

        return new JsonResponse(['message' => 'Check your e-mail box.'], JsonResponse::HTTP_OK);
    }

    /**
     * @Route("/api/reset-password", name="api_reset_password", methods={"POST"})
     * @ParamConverter("message", class="App\Message\User\ResetPassword")
     *
     * @param ResetPassword $message
     * @param UserInterface $user
     * @return JsonResponse
     */
    public function resetPassword(ResetPassword $message, UserInterface $user): JsonResponse
    {
        $this->messageBus->dispatch($message->setUser($user));

        return new JsonResponse(['message' => 'New password set.'], JsonResponse::HTTP_OK);
    }

    /**
     * @Route("/api/change-password", name="api_change_password", methods={"POST"})
     * @ParamConverter("message", class="App\Message\User\ChangePassword")
     *
     * @param ChangePassword $message
     * @param UserInterface $user
     * @return JsonResponse
     */
    public function changePassword(ChangePassword $message, UserInterface $user): JsonResponse
    {
        $this->messageBus->dispatch($message->setUser($user));

        return new JsonResponse(['message' => 'Password changed successfully.'], JsonResponse::HTTP_OK);
    }
}
