<?php
declare(strict_types=1);

namespace App\ParamConverter;

use App\Pagination\PaginationParams;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;

class PaginationParamConverter implements ParamConverterInterface
{
    private const ATTRIBUTE_NAME = "paginationParams";
    private const SUPPORTED_NAME = "pagination_params";

    /**
     * @param Request $request
     * @param ParamConverter $configuration
     * @return bool
     */
    public function apply(Request $request, ParamConverter $configuration): bool
    {
        $limit = $request->query->getInt('limit', PaginationParams::DEFAULT_LIMIT);
        $page = $request->query->getInt('page', PaginationParams::DEFAULT_PAGE);
        $searchPhrase = $request->query->get('search');

        $paginationParams = new PaginationParams($limit, $page);

        if (null !== $searchPhrase) {
            $paginationParams->setSearchPhrase($searchPhrase);
        }

        $request->attributes->set(self::ATTRIBUTE_NAME, $paginationParams);

        return true;
    }

    /**
     * @param ParamConverter $configuration
     * @return bool
     */
    public function supports(ParamConverter $configuration): bool
    {
        return self::SUPPORTED_NAME === $configuration->getName();
    }
}
