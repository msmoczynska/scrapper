<?php
declare(strict_types=1);

namespace App\ParamConverter;

use App\Message\MessageInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class RequestToMessageParamConverter implements ParamConverterInterface
{
    /**
     * @param Request $request
     * @param ParamConverter $configuration
     * @return bool
     * @throws ExceptionInterface
     */
    public function apply(Request $request, ParamConverter $configuration): bool
    {
        $normalizer = new ObjectNormalizer();
        $post = $request->request->all();

        $message = $normalizer->denormalize($post, $configuration->getClass());

        $this->attendDataFromAttributes($request, $message);
        $request->attributes->set($configuration->getName(), $message);

        return true;
    }

    /**
     * @param ParamConverter $configuration
     * @return bool
     */
    public function supports(ParamConverter $configuration): bool
    {
        return $configuration->getClass() && in_array(MessageInterface::class, class_implements($configuration->getClass()));
    }

    /**
     * @param Request $request
     * @param $message
     */
    public function attendDataFromAttributes(Request $request, $message): void
    {
        foreach ($request->attributes->all() as $key => $value){
            if (property_exists($message, $key)) {
                $message->{$key} = $value;
            }
        }
    }
}
