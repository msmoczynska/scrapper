<?php
declare(strict_types=1);

namespace App\Message\User;

use App\Message\MessageInterface;

class ConfirmUser implements MessageInterface
{
    public string $confirmationToken;

    /**
     * @param $confirmationToken
     * @return ConfirmUser
     */
    public static function create($confirmationToken): self
    {
        $confirmUser = new self();
        $confirmUser->confirmationToken = $confirmationToken;

        return $confirmUser;
    }
}
