<?php
declare(strict_types=1);

namespace App\Message\User;

use Ramsey\Uuid\UuidInterface;

interface UserAwareInterface
{
    /**
     * @return UuidInterface
     */
    public function getUserId(): UuidInterface;
}
