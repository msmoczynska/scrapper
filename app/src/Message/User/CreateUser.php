<?php
declare(strict_types=1);

namespace App\Message\User;

use App\Message\MessageInterface;
use Symfony\Component\Validator\Constraints as Assert;

class CreateUser implements MessageInterface
{
    /**
     * @Assert\Type("Ramsey\Uuid\UuidInterface")
     */
    public $id;

    /**
     * @Assert\NotBlank()
     * @Assert\Email()
     * @Assert\Type("string")
     * @Assert\Length(max="50")
     */
    public $email;

    /**
     * @Assert\NotBlank()
     * @Assert\Type("string")
     * @Assert\Length(
     *     min=8,
     *     minMessage="The password must be at least 8 characters long"
     * )
     */
    public $password;

    /**
     * @Assert\NotBlank()
     * @Assert\Type("string")
     * @Assert\Length(max="50")
     */
    public $firstName;

    /**
     * @Assert\NotBlank()
     * @Assert\Type("string")
     * @Assert\Length(max="50")
     */
    public $secondName;

    /**
     * @Assert\NotBlank()
     * @Assert\Date()
     * @Assert\Type("string")
     * @Assert\Length(max="10")
     */
    public $birthDate;

    /**
     * @param $email
     * @param $password
     * @param $firstName
     * @param $secondName
     * @param $birthDate
     * @return static
     */
    public static function create($email, $password, $firstName, $secondName, $birthDate): self
    {
        $createUser = new self();
        $createUser->email = $email;
        $createUser->password = $password;
        $createUser->firstName = $firstName;
        $createUser->secondName = $secondName;
        $createUser->birthDate = $birthDate;

        return $createUser;
    }
}
