<?php
declare(strict_types=1);

namespace App\Message\User;

use App\Entity\User;
use App\Message\MessageInterface;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

class ResetPassword implements UserAwareInterface, MessageInterface
{
    /**
     * @Assert\NotBlank()
     * @Assert\Type("string")
     * @Assert\Length(
     *     min=8,
     *     minMessage="The password must be at least 8 characters long"
     * )
     */
    public $newPassword;

    /**
     * @Assert\NotBlank()
     * @Assert\Type("string")
     * @Assert\Length(
     *     min=8,
     *     minMessage="The password must be at least 8 characters long"
     * )
     * @Assert\IdenticalTo(propertyPath = "newPassword", message="Passwords must be identical!")
     */
    public $confirmNewPassword;

    /**
     * @Assert\Type("Ramsey\Uuid\UuidInterface")
     */
    public $userId;

    /**
     * @param $newPassword
     * @param $confirmNewPassword
     * @return static
     */
    public static function create($newPassword, $confirmNewPassword): self
    {
        $resetPassword = new self();
        $resetPassword->newPassword = $newPassword;
        $resetPassword->confirmNewPassword = $confirmNewPassword;

        return $resetPassword;
    }

    /**
     * @param UserInterface $user
     * @return static
     */
    public function setUser(UserInterface $user): self
    {
        /** @var User $user */
        $this->userId = $user->getId();

        return $this;
    }

    /**
     * @return UuidInterface
     */
    public function getUserId(): UuidInterface
    {
        return $this->userId;
    }
}
