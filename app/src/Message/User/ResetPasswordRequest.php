<?php
declare(strict_types=1);

namespace App\Message\User;

use App\Message\MessageInterface;
use Symfony\Component\Validator\Constraints as Assert;

class ResetPasswordRequest implements MessageInterface
{
    /**
     * @Assert\NotBlank()
     * @Assert\Email()
     * @Assert\Type("string")
     * @Assert\Length(max="50")
     */
    public $email;

    public static function create($email): self
    {
        $resetPasswordRequest = new self();
        $resetPasswordRequest->email = $email;

        return $resetPasswordRequest;
    }
}
