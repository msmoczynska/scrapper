<?php
declare(strict_types=1);

namespace App\Message\Export;

use App\Message\MessageInterface;
use Symfony\Component\Validator\Constraints as Assert;

class GenerateExportFile implements MessageInterface
{
    /**
     * @Assert\NotBlank()
     * @Assert\Type("string")
     */
    public string $format;

    /**
     * @Assert\NotBlank()
     * @Assert\Type("string")
     */
    public string $type;

    /**
     * @param $format
     * @param $type
     * @return static
     */
    public static function create($format, $type): self
    {
        $exportItemsList = new self();
        $exportItemsList->format = $format;
        $exportItemsList->type = $type;

        return $exportItemsList;
    }
}
