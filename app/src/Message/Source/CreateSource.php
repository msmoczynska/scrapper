<?php
declare(strict_types=1);

namespace App\Message\Source;

use App\Message\MessageInterface;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Validator\Constraints as Assert;

class CreateSource implements MessageInterface
{
    /**
     * @Assert\Type("Ramsey\Uuid\UuidInterface")
     */
    public $id;

    /**
     * @Assert\NotBlank()
     * @Assert\Type("string")
     * @Assert\Length(max="255")
     */
    public $path;

    /**
     * @Assert\NotBlank()
     * @Assert\Type("array")
     */
    public $sections;

    /**
     * @param $path
     * @param $sections
     * @return CreateSource
     */
    public static function create($path, $sections): self
    {
        $createSource = new self();
        $createSource->id = Uuid::uuid4();
        $createSource->path = $path;
        $createSource->sections = $sections;

        return $createSource;
    }
}
