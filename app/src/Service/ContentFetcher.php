<?php
declare(strict_types=1);

namespace App\Service;

class ContentFetcher implements ContentFetcherInterface
{
    /**
     * @param string $path
     * @return string
     */
    public function fetch(string $path): string
    {
        return file_get_contents($path);
    }
}
