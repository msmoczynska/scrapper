<?php
declare(strict_types=1);

namespace App\Service;

class AllSourceStats
{
    private int $recentNewsCount = 0;
    private int $activeNewsCount = 0;
    private int $outdatedNewsCount = 0;
    private int $allNewsCount = 0;

    /**
     * @return int
     */
    public function getRecentNewsCount(): int
    {
        return $this->recentNewsCount;
    }

    /**
     * @param int $recentNewsCount
     * @return AllSourceStats
     */
    public function setRecentNewsCount(int $recentNewsCount): self
    {
        $this->recentNewsCount = $recentNewsCount;

        return $this;
    }

    /**
     * @param int $sourceRecentNewsCount
     * @return void
     */
    public function appendRecentNewsCount(int $sourceRecentNewsCount): void
    {
        $this->recentNewsCount += $sourceRecentNewsCount;
    }

    /**
     * @return int
     */
    public function getActiveNewsCount(): int
    {
        return $this->activeNewsCount;
    }

    /**
     * @param int $activeNewsCount
     * @return AllSourceStats
     */
    public function setActiveNewsCount(int $activeNewsCount): self
    {
        $this->activeNewsCount = $activeNewsCount;

        return $this;
    }

    /**
     * @param int $sourceActiveNewsCount
     * @return void
     */
    public function appendActiveNewsCount(int $sourceActiveNewsCount): void
    {
        $this->activeNewsCount += $sourceActiveNewsCount;
    }

    /**
     * @return int
     */
    public function getOutdatedNewsCount(): int
    {
        return $this->outdatedNewsCount;
    }

    /**
     * @param int $outdatedNewsCount
     * @return AllSourceStats
     */
    public function setOutdatedNewsCount(int $outdatedNewsCount): self
    {
        $this->outdatedNewsCount = $outdatedNewsCount;

        return $this;
    }

    /**
     * @param int $sourceOutdatedNewsCount
     * @return void
     */
    public function appendOutdatedNewsCount(int $sourceOutdatedNewsCount): void
    {
        $this->outdatedNewsCount += $sourceOutdatedNewsCount;
    }

    /**
     * @return int
     */
    public function getAllNewsCount(): int
    {
        return $this->allNewsCount;
    }

    /**
     * @param $allNewsCount
     * @return AllSourceStats
     */
    public function setAllNewsCount($allNewsCount): self
    {
        $this->allNewsCount = $allNewsCount;

        return $this;
    }

    /**
     * @param int $sourceAllNewsCount
     * @return void
     */
    public function appendAllNewsCount(int $sourceAllNewsCount): void
    {
        $this->allNewsCount += $sourceAllNewsCount;
    }
}
