<?php
declare(strict_types=1);

namespace App\Service;

use App\Entity\Section;
use App\Entity\Source;
use App\Exception\SourceAlreadyExistException;
use App\Exception\ValidationException;
use App\Repository\SectionRepository;
use App\Repository\SourceRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class SourceCreator
{
    private SourceTitleGenerator $sourceTitleGenerator;
    private SourceRepository $sourceRepository;
    private SourceValidator $sourceValidator;
    private SectionRepository $sectionRepository;

    /**
     * SourceCreator constructor.
     * @param SourceTitleGenerator $sourceTitleGenerator
     * @param SourceRepository $sourceRepository
     * @param SourceValidator $sourceValidator
     * @param SectionRepository $sectionRepository
     */
    public function __construct(
        SourceTitleGenerator $sourceTitleGenerator,
        SourceRepository $sourceRepository,
        SourceValidator $sourceValidator,
        SectionRepository $sectionRepository
    )
    {
        $this->sourceTitleGenerator = $sourceTitleGenerator;
        $this->sourceRepository = $sourceRepository;
        $this->sourceValidator = $sourceValidator;
        $this->sectionRepository = $sectionRepository;
    }

    /**
     * @param UuidInterface $id
     * @param string $path
     * @param array $sectionsArray
     * @return Source
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws SourceAlreadyExistException
     * @throws ValidationException
     */
    public function createSource(UuidInterface $id, string $path, array $sectionsArray): Source
    {
        $title = $this->sourceTitleGenerator->generateFromFilePath($path);

        if (null !== $this->sourceRepository->findOneBy(['title' => $title])) {
            throw new SourceAlreadyExistException();
        }

        $type = true === is_file($path) ? Source::TYPE_FILE : Source::TYPE_HTTP;

        $source = new Source($id, $path, $type, $title);
        $this->sourceValidator->validateSource($source);

        $this->createSections($sectionsArray, $source);

        $this->sourceRepository->persist($source);
        $this->sourceRepository->save();

        return $source;
    }

    /**
     * @param array $sectionsArray
     * @param Source $source
     */
    public function createSections(array $sectionsArray, Source $source): void
    {
        foreach ($sectionsArray as $name => $regex) {
            /** @var Section $section */
            $section = $this->sectionRepository->findOneBy(['name' => $name, 'source' => $source]);

            if (null === $section) {
                $section = new Section(Uuid::uuid4(), $name, $regex, $source);

                $source->addSection($section);
            }
        }
    }
}
