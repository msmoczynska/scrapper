<?php
declare(strict_types=1);

namespace App\Service;

use App\Entity\Source;
use App\EntityCollection\NewsCollection;
use App\Exception\MissingScrapperException;
use App\Repository\SourceRepository;
use App\Scrapper\NewsCollectionCreator;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Psr\Log\LoggerInterface;

class NewsUpdater
{
    private SourceRepository $sourceRepository;
    private NewsCollectionCreator $newsCollectionCreator;
    private LoggerInterface $logger;

    /**
     * GetAllNewsCommand constructor.
     * @param SourceRepository $sourceRepository
     * @param NewsCollectionCreator $newsCollectionCreator
     * @param LoggerInterface $cronLogger
     */
    public function __construct(
        SourceRepository $sourceRepository,
        NewsCollectionCreator $newsCollectionCreator,
        LoggerInterface $cronLogger
    )
    {
        $this->sourceRepository = $sourceRepository;
        $this->newsCollectionCreator = $newsCollectionCreator;
        $this->logger = $cronLogger;
    }

    /**
     * @return AllSourceStats
     * @throws MissingScrapperException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function update(): AllSourceStats
    {
        $sources = $this->sourceRepository->findAll();
        $this->logger->notice("GetAllNews started for " . count($sources) . " sources.");

        $allSourceStats = new AllSourceStats();

        foreach ($sources as $source) {
            $this->logger->notice("Starting for the source [id: {$source->getId()}] \"{$source->getTitle()}\".");
            $sourceNewsCollection = $this->newsCollectionCreator->create($source);

            $allSourceStats = $this->countNews($sourceNewsCollection, $allSourceStats, $source);
        }

        $this->sourceRepository->save();

        $this->logger->notice("{$allSourceStats->getRecentNewsCount()} news added to the database, {$allSourceStats->getActiveNewsCount()} updated and {$allSourceStats->getOutdatedNewsCount()} marked as outdated");

        return $allSourceStats;
    }

    /**
     * @param NewsCollection $sourceNewsCollection
     * @param AllSourceStats $allSourceStats
     * @param Source $source
     * @return AllSourceStats
     */
    private function countNews(
        NewsCollection $sourceNewsCollection,
        AllSourceStats $allSourceStats,
        Source $source
    ): AllSourceStats
    {
        $sourceRecentNewsCount = $sourceNewsCollection->getRecentNews()->count();
        $sourceActiveNewsCount = $sourceNewsCollection->getActiveNews()->count();
        $sourceOutdatedNewsCount = $sourceNewsCollection->getOutdatedNews()->count();
        $sourceAllNewsCount = $sourceNewsCollection->count();

        $source->setRecentNewsCount($sourceRecentNewsCount);
        $source->setActiveNewsCount($sourceActiveNewsCount);
        $source->setOutdatedNewsCount($sourceOutdatedNewsCount);
        $source->setAllNewsCount($sourceAllNewsCount);

        $this->logger->notice("News for source [id: {$source->getId()}] \"{$source->getTitle()}\" saved in the database.");

        $allSourceStats->appendRecentNewsCount($sourceRecentNewsCount);
        $allSourceStats->appendActiveNewsCount($sourceActiveNewsCount);
        $allSourceStats->appendOutdatedNewsCount($sourceOutdatedNewsCount);
        $allSourceStats->appendAllNewsCount($sourceAllNewsCount);

        return $allSourceStats;
    }
}
