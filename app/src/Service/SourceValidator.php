<?php
declare(strict_types=1);

namespace App\Service;

use App\Entity\Source;
use App\Exception\ValidationException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class SourceValidator
{
    private ValidatorInterface $validator;

    /**
     * SourceValidator constructor.
     * @param ValidatorInterface $validator
     */
    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    /**
     * @param Source $source
     * @throws ValidationException
     */
    public function validateSource(Source $source): void
    {
        $errors = $this->validator->validate($source);

        if (0 < count($errors)) {
            throw new ValidationException($errors);
        }
    }
}
