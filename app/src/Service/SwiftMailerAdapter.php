<?php
declare(strict_types=1);

namespace App\Service;

use Swift_Mailer;
use Swift_Mime_SimpleMessage;

class SwiftMailerAdapter implements MailerInterface
{
    private Swift_Mailer $mailer;

    /**
     * SwiftMailerAdapter constructor.
     * @param Swift_Mailer $mailer
     */
    public function __construct(Swift_Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * @param Swift_Mime_SimpleMessage $message
     * @return int
     */
    public function send(Swift_Mime_SimpleMessage $message): int
    {
        return $this->mailer->send($message);
    }
}
