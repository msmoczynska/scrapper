<?php
declare(strict_types=1);

namespace App\Service;

interface ContentFetcherInterface
{
    /**
     * @param string $path
     * @return string
     */
    public function fetch(string $path): string;
}
