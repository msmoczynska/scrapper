<?php
declare(strict_types=1);

namespace App\Service;

use App\Exception\NoResultsForRegexException;
use App\Exception\WrongRegexException;
use App\Resolver\RegexResolver;
use LogicException;

class SourceTitleGenerator
{
    private const TITLE_REGEX_SET = [
        '/<title.*>(?P<ref>[^"]*)<\/title>/'
    ];

    private RegexResolver $regexResolver;
    private ContentFetcherInterface $contentFetcher;

    /**
     * SourceTitleGenerator constructor.
     * @param RegexResolver $regexResolver
     * @param ContentFetcherInterface $contentFetcher
     */
    public function __construct(RegexResolver $regexResolver, ContentFetcherInterface $contentFetcher)
    {
        $this->regexResolver = $regexResolver;
        $this->contentFetcher = $contentFetcher;
    }

    /**
     * @param string $path
     * @return string
     */
    public function generateFromFilePath(string $path): string
    {
        $content = $this->contentFetcher->fetch($path);

        foreach (self::TITLE_REGEX_SET as $titleRegex) {
            try {
                return $this->fetchTitleByRegex($titleRegex, $content);
            } catch (WrongRegexException | NoResultsForRegexException $exception) {
            }
        }

        throw new LogicException("The application couldn't get the title of the website based on owned regexes");
    }

    /**
     * @param string $titleRegex
     * @param string $content
     * @return string
     * @throws WrongRegexException
     * @throws NoResultsForRegexException
     */
    private function fetchTitleByRegex(string $titleRegex, string $content): string
    {
        $titleResults = $this->regexResolver->resolve($titleRegex, $content);

        if (0 < $titleResults->count()) {
            return $titleResults->first();
        }

        throw new NoResultsForRegexException();
    }
}
