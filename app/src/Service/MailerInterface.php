<?php
declare(strict_types=1);

namespace App\Service;

use Swift_Mime_SimpleMessage;

interface MailerInterface
{
    /**
     * @param Swift_Mime_SimpleMessage $message
     * @return int
     */
//    TODO add abstractions for message
    public function send(Swift_Mime_SimpleMessage $message): int;
}
