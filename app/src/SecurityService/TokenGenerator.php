<?php
declare(strict_types=1);

namespace App\SecurityService;

class TokenGenerator
{
    /**
     * @return string
     */
    public function generate(): string
    {
        return substr(md5(strval(time() + rand(0, 10000))), 0, 20);
    }
}
