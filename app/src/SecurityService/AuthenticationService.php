<?php
declare(strict_types=1);

namespace App\SecurityService;

use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class AuthenticationService
{
    private const PROVIDER_KEY = "main";

    private TokenStorageInterface $tokenStorage;
    private SessionInterface $session;
    private UserRepository $userRepository;

    /**
     * AuthenticationService constructor.
     * @param TokenStorageInterface $tokenStorage
     * @param SessionInterface $session
     * @param UserRepository $userRepository
     */
    public function __construct(TokenStorageInterface $tokenStorage, SessionInterface $session, UserRepository $userRepository)
    {
        $this->tokenStorage = $tokenStorage;
        $this->session = $session;
        $this->userRepository = $userRepository;
    }

    /**
     * @param User $user
     */
    public function logInUser(User $user): void
    {
        $usernamePasswordToken = new UsernamePasswordToken($user, null, self::PROVIDER_KEY, $user->getRoles());
        $this->tokenStorage->setToken($usernamePasswordToken);
        $this->session->set('_security_main', serialize($usernamePasswordToken));
    }
}
