<?php
declare(strict_types=1);

namespace App\SecurityService;

use App\Entity\User;
use App\Service\MailerInterface;
use Swift_Message;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class SecurityMailer
{
    private Environment $templating;
    private RouterInterface $router;
    private MailerInterface $mailer;

    /**
     * SecurityMailer constructor.
     * @param Environment $templating
     * @param RouterInterface $router
     * @param MailerInterface $mailer
     */
    public function __construct(Environment $templating, RouterInterface $router, MailerInterface $mailer)
    {
        $this->templating = $templating;
        $this->router = $router;
        $this->mailer = $mailer;
    }

    /**
     * @param User $user
     * @return int
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function sendConfirmRegistrationEmail(User $user): int
    {
        $message = (new Swift_Message('Hello Email'))
            ->setFrom(['info@serenite.pl' => 'Serenite'])
            ->setTo([$user->getEmail() => $user->getFirstName()])
            ->setBody(
                $this->templating->render(
                    'web/registration/confirmRegistrationEmail.html.twig',
                    [
                        'name' => $user->getFirstName(),
                        'url' => $this->router->generate(
                            'confirm_registration',
                            ['token' => $user->getConfirmationToken()],
                            UrlGeneratorInterface::ABSOLUTE_URL
                        )
                    ]
                ),
                'text/html'
            );

        return $this->mailer->send($message);
    }

    /**
     * @param User $user
     * @return int
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function sendResetPasswordEmail(User $user): int
    {
        $message = (new Swift_Message('Reset password'))
            ->setFrom(['info@serenite.pl' => 'Serenite'])
            ->setTo([$user->getEmail() => $user->getFirstName()])
            ->setBody(
                $this->templating->render(
                    'web/password/resetPasswordEmail.html.twig',
                    [
                        'name' => $user->getFirstName(),
                        'url' => $this->router->generate(
                            'reset_password',
                            ['token' => $user->getResetPasswordToken()],
                            UrlGeneratorInterface::ABSOLUTE_URL)
                    ]),
                'text/html'
            );

        return $this->mailer->send($message);
    }
}
