<?php
declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\News;
use App\Exception\SourceAlreadyExistException;
use App\Exception\ValidationException;
use App\Service\SourceCreator;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ObjectManager;
use Ramsey\Uuid\Uuid;

/**
 * @codeCoverageIgnore
 */
class NewsFixtures extends Fixture implements FixtureGroupInterface
{
    private SourceCreator $sourceCreator;

    /**
     * NewsFixtures constructor.
     * @param SourceCreator $sourceCreator
     */
    public function __construct(SourceCreator $sourceCreator)
    {
        $this->sourceCreator = $sourceCreator;
    }

    /**
     * @param ObjectManager $objectManager
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws ValidationException
     * @throws SourceAlreadyExistException
     */
    public function load(ObjectManager $objectManager): void
    {
        $source = $this->sourceCreator->createSource(
            Uuid::uuid4(),
            "https://globalvoices.org/",
            ["news" => "/rel=\"bookmark\">(?P<ref>[^\"]*)<\/a>/"]
        );

        for ($i = 0; $i < 1000; $i++) {
            $news = new News(Uuid::uuid4(), 'newsTitle' . $i, $source->getSections()[0]);

            $objectManager->persist($news);
        }

        $objectManager->flush();
    }

    /**
     * @return array
     */
    public static function getGroups(): array
    {
        return ['timeTestGroup'];
    }
}
