<?php
declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\User;
use App\Enum\Roles;
use App\SecurityService\TokenGenerator;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{
    private UserPasswordEncoderInterface $passwordEncoder;
    private TokenGenerator $tokenGenerator;

    /**
     * UserFixtures constructor.
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param TokenGenerator $tokenGenerator
     */
    public function __construct(UserPasswordEncoderInterface $passwordEncoder, TokenGenerator $tokenGenerator)
    {
        $this->passwordEncoder = $passwordEncoder;
        $this->tokenGenerator = $tokenGenerator;
    }

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        for ($i = 0; $i < 5; $i++) {
            $user = new User(
                Uuid::uuid4(),
                'email' . $i . '@email.pl',
                'firstname' . $i,
                'secondname' . $i,
                new DateTime(),
                $this->tokenGenerator->generate()
            );

            $user
                ->setPassword($this->passwordEncoder->encodePassword($user,'aqwerty' . $i))
                ->setVerified(true);

            $manager->persist($user);
        }

        $admin = new User(
            Uuid::uuid4(),
            'admin@email.pl',
            'test',
            'admin',
            new DateTime(),
            $this->tokenGenerator->generate()
        );

        $admin
            ->setPassword($this->passwordEncoder->encodePassword($admin, 'qwerty1234'))
            ->setVerified(true)
            ->addRole(Roles::ROLE_ADMIN);

        $manager->persist($admin);
        $manager->flush();
    }
}
