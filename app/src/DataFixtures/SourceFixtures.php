<?php
declare(strict_types=1);

namespace App\DataFixtures;

use App\Exception\MissingScrapperException;
use App\Exception\SourceAlreadyExistException;
use App\Exception\ValidationException;
use App\Repository\SourceRepository;
use App\Scrapper\NewsCollectionCreator;
use App\Service\SourceCreator;
use App\Tests\support\ContentFetcherMock;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ObjectManager;
use Ramsey\Uuid\Uuid;

class SourceFixtures extends Fixture
{
    public const SOURCE_PROPERTIES = [
        [
            "path" => "/resources/fixtures/WeedWeekNews.html",
            "sectionsArray" => [
                "recenzje" => "/<h2 class=\"Article__StyledTitle-sc-1ui24ze-5 jToriD\">(?P<ref>[^\"]*)<\/h2>/",
                "aktualności" => "/<h2 class=\"Article__StyledTitle-sc-1ui24ze-5 jToriD\">(?P<ref>[^\"]*)<\/h2>/"
            ],
            "recentNewsCount" => 4,
            "activeNewsCount" => 8,
            "outdatedNewsCount" => 32,
            "allNewsCount" => 40
        ],
        [
            "path" => "/resources/fixtures/HairdressersNews.html",
            "sectionsArray" => [
                "aktualności" => "/<p class=\"tytul\">\s+(?P<ref>[^\"]*)<\/p>/"
            ],
            "recentNewsCount" => 5,
            "activeNewsCount" => 10,
            "outdatedNewsCount" => 50,
            "allNewsCount" => 60
        ],
        [
            "path" => "/resources/fixtures/CosmeticNews.html",
            "sectionsArray" => [
                "aktualności" => "/class=\"title\">(?P<ref>[^\"]*)<\/a>/"
            ],
            "recentNewsCount" => 3,
            "activeNewsCount" => 7,
            "outdatedNewsCount" => 21,
            "allNewsCount" => 28
        ],
        [
            "path" => "/resources/fixtures/LenkowoNews.html",
            "sectionsArray" => [
                "aktualności" => "/<a href=\".*#\" title=\"#\">(?P<ref>[^\"]*)<\/a>/"
            ],
            "recentNewsCount" => 2,
            "activeNewsCount" => 10,
            "outdatedNewsCount" => 20,
            "allNewsCount" => 30
        ],
        [
            "path" => "/resources/fixtures/CracowSchoolsNews.html",
            "sectionsArray" => [
                "aktualności" => "/<h2 class=\"no-margin-top\">(?P<ref>[^\"]*)<\/h2>/"
            ],
            "recentNewsCount" => 3,
            "activeNewsCount" => 8,
            "outdatedNewsCount" => 24,
            "allNewsCount" => 32
        ]
    ];

    private NewsCollectionCreator $newsCollectionCreator;
    private SourceCreator $sourceCreator;
    private SourceRepository $sourceRepository;
    private string $projectDir;

    /**
     * SourceFixtures constructor.
     * @param NewsCollectionCreator $newsCollectionCreator
     * @param SourceCreator $sourceCreator
     * @param SourceRepository $sourceRepository
     * @param string $projectDir
     */
    public function __construct(
        NewsCollectionCreator $newsCollectionCreator,
        SourceCreator $sourceCreator,
        SourceRepository $sourceRepository,
        string $projectDir
    )
    {
        $this->newsCollectionCreator = $newsCollectionCreator;
        $this->sourceCreator = $sourceCreator;
        $this->sourceRepository = $sourceRepository;
        $this->projectDir = $projectDir;
    }

    /**
     * @param ObjectManager $objectManager
     * @throws MissingScrapperException
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws ValidationException
     * @throws SourceAlreadyExistException
     */
    public function load(ObjectManager $objectManager): void
    {
        foreach (self::SOURCE_PROPERTIES as $sourceProperty) {
            $source = $this->sourceCreator->createSource(
                Uuid::uuid4(),
                $this->projectDir . $sourceProperty["path"],
                $sourceProperty["sectionsArray"]
                );

            $this->sourceRepository->persist($source);
            $this->sourceRepository->save();

            ContentFetcherMock::addNewContent(
                $this->projectDir . $sourceProperty["path"],
                file_get_contents($this->projectDir . $sourceProperty["path"])
            );

            $this->newsCollectionCreator->create($source);
            $this->sourceRepository->save();
        }
    }
}
