<?php
declare(strict_types=1);

namespace App\Pagination;

class PaginatedList
{
    public array $items;
    public array $pagination;

    /**
     * PaginatedList constructor.
     * @param array $items
     * @param array $pagination
     */
    public function __construct(array $items, array $pagination)
    {
        $this->items = $items;
        $this->pagination = $pagination;
    }
}
