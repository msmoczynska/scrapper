<?php
declare(strict_types=1);

namespace App\Pagination;

use App\Repository\SourceRepository;
use JMS\Serializer\SerializerInterface;

class SourcePaginator extends AbstractPaginator
{
    /**
     * NewsPaginator constructor.
     * @param SourceRepository $sourceRepository
     * @param SerializerInterface $serializer
     */
    public function __construct(SourceRepository $sourceRepository, SerializerInterface $serializer)
    {
        $this->repository = $sourceRepository;

        parent::__construct($serializer);
    }
}
