<?php
declare(strict_types=1);

namespace App\Pagination;

use App\Repository\NewsRepository;
use JMS\Serializer\SerializerInterface;

class NewsPaginator extends AbstractPaginator
{
    /**
     * NewsPaginator constructor.
     * @param NewsRepository $sourceRepository
     * @param SerializerInterface $serializer
     */
    public function __construct(NewsRepository $sourceRepository, SerializerInterface $serializer)
    {
        $this->repository = $sourceRepository;

        parent::__construct($serializer);
    }
}

