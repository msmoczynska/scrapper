<?php
declare(strict_types=1);

namespace App\Pagination;

use App\Repository\PaginationAwareRepositoryInterface;
use Doctrine\ORM\NonUniqueResultException;
use JMS\Serializer\SerializerInterface;

abstract class AbstractPaginator
{
    protected PaginationAwareRepositoryInterface $repository;
    private SerializerInterface $serializer;

    /**
     * NewsPaginator constructor.
     * @param SerializerInterface $serializer
     */
    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * @param PaginationParams $paginationParams
     * @return PaginatedList
     * @throws NonUniqueResultException
     */
    public function getPaginatedList(PaginationParams $paginationParams): PaginatedList
    {
        $itemsForPage = $this->repository->findItemsForPage(
            $paginationParams->getLimit(),
            $paginationParams->getFirstResult(),
            $paginationParams->getSearchPhrase()
        );

        $numberOfAllItems = $this->repository->countAllItems($paginationParams->getSearchPhrase());

        $arrayItems = $this->serializer->toArray($itemsForPage);
        $pagination = $paginationParams->createPaginationParamsArray($numberOfAllItems);

        return new PaginatedList($arrayItems, $pagination);
    }
}
