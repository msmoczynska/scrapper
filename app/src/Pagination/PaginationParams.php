<?php
declare(strict_types=1);

namespace App\Pagination;

class PaginationParams
{
    public const DEFAULT_LIMIT = 10;
    public const DEFAULT_PAGE = 1;

    private int $limit;
    private int $currentPage;
    private ?string $searchPhrase = null;

    /**
     * PaginationParams constructor.
     * @param int $limit
     * @param int $currentPage
     */
    public function __construct(
        int $limit = self::DEFAULT_LIMIT,
        int $currentPage = self::DEFAULT_PAGE
    )
    {
        $this->limit = $limit;
        $this->currentPage = $currentPage;
    }

    /**
     * @return int
     */
    public function getLimit(): int
    {
        return $this->limit;
    }

    /**
     * @return int
     */
    public function getCurrentPage(): int
    {
        return $this->currentPage;
    }

    /**
     * @return string|null
     */
    public function getSearchPhrase(): ?string
    {
        return $this->searchPhrase;
    }

    /**
     * @param string $searchPhrase
     * @return $this
     */
    public function setSearchPhrase(string $searchPhrase): self
    {
        $this->searchPhrase = $searchPhrase;

        return $this;
    }

    /**
     * @param int $numberOfAllItems
     * @return int
     */
    public function getNumberOfAllPages(int $numberOfAllItems): int
    {
        return (int) ceil($numberOfAllItems / $this->limit);
    }

    /**
     * @param int $numberOfAllItems
     * @return int
     */
    public function getNumberOfItemsOnCurrentPage(int $numberOfAllItems): int
    {
        $numberOfAllPages = $this->getNumberOfAllPages($numberOfAllItems);
        $numberOfItemsOnLastPage = 0 === $numberOfAllItems % $this->limit ? $this->limit : $numberOfAllItems % $this->limit;

        return $this->currentPage === $numberOfAllPages ? $numberOfItemsOnLastPage : $this->limit;
    }

    /**
     * @return int
     */
    public function getFirstResult(): int
    {
        return ($this->currentPage * $this->limit) - $this->limit;
    }

    /**
     * @param int $numberOfAllItems
     * @return array
     */
    public function createPaginationParamsArray(int $numberOfAllItems): array
    {
        return [
            "limit" => $this->getLimit(),
            "page" => $this->getCurrentPage(),
            "pages" => $this->getNumberOfAllPages($numberOfAllItems),
            "selected" => $this->getNumberOfItemsOnCurrentPage($numberOfAllItems),
            "total" => $numberOfAllItems,
            "firstOrdinalNumber" => $this->getFirstResult() + 1,
        ];
    }
}
