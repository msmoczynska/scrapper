<?php
declare(strict_types=1);

namespace App\ExportGenerator;

use App\Enum\ExportDetails;
use DateTime;
use PhpOffice\PhpSpreadsheet\Shared\File;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Exception;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class XlsxExportGenerator implements ExportGeneratorInterface
{
    /**
     * @param array $exportContent
     * @param string $type
     * @return string
     * @throws Exception
     */
    public function generate(array $exportContent, string $type): string
    {
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->fromArray($exportContent);

        $filename = (new DateTime())->format('Y-m-d-H-i') . $type . 'List.xlsx';

        $writer = new Xlsx($spreadsheet);
        $writer->save(File::sysGetTempDir() . '/' . $filename);

        return $filename;
    }

    /**
     * @param string $exportFormat
     * @return bool
     */
    public function support(string $exportFormat): bool
    {
        return ExportDetails::XLSX_FORMAT === $exportFormat;
    }
}
