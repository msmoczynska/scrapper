<?php
declare(strict_types=1);

namespace App\ExportGenerator;

use App\Enum\ExportDetails;
use DateTime;
use PhpOffice\PhpSpreadsheet\Shared\File;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Csv;

class CsvExportGenerator implements ExportGeneratorInterface
{
    /**
     * @param array $exportContent
     * @param string $type
     * @return string
     */
    public function generate(array $exportContent, string $type): string
    {
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->fromArray($exportContent);

        $filename = (new DateTime())->format('Y-m-d-H-i') . $type . 'List.csv';

        $writer = new Csv($spreadsheet);
        $writer->save(File::sysGetTempDir() . '/' . $filename);

        return $filename;
    }

    /**
     * @param string $exportFormat
     * @return bool
     */
    public function support(string $exportFormat): bool
    {
        return ExportDetails::CSV_FORMAT === $exportFormat;
    }
}
