<?php
declare(strict_types=1);

namespace App\ExportGenerator;

use App\Exception\MissingExportGeneratorException;

class ExportGeneratorProvider
{
    private iterable $exportGenerators;

    /**
     * ExportGeneratorProvider constructor.
     * @param iterable $exportGenerators
     */
    public function __construct(iterable $exportGenerators)
    {
        $this->exportGenerators = $exportGenerators;
    }

    /**
     * @param string $exportFormat
     * @return ExportGeneratorInterface
     * @throws MissingExportGeneratorException
     */
    public function provide(string $exportFormat): ExportGeneratorInterface
    {
        /** @var ExportGeneratorInterface $exportGenerator */
        foreach ($this->exportGenerators as $exportGenerator) {
            if ($exportGenerator->support($exportFormat)) {
                return $exportGenerator;
            }
        }

        throw new MissingExportGeneratorException();
    }
}
