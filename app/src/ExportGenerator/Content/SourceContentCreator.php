<?php
declare(strict_types=1);

namespace App\ExportGenerator\Content;

use App\Enum\ExportDetails;
use App\Enum\TableHeadersForExport;
use App\Repository\SourceRepository;

class SourceContentCreator implements ContentCreatorInterface
{
    private SourceRepository $sourceRepository;

    /**
     * SourceContentCreator constructor.
     * @param SourceRepository $sourceRepository
     */
    public function __construct(SourceRepository $sourceRepository)
    {
        $this->sourceRepository = $sourceRepository;
    }

    /**
     * @return array
     */
    public function create(): array
    {
        $sources = $this->sourceRepository->findItemsForExport();
        $sourcesExportArray = [];

        $sourcesExportArray[] = TableHeadersForExport::SOURCE_HEADERS;

        foreach ($sources as $key => $source) {
            $sourcesExportArray[] = [
                $key + 1,
                $source->getPath(),
                "",
                $source->getDaysSinceInTheSystem(),
                $source->getRecentNewsCount(),
                $source->getActiveNewsCount(),
                $source->getOutdatedNewsCount(),
                $source->getAllNewsCount(),
            ];

            foreach ($source->getSections() as $section) {
                $sourcesExportArray[] = [
                    "",
                    "",
                    $section->getName(),
                    $section->getDaysSinceInTheSystem(),
                    $section->getRecentNewsCount(),
                    $section->getActiveNewsCount(),
                    $section->getOutdatedNewsCount(),
                    $section->getAllNewsCount()
                ];
            }
        }

        return $sourcesExportArray;
    }

    /**
     * @param string $type
     * @return bool
     */
    public function support(string $type): bool
    {
        return ExportDetails::SOURCE_TYPE === $type;
    }
}
