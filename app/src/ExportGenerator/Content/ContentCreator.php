<?php
declare(strict_types=1);

namespace App\ExportGenerator\Content;

use App\Exception\MissingContentCreatorException;

class ContentCreator
{
    private iterable $contentCreators;

    /**
     * ContentCreator constructor.
     * @param iterable $contentCreators
     */
    public function __construct(iterable $contentCreators)
    {
        $this->contentCreators = $contentCreators;
    }

    /**
     * @param string $type
     * @return array
     * @throws MissingContentCreatorException
     */
    public function createContentForProperItemsType(string $type): array
    {
        /** @var ContentCreatorInterface $contentCreator */
        foreach ($this->contentCreators as $contentCreator) {
            if ($contentCreator->support($type)) {
                return $contentCreator->create();
            }
        }

        throw new MissingContentCreatorException();
    }
}
