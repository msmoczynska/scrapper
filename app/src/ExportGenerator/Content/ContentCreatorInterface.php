<?php
declare(strict_types=1);

namespace App\ExportGenerator\Content;

interface ContentCreatorInterface
{
    /**
     * @return array
     */
    public function create(): array;

    /**
     * @param string $type
     * @return bool
     */
    public function support(string $type): bool;
}
