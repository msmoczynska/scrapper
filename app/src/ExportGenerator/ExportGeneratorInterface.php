<?php
declare(strict_types=1);

namespace App\ExportGenerator;

interface ExportGeneratorInterface
{
    /**
     * @param array $exportContent
     * @param string $type
     * @return string
     */
    public function generate(array $exportContent, string $type): string;

    /**
     * @param string $exportFormat
     * @return bool
     */
    public function support(string $exportFormat): bool;
}
