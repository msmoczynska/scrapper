<?php
declare(strict_types=1);

namespace App\File;

use App\Exception\FileValidationException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class FileUploader
{
    private const FILES_DIRECTORY = "/var/files/";

    private string $projectDir;
    private ValidatorInterface $validator;

    /**
     * FileUploader constructor.
     * @param string $projectDir
     * @param ValidatorInterface $validator
     */
    public function __construct(string $projectDir, ValidatorInterface $validator)
    {
        $this->projectDir = $projectDir;
        $this->validator = $validator;
    }

    /**
     * @param UploadedFile $file
     * @return string
     * @throws FileValidationException
     */
    public function upload(UploadedFile $file): string
    {
        $this->validate($file);

        $fileName = $file->getClientOriginalName();
        $fileDirectory = $this->projectDir . self::FILES_DIRECTORY;

        $file->move($fileDirectory, $fileName);

        return $fileDirectory . $fileName;
    }

    /**
     * @param UploadedFile $file
     * @throws FileValidationException
     */
    private function validate(UploadedFile $file): void
    {
        $fileErrors = $this->validator->validate(
            $file,
            [
                new NotBlank([
                    'message' => 'Please, upload the file.'
                ]),
                new File([
                    'maxSize' => '300k',
                    'mimeTypes'=> 'text/html'
                ])
            ]
        );

        if (count($fileErrors) > 0) {
            throw new FileValidationException($fileErrors);
        }
    }
}
