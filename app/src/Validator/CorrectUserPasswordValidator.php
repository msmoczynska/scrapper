<?php
declare(strict_types=1);

namespace App\Validator;

use App\Message\User\UserAwareInterface;
use App\Repository\UserRepository;
use LogicException;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class CorrectUserPasswordValidator extends ConstraintValidator
{
    private UserRepository $userRepository;
    private  UserPasswordEncoderInterface $passwordEncoder;

    /**
     * CorrectUserPasswordValidator constructor.
     * @param UserRepository $userRepository
     * @param UserPasswordEncoderInterface $passwordEncoder
     */
    public function __construct(UserRepository $userRepository, UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->userRepository = $userRepository;
        $this->passwordEncoder = $passwordEncoder;
    }

    /**
     * @param mixed $password
     * @param Constraint $constraint
     *
     * {@inheritdoc}
     */
    public function validate($password, Constraint $constraint): void
    {
        if (!$constraint instanceof CorrectUserPassword) {
            throw new UnexpectedTypeException($constraint, CorrectUserPassword::class);
        }

        $object = $this->context->getObject();

        if (!$object instanceof UserAwareInterface) {
            throw new LogicException("This constraint cannot be used on object, which not implements UserAwareInterface");
        }

        $user = $this->userRepository->findOneBy(['id' => $object->getUserId()]);

        if ($user && $this->passwordEncoder->isPasswordValid($user, $password)) {
            return;
        }

        /* @var CorrectUserPassword $constraint */
        $this->context->buildViolation($constraint->message)
            ->setParameter('{{ value }}', $password)
            ->addViolation();
    }
}
