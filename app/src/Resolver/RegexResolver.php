<?php
declare(strict_types=1);

namespace App\Resolver;

use App\Exception\WrongRegexException;
use Exception;

class RegexResolver
{
    /**
     * @param string $regex
     * @param string $sourceContent
     * @return RegexResults
     * @throws WrongRegexException
     */
    public function resolve(string $regex, string $sourceContent): RegexResults
    {
        try {
            preg_match_all($regex, $sourceContent, $matches);
        } catch (Exception $exception) {
            throw new WrongRegexException();
        }

        return new RegexResults(false !== $matches ? $matches['ref'] : []);
    }
}
