<?php
declare(strict_types=1);

namespace App\Resolver;

class RegexResultsCleaner
{
    /**
     * @param RegexResults $results
     * @return RegexResults
     */
    public function clean(RegexResults $results): RegexResults
    {
        $resultsArray = $results->toArray();
        foreach ($resultsArray as &$result) {
            $result = trim($result);
            $result = str_replace("'", "", $result);
        }

        return new RegexResults($resultsArray);
    }
}
