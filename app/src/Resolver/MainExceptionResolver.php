<?php
declare(strict_types=1);

namespace App\Resolver;

use Symfony\Component\Messenger\Exception\HandlerFailedException;
use Throwable;

class MainExceptionResolver
{
    public function resolve(Throwable $exception): Throwable
    {
        if ($exception instanceof HandlerFailedException) {
            return $this->resolve($exception->getPrevious());
        }

        return $exception;
    }
}
