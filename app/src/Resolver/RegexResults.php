<?php
declare(strict_types=1);

namespace App\Resolver;

use Doctrine\Common\Collections\ArrayCollection;

class RegexResults extends ArrayCollection
{
    /**
     * @return array
     */
    public function getUniqueResults():array
    {
        return array_values(array_unique($this->toArray()));
    }
}
