<?php

declare(strict_types=1);

namespace App\DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Class Version20210127071652
 * @package App\DoctrineMigrations
 * @codeCoverageIgnore
 */
final class Version20210127071652 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE user ADD confirmation_token VARCHAR(20) NOT NULL, ADD verified TINYINT(1) DEFAULT \'0\' NOT NULL, ADD reset_password_token VARCHAR(20) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE user DROP confirmation_token, DROP verified, DROP reset_password_token');
    }
}
