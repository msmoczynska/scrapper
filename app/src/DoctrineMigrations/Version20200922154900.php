<?php

declare(strict_types=1);

namespace App\DoctrineMigrations;

use App\Entity\News;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Auto-generated Migration: Please modify to your needs!
 * @codeCoverageIgnore
 */
final class Version20200922154900 extends AbstractMigration implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @return string
     */
    public function getDescription() : string
    {
        return '';
    }

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema) : void
    {
        $entityManager = $this->container->get('doctrine.orm.entity_manager');
        $newsRepository = $entityManager->getRepository(News::class);
        $incorrectlyLabeledNews = $newsRepository->findBy(['updatedAt' => null, 'enabled' => 0]);

        foreach ($incorrectlyLabeledNews as $news) {
            /** @var News $news */
            $dateForSetUpdatedAt = clone $news->getCreatedAt();
            $news->setUpdatedAt($dateForSetUpdatedAt->modify('+1 hour'));
        }

        $entityManager->flush();
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema) : void
    {
    }
}
