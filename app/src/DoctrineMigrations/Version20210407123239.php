<?php

declare(strict_types=1);

namespace App\DoctrineMigrations;

use CapCollectif\IdToUuid\IdToUuidMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210407123239 extends IdToUuidMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function postUp(Schema $schema): void
    {
        $this->migrate('source');
        $this->migrate('section');
        $this->migrate('news');
        $this->migrate('user');
    }

    public function up(Schema $schema) : void
    {
    }

    public function down(Schema $schema) : void
    {
    }
}
