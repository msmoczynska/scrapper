<?php

declare(strict_types=1);

namespace App\DoctrineMigrations;

use App\Entity\News;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Auto-generated Migration: Please modify to your needs!
 * @codeCoverageIgnore
 */
final class Version20201012101659 extends AbstractMigration implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @return string
     */
    public function getDescription() : string
    {
        return '';
    }

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema) : void
    {
        $entityManager = $this->container->get('doctrine.orm.entity_manager');
        $newsRepository = $entityManager->getRepository(News::class);

        foreach ($newsRepository->findAll() as $news) {
            /** @var News $news */
            $cleanedNews = trim($news->getTitle());
            $cleanedNews = str_replace("'", "", $cleanedNews);
            $news->setTitle($cleanedNews);
        }

        $entityManager->flush();
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema) : void
    {
    }
}
