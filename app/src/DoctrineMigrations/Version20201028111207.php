<?php

declare(strict_types=1);

namespace App\DoctrineMigrations;

use App\Service\NewsUpdater;
use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class Version20201028111207
 * @package App\DoctrineMigrations
 * @codeCoverageIgnore
 */
final class Version20201028111207 extends AbstractMigration implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    public function getDescription() : string
    {
        return '';
    }

    /**
     * @param Schema $schema
     * @throws DBALException
     */
    public function up(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE source ADD recent_news_count INT DEFAULT 0 NOT NULL, ADD active_news_count INT DEFAULT 0 NOT NULL, ADD outdated_news_count INT DEFAULT 0 NOT NULL, ADD all_news_count INT DEFAULT 0 NOT NULL');
        $this->addSql('ALTER TABLE section ADD recent_news_count INT DEFAULT 0 NOT NULL, ADD active_news_count INT DEFAULT 0 NOT NULL, ADD outdated_news_count INT DEFAULT 0 NOT NULL, ADD all_news_count INT DEFAULT 0 NOT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function postUp(Schema $schema) : void
    {
        $newsUpdater = $this->container->get(NewsUpdater::class);

        $newsUpdater->update();
    }

    /**
     * @param Schema $schema
     * @throws DBALException
     */
    public function down(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE section DROP recent_news_count, DROP active_news_count, DROP outdated_news_count, DROP all_news_count');
        $this->addSql('ALTER TABLE source DROP recent_news_count, DROP active_news_count, DROP outdated_news_count, DROP all_news_count');
    }
}
