<?php
declare(strict_types=1);

namespace App\Repository;

use Doctrine\ORM\NonUniqueResultException;

interface PaginationAwareRepositoryInterface
{
    /**
     * @param int $limit
     * @param int $firstResult
     * @param string|null $searchPhrase
     * @return array
     */
    public function findItemsForPage(int $limit, int $firstResult, ?string $searchPhrase = null): array;

    /**
     * @param string|null $searchPhrase
     * @return int
     * @throws NonUniqueResultException
     */
    public function countAllItems(?string $searchPhrase = null): int;
}
