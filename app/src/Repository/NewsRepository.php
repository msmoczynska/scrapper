<?php
declare(strict_types=1);

namespace App\Repository;

use App\Entity\News;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method News|null find($id, $lockMode = null, $lockVersion = null)
 * @method News|null findOneBy(array $criteria, array $orderBy = null)
 * @method News[]    findAll()
 * @method News[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NewsRepository extends ServiceEntityRepository implements PaginationAwareRepositoryInterface
{
    /**
     * NewsRepository constructor.
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, News::class);
    }

    /**
     * @param int $limit
     * @param int $firstResult
     * @param string|null $searchPhrase
     * @return array
     */
    public function findItemsForPage(int $limit, int $firstResult, ?string $searchPhrase = null): array
    {
        $queryBuilder = $this->createQueryBuilder('n')
            ->orderBy('n.createdAt', 'DESC')
            ->setFirstResult($firstResult)
            ->setMaxResults($limit);

        if (null !== $searchPhrase) {
            $queryBuilder
                ->where($queryBuilder->expr()->like('n.title', ':searchPhrase'))
                ->setParameter("searchPhrase", "%" . $searchPhrase . "%");
        }

        return $queryBuilder->getQuery()->getResult();
    }

    /**
     * @param string|null $searchPhrase
     * @return int
     * @throws NonUniqueResultException
     * @throws NoResultException
     */
    public function countAllItems(?string $searchPhrase = null): int
    {
        $queryBuilder = $this->createQueryBuilder('n')->select('count(n.id)');

        if (null !== $searchPhrase) {
            $queryBuilder
                ->where($queryBuilder->expr()->like('n.title', ':searchPhrase'))
                ->setParameter("searchPhrase", "%" . $searchPhrase . "%");
        }

        return (int) $queryBuilder->getQuery()->getSingleScalarResult();
    }
}
