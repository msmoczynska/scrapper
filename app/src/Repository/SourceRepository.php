<?php
declare(strict_types=1);

namespace App\Repository;

use App\Entity\Source;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Source|null find($id, $lockMode = null, $lockVersion = null)
 * @method Source|null findOneBy(array $criteria, array $orderBy = null)
 * @method Source[]    findAll()
 * @method Source[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SourceRepository extends ServiceEntityRepository implements PaginationAwareRepositoryInterface
{
    /**
     * SourceRepository constructor.
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Source::class);
    }

    /**
     * @param Source $source
     * @throws ORMException
     */
    public function persist(Source $source): void
    {
        $this->_em->persist($source);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function save(): void
    {
        $this->_em->flush();
    }

    /**
     * @param int $limit
     * @param int $firstResult
     * @param string|null $searchPhrase
     * @return array
     */
    public function findItemsForPage(int $limit, int $firstResult, ?string $searchPhrase = null): array
    {
        $queryBuilder = $this->createQueryBuilder('s')
            ->orderBy('s.createdAt', 'DESC')
            ->setFirstResult($firstResult)
            ->setMaxResults($limit);

        if (null !== $searchPhrase) {
            $queryBuilder
                ->where($queryBuilder->expr()->like('s.path', ':searchPhrase'))
                ->setParameter("searchPhrase", "%" . $searchPhrase . "%");
        }

        return $queryBuilder->getQuery()->getResult();
    }

    /**
     * @param string|null $searchPhrase
     * @return int
     * @throws NonUniqueResultException
     * @throws NoResultException
     */
    public function countAllItems(?string $searchPhrase = null): int
    {
        $queryBuilder = $this->createQueryBuilder('s')->select('count(s.id)');

        if (null !== $searchPhrase) {
            $queryBuilder
                ->where($queryBuilder->expr()->like('s.path', ':searchPhrase'))
                ->setParameter("searchPhrase", "%" . $searchPhrase . "%");
        }

        return (int) $queryBuilder->getQuery()->getSingleScalarResult();
    }

    /**
     * @return array
     */
    public function findItemsForExport(): array
    {
        $queryBuilder = $this->createQueryBuilder('s')
            ->addOrderBy('s.title');

        return $queryBuilder->getQuery()->getResult();
    }
}
