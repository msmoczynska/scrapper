<?php
declare(strict_types=1);

namespace App\Command;

use App\Exception\MissingScrapperException;
use App\Service\NewsUpdater;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GetAllNewsCommand extends Command
{
    protected static $defaultName = 'app:scrapper:getAllNews';
    private NewsUpdater $newsUpdater;

    /**
     * GetAllNewsCommand constructor.
     * @param NewsUpdater $newsUpdater
     */
    public function __construct(NewsUpdater $newsUpdater)
    {
        $this->newsUpdater = $newsUpdater;

        parent::__construct();
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     * @throws MissingScrapperException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    protected function execute(InputInterface $input, OutputInterface $output): int 
    {
        $allSourceStats = $this->newsUpdater->update();

        $output->writeln(sprintf(
                "%u news added to the database, %u updated and %u marked as outdated.",
                $allSourceStats->getRecentNewsCount(),
                $allSourceStats->getActiveNewsCount(),
                $allSourceStats->getOutdatedNewsCount()
            ));

        return 0;
    }
}
