<?php
declare(strict_types=1);

namespace App\EntityCollection;

use App\Entity\News;
use Doctrine\Common\Collections\ArrayCollection;

class NewsCollection extends ArrayCollection
{
    /**
     * @return NewsCollection
     */
    public function getRecentNews(): NewsCollection
    {
        return $this->filter(function (News $news) {
            return null === $news->getUpdatedAt() && $news->getEnabled();
        });
    }

    /**
     * @return NewsCollection
     */
    public function getActiveNews(): NewsCollection
    {
        return $this->filter(function (News $news) {
            return true === $news->getEnabled();
        });
    }

    /**
     * @return NewsCollection
     */
    public function getUpdatedNews(): NewsCollection
    {
        return $this->filter(function (News $news) {
            return null !== $news->getUpdatedAt();
        });
    }

    /**
     * @return NewsCollection
     */
    public function getOutdatedNews(): NewsCollection
    {
        return $this->filter(function (News $news) {
            return false === $news->getEnabled();
        });
    }
}
