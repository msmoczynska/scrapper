<?php
declare(strict_types=1);

namespace App\EventSubscriber;

use App\Exception\FileValidationException;
use App\Exception\ConfirmationTokenNotFoundException;
use App\Exception\ValidationException;
use App\Resolver\MainExceptionResolver;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Throwable;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class ExceptionSubscriber implements EventSubscriberInterface
{
    private Environment $templating;
    private MainExceptionResolver $mainExceptionResolver;

    /**
     * ExceptionSubscriber constructor.
     * @param Environment $templating
     * @param MainExceptionResolver $mainExceptionResolver
     */
    public function __construct(Environment $templating, MainExceptionResolver $mainExceptionResolver)
    {
        $this->templating = $templating;
        $this->mainExceptionResolver = $mainExceptionResolver;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::EXCEPTION => ['onKernelException', 10]
        ];
    }

    /**
     * @param ExceptionEvent $event
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function onKernelException(ExceptionEvent $event): void
    {
        $exception = $this->mainExceptionResolver->resolve($event->getThrowable());

        $response = !$event->getRequest()->isXmlHttpRequest() ? $this->generateResponse($exception) : $this->generateJsonResponse($exception);

        $event->setResponse($response);
    }

    /**
     * @param Throwable $exception
     * @return Response
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function generateResponse(Throwable $exception): Response
    {
        $exceptionCode = 0 !== $exception->getCode() ? $exception->getCode() : Response::HTTP_BAD_REQUEST;

        return new Response(
            $this->templating->render(
                $this->getProperTwigTemplate($exception),
                ['message' => $exception->getMessage()]
            ),
            $exceptionCode
        );
    }

    /**
     * @param Throwable $exception
     * @return string
     */
    public function getProperTwigTemplate(Throwable $exception): string
    {
        switch ($exception){
            case $exception instanceof ConfirmationTokenNotFoundException:
                return 'web/registration/confirmRegistrationError.html.twig';
            default:
                return 'web/exception/exception.html.twig';
        }
    }

    /**
     * @param Throwable $exception
     * @return JsonResponse
     */
    public function generateJsonResponse(Throwable $exception): JsonResponse
    {
        return new JsonResponse(
            ["message" => $this->generateErrorMessage($exception)],
            JsonResponse::HTTP_BAD_REQUEST
        );
    }

    /**
     * @param Throwable $exception
     * @return string
     */
    public function generateErrorMessage(Throwable $exception): string
    {
        switch (true) {
            case $exception instanceof ValidationException:
                return $this->getValidationExceptionMessage($exception);
            case $exception instanceof AccessDeniedException:
                return "Access denied.";
            default:
                return $exception->getMessage();
        }
    }

    /**
     * @param ValidationException $exception
     * @return string
     */
    private function getValidationExceptionMessage(ValidationException $exception): string
    {
        $message = "";

        foreach ($exception->getErrors() as $key => $violation) {
            $message = $message . " | " . $violation->getPropertyPath() . ": " . $violation->getMessage();

            if (0 === $key && $exception instanceof FileValidationException) {
                $message = substr($message, 5);
            } elseif (0 === $key) {
                $message = substr($message, 3);
            }
        }

        return $message;
    }
}
