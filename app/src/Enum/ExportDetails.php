<?php
declare(strict_types=1);

namespace App\Enum;

class ExportDetails
{
    public const XLSX_FORMAT = "xlsx";
    public const CSV_FORMAT = "csv";

    public const SOURCE_TYPE = "source";
    public const NEWS_TYPE = "news";
}
