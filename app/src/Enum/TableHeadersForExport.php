<?php
declare(strict_types=1);

namespace App\Enum;

class TableHeadersForExport
{
    public const SOURCE_HEADERS  = [
        'No.',
        'WWW',
        'Section',
        'Days since in the system',
        'Recent news',
        'Active news',
        'Outdated news',
        'All news',
    ];
}
