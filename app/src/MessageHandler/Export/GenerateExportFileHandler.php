<?php
declare(strict_types=1);

namespace App\MessageHandler\Export;

use App\Exception\MissingContentCreatorException;
use App\Exception\MissingExportGeneratorException;
use App\ExportGenerator\Content\ContentCreator;
use App\ExportGenerator\ExportGeneratorProvider;
use App\Message\Export\GenerateExportFile;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class GenerateExportFileHandler implements MessageHandlerInterface
{
    private ExportGeneratorProvider $exportGeneratorProvider;
    private ContentCreator $contentCreator;

    /**
     * GenerateExportFileHandler constructor.
     * @param ExportGeneratorProvider $exportGeneratorProvider
     * @param ContentCreator $contentCreator
     */
    public function __construct(ExportGeneratorProvider $exportGeneratorProvider, ContentCreator $contentCreator)
    {
        $this->exportGeneratorProvider = $exportGeneratorProvider;
        $this->contentCreator = $contentCreator;
    }

    /**
     * @param GenerateExportFile $generateExportFile
     * @return string
     * @throws MissingExportGeneratorException
     * @throws MissingContentCreatorException
     */
    public function __invoke(GenerateExportFile $generateExportFile): string
    {
        $exportContent = $this->contentCreator->createContentForProperItemsType($generateExportFile->type);

        $exportGenerator = $this->exportGeneratorProvider->provide($generateExportFile->format);

        return $exportGenerator->generate($exportContent, $generateExportFile->type);
    }
}