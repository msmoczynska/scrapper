<?php
declare(strict_types=1);

namespace App\MessageHandler\User;

use App\Exception\UserNotFoundException;
use App\Message\User\ResetPasswordRequest;
use App\Repository\UserRepository;
use App\SecurityService\SecurityMailer;
use App\SecurityService\TokenGenerator;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class ResetPasswordRequestHandler implements MessageHandlerInterface
{
    private UserRepository $userRepository;
    private SecurityMailer $securityMailer;
    private TokenGenerator $tokenGenerator;

    /**
     * PasswordController constructor.
     * @param UserRepository $userRepository
     * @param SecurityMailer $securityMailer
     * @param TokenGenerator $tokenGenerator
     */
    public function __construct(UserRepository $userRepository, SecurityMailer $securityMailer, TokenGenerator $tokenGenerator)
    {
        $this->userRepository = $userRepository;
        $this->securityMailer = $securityMailer;
        $this->tokenGenerator = $tokenGenerator;
    }

    /**
     * @param ResetPasswordRequest $message
     * @throws UserNotFoundException
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function __invoke(ResetPasswordRequest $message): void
    {
        $user = $this->userRepository->findOneBy(['email' => $message->email]);

        if (null === $user) {
            throw new UserNotFoundException();
        }

        $user->setResetPasswordToken($this->tokenGenerator->generate());

        $this->userRepository->save();

        $this->securityMailer->sendResetPasswordEmail($user);
    }
}

