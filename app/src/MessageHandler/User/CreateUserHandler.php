<?php
declare(strict_types=1);

namespace App\MessageHandler\User;

use App\Entity\User;
use App\Exception\UserAlreadyExistException;
use App\Message\User\CreateUser;
use App\Repository\UserRepository;
use App\SecurityService\SecurityMailer;
use App\SecurityService\TokenGenerator;
use DateTime;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Exception;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class CreateUserHandler implements MessageHandlerInterface
{
    private UserRepository $userRepository;
    private UserPasswordEncoderInterface $passwordEncoder;
    private TokenGenerator $tokenGenerator;
    private SecurityMailer $securityMailer;

    /**
     * CreateUserHandler constructor.
     * @param UserRepository $userRepository
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param TokenGenerator $tokenGenerator
     * @param SecurityMailer $securityMailer
     */
    public function __construct(
        UserRepository $userRepository,
        UserPasswordEncoderInterface $passwordEncoder,
        TokenGenerator $tokenGenerator,
        SecurityMailer $securityMailer
    )
    {
        $this->userRepository = $userRepository;
        $this->passwordEncoder = $passwordEncoder;
        $this->tokenGenerator = $tokenGenerator;
        $this->securityMailer = $securityMailer;
    }

    /**
     * @param CreateUser $createUser
     * @return User
     * @throws LoaderError
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws RuntimeError
     * @throws SyntaxError
     * @throws UserAlreadyExistException
     * @throws Exception
     */
    public function __invoke(CreateUser $createUser): User
    {
        /** @var User $user */
        $user = $this->userRepository->findOneBy(['email' => $createUser->email]);

        if (null !== $user) {
            throw new UserAlreadyExistException();
        }

        $user = new User(
            $createUser->id,
            $createUser->email,
            $createUser->firstName,
            $createUser->secondName,
            new DateTime($createUser->birthDate),
            $this->tokenGenerator->generate()
        );

        $user
            ->setPassword($this->passwordEncoder->encodePassword($user, $createUser->password));

        $this->userRepository->persist($user);
        $this->userRepository->save();

        $this->securityMailer->sendConfirmRegistrationEmail($user);

//        TODO: make a wrapper on messageBus that will pull the verse from the last message
//              or use the HandleTrait provided by the messenger
        return $user;
    }
}
