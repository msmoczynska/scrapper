<?php
declare(strict_types=1);

namespace App\MessageHandler\User;

use App\Entity\User;
use App\Exception\ConfirmationTokenNotFoundException;
use App\Message\User\ConfirmUser;
use App\Repository\UserRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class ConfirmUserHandler implements MessageHandlerInterface
{
    private UserRepository $userRepository;

    /**
     * ConfirmUserHandler constructor.
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param ConfirmUser $confirmUser
     * @return User
     * @throws ConfirmationTokenNotFoundException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function __invoke(ConfirmUser $confirmUser): User
    {
        $user = $this->userRepository->findOneBy(['confirmationToken' => $confirmUser->confirmationToken]);

        if (null === $user) {
            throw new ConfirmationTokenNotFoundException();
        }

        $user->setVerified(true);
        $this->userRepository->save();

        return $user;
    }
}
