<?php
declare(strict_types=1);

namespace App\MessageHandler\User;

use App\Message\User\ResetPassword;
use App\Repository\UserRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class ResetPasswordHandler implements MessageHandlerInterface
{
    private UserRepository $userRepository;
    private UserPasswordEncoderInterface $passwordEncoder;

    /**
     * ResetPasswordHandler constructor.
     * @param UserRepository $userRepository
     * @param UserPasswordEncoderInterface $passwordEncoder
     */
    public function __construct(UserRepository $userRepository, UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->userRepository = $userRepository;
        $this->passwordEncoder = $passwordEncoder;
    }

    /**
     * @param ResetPassword $message
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function __invoke(ResetPassword $message): void
    {
        $user = $this->userRepository->find($message->getUserId());

        $encodedPassword = $this->passwordEncoder->encodePassword($user, $message->newPassword);

        $user
            ->setPassword($encodedPassword)
            ->setResetPasswordToken(null);

        $this->userRepository->save();
    }
}
