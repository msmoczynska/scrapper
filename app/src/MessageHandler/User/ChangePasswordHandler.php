<?php
declare(strict_types=1);

namespace App\MessageHandler\User;

use App\Message\User\ChangePassword;
use App\Repository\UserRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class ChangePasswordHandler implements MessageHandlerInterface
{
    private UserRepository $userRepository;
    private UserPasswordEncoderInterface $passwordEncoder;

    /**
     * ChangePasswordHandler constructor.
     * @param UserRepository $userRepository
     * @param UserPasswordEncoderInterface $passwordEncoder
     */
    public function __construct(UserRepository $userRepository, UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->userRepository = $userRepository;
        $this->passwordEncoder = $passwordEncoder;
    }

    /**
     * @param ChangePassword $message
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function __invoke(ChangePassword $message): void
    {
        $user = $this->userRepository->find($message->getUserId());

        $encodedPassword = $this->passwordEncoder->encodePassword($user, $message->newPassword);

        $user->setPassword($encodedPassword);
        $this->userRepository->save();
    }
}
