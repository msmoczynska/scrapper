<?php
declare(strict_types=1);

namespace App\MessageHandler\Source;

use App\Entity\Source;
use App\Exception\SourceAlreadyExistException;
use App\Exception\ValidationException;
use App\Message\Source\CreateSource;
use App\Service\SourceCreator;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class CreateSourceHandler implements MessageHandlerInterface
{
    private SourceCreator $sourceCreator;

    /**
     * CreateSourceHandler constructor.
     * @param SourceCreator $sourceCreator
     */
    public function __construct(SourceCreator $sourceCreator)
    {
        $this->sourceCreator = $sourceCreator;
    }

    /**
     * @param CreateSource $createSource
     * @return Source
     * @throws ORMException
     * @throws ValidationException
     * @throws SourceAlreadyExistException
     * @throws OptimisticLockException
     */
    public function __invoke(CreateSource $createSource): Source
    {
//        TODO: make a wrapper on messageBus that will pull the verse from the last message
//              or use the HandleTrait provided by the messenger
        return $this->sourceCreator->createSource($createSource->id, $createSource->path, $createSource->sections);
    }
}
