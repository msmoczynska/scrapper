<?php
declare(strict_types=1);

namespace App\Exception;

use Exception;

class UserNotFoundException extends Exception
{
    /**
     * UserNotFoundException constructor.
     * @param string $message
     */
    public function __construct($message = "User not found.")
    {
        parent::__construct($message);
    }
}
