<?php
declare(strict_types=1);

namespace App\Exception;

use Exception;

class MissingScrapperException extends Exception
{
    /**
     * MissingScrapperException constructor.
     * @param string $message
     */
    public function __construct($message = "No suitable scrapper was found.")
    {
        parent::__construct($message);
    }
}
