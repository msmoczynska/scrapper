<?php
declare(strict_types=1);

namespace App\Exception;

use Exception;

class NoResultsForRegexException extends Exception
{
    /**
     * NoResultsExceptionException constructor.
     * @param string $message
     */
    public function __construct($message = "No results for regex.")
    {
        parent::__construct($message);
    }
}
