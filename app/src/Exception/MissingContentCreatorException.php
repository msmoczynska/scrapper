<?php
declare(strict_types=1);

namespace App\Exception;

use Exception;

class MissingContentCreatorException extends Exception
{
    /**
     * MissingContentCreatorException constructor.
     * @param string $message
     */
    public function __construct($message = "No suitable content creator was found.")
    {
        parent::__construct($message);
    }
}
