<?php
declare(strict_types=1);

namespace App\Exception;

use Exception;

class MissingContentException extends Exception
{
    /**
     * ContentFetcherException constructor.
     * @param string $message
     */
    public function __construct($message = "Content for given path is not exist.")
    {
        parent::__construct($message);
    }
}
