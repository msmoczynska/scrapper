<?php
declare(strict_types=1);

namespace App\Exception;

use Exception;

class UserAlreadyExistException extends Exception
{
    /**
     * UserAlreadyExistException constructor.
     * @param string $message
     */
    public function __construct($message = "User with given email already exists.")
    {
        parent::__construct($message);
    }
}
