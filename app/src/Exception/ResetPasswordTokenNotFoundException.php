<?php
declare(strict_types=1);

namespace App\Exception;

use Exception;

class ResetPasswordTokenNotFoundException extends Exception
{
    /**
     * ResetPasswordTokenNotFoundException constructor.
     * @param string $message
     */
    public function __construct($message = "Password reset has failed. Please try the \"forgot password\" option again.")
    {
        parent::__construct($message);
    }
}
