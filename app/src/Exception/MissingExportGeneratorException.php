<?php
declare(strict_types=1);

namespace App\Exception;

use Exception;

class MissingExportGeneratorException extends Exception
{
    /**
     * MissingExportGeneratorException constructor.
     * @param string $message
     */
    public function __construct($message = "No suitable export generator was found.")
    {
        parent::__construct($message);
    }
}
