<?php
declare(strict_types=1);

namespace App\Exception;

use Exception;

class SourceAlreadyExistException extends Exception
{
    /**
     * SourceAlreadyExistException constructor.
     * @param string $message
     */
    public function __construct($message = "That source already exist.")
    {
        parent::__construct($message);
    }
}
