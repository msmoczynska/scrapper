<?php
declare(strict_types=1);

namespace App\Exception;

use Exception;

class UserNotVerifiedException extends Exception
{
    /**
     * UserNotVerifiedException constructor.
     * @param string $message
     */
    public function __construct($message = "User not verified - check the e-mail box and click the link confirming registration.")
    {
        parent::__construct($message);
    }
}
