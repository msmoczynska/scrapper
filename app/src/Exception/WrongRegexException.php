<?php
declare(strict_types=1);

namespace App\Exception;

use Exception;

class WrongRegexException extends Exception
{
    /**
     * WrongRegexException constructor.
     * @param string $message
     */
    public function __construct($message = "Invalid regex.")
    {
        parent::__construct($message);
    }
}
