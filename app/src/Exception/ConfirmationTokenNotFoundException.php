<?php
declare(strict_types=1);

namespace App\Exception;

use Exception;

class ConfirmationTokenNotFoundException extends Exception
{
    /**
     * ConfirmationTokenNotFoundException constructor.
     * @param string $message
     */
    public function __construct($message = "User account validation failed.")
    {
        parent::__construct($message);
    }
}
