<?php
declare(strict_types=1);

namespace App\Exception;

use Exception;

class FileNotFoundException extends Exception
{
    /**
     * FileNotFoundException constructor.
     * @param string $message
     */
    public function __construct($message = "File not found.")
    {
        parent::__construct($message);
    }
}
