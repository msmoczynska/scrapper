<?php
declare(strict_types=1);

namespace App\Messenger\Middleware;

use App\Exception\ValidationException;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\Middleware\MiddlewareInterface;
use Symfony\Component\Messenger\Middleware\StackInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ValidationMiddleware implements MiddlewareInterface
{
    private ValidatorInterface $validator;

    /**
     * PasswordValidationMiddleware constructor.
     * @param ValidatorInterface $validator
     */
    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    /**
     * @param Envelope $envelope
     * @param StackInterface $stack
     * @return Envelope
     * @throws ValidationException
     */
    public function handle(Envelope $envelope, StackInterface $stack): Envelope
    {
        $errors = $this->validator->validate($envelope->getMessage());

        if (0 < count($errors)) {
            throw new ValidationException($errors);
        }

        return $stack->next()->handle($envelope, $stack);
    }
}
