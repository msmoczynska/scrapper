<?php
declare(strict_types=1);

namespace App\Entity;

use DateTime;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\NewsRepository")
 */
class News
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid", unique=true)
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Type("string")
     * @Assert\Length(max="255")
     */
    private $title;

    /**
     * @ORM\Column(type="datetime")
     * @Assert\Type("DateTimeInterface")
     * @Serializer\Type("DateTime<'Y-m-d H:i'>")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Assert\Type("DateTimeInterface")
     * @Serializer\Type("DateTime<'Y-m-d H:i'>")
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="boolean")
     * @Assert\NotBlank()
     * @Assert\Type("bool")
     */
    private $enabled;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Section", inversedBy="news")
     * @Assert\Valid()
     */
    private $section;

    /**
     * News constructor.
     * @param UuidInterface $id
     * @param string $title
     * @param Section $section
     * @param bool $enabled
     */
    public function __construct(UuidInterface $id, string $title, Section $section, bool $enabled = true)
    {
        $this->id = $id;
        $this->title = $title;
        $this->section = $section;
        $this->enabled = $enabled;
        $this->createdAt = new DateTime();
    }

    /**
     * @return UuidInterface
     */
    public function getId(): UuidInterface
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return News
     */
    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return DateTimeInterface
     */
    public function getCreatedAt(): DateTimeInterface
    {
        return $this->createdAt;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getUpdatedAt(): ?DateTimeInterface
    {
        return $this->updatedAt;
    }

    /**
     * @param DateTime $updatedAt
     * @return News
     */
    public function setUpdatedAt(DateTime $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @return bool
     */
    public function getEnabled(): bool
    {
        return $this->enabled;
    }

    /**
     * @param bool $enabled
     * @return $this
     */
    public function setEnabled(bool $enabled): self
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * @return Section
     */
    public function getSection(): Section
    {
        return $this->section;
    }

    /**
     * @return string
     * @Serializer\VirtualProperty()
     */
    public function getOutdated(): string
    {
        return $this->enabled ? "-" : $this->getUpdatedAt()->format("Y-m-d H:i");
    }

    /**
     * @return bool
     * @Serializer\VirtualProperty()
     */
    public function getRecent(): bool
    {
        return null === $this->updatedAt;
    }
}
