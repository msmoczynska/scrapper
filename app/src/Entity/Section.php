<?php
declare(strict_types=1);

namespace App\Entity;

use App\EntityCollection\NewsCollection;
use DateTime;
use DateTimeInterface;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 *@ORM\Entity(repositoryClass="App\Repository\SectionRepository")
 */
class Section
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid", unique=true)
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Type("string")
     * @Assert\Length(max="255")
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Type("string")
     * @Assert\Length(max="255")
     */
    private $regex;

    /**
     * @ORM\Column(type="datetime")
     * @Assert\Type("DateTimeInterface")
     */
    private $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Source", inversedBy="section")
     * @Assert\Valid()
     */
    private $source;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\News", mappedBy="section", cascade={"persist"})
     * @Assert\Valid()
     * @Serializer\Exclude()
     */
    private $news;

    /**
     * @ORM\Column(type="integer", options={"default":0})
     * @Assert\Type("int")
     */
    private int $recentNewsCount = 0;

    /**
     * @ORM\Column(type="integer", options={"default":0})
     * @Assert\Type("int")
     */
    private int $activeNewsCount = 0;

    /**
     * @ORM\Column(type="integer", options={"default":0})
     * @Assert\Type("int")
     */
    private int $outdatedNewsCount = 0;

    /**
     * @ORM\Column(type="integer", options={"default":0})
     * @Assert\Type("int")
     */
    private int $allNewsCount = 0;

    /**
     * Section constructor.
     * @param UuidInterface $id
     * @param string $name
     * @param string $regex
     * @param Source $source
     */
    public function __construct(UuidInterface $id, string $name, string $regex, Source $source)
    {
        $this->id = $id;
        $this->name = $name;
        $this->regex = $regex;
        $this->source = $source;
        $this->news = new NewsCollection();
        $this->createdAt = new DateTime();
    }

    /**
     * @return UuidInterface
     */
    public function getId(): UuidInterface
    {
        return $this->id;
    }

    /**
     * @return null|string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @return null|string
     */
    public function getRegex(): ?string
    {
        return $this->regex;
    }

    /**
     * @return DateTimeInterface
     */
    public function getCreatedAt(): DateTimeInterface
    {
        return $this->createdAt;
    }

    /**
     * @return int
     * @Serializer\VirtualProperty()
     */
    public function getDaysSinceInTheSystem(): int
    {
        return $this->createdAt->diff(new DateTime())->days;
    }

    /**
     * @return Source|null
     */
    public function getSource(): ?Source
    {
        return $this->source;
    }

    /**
     * @return Collection
     */
    public function getNews(): Collection
    {
        return $this->news;
    }

    /**
     * @return int
     */
    public function getRecentNewsCount(): int
    {
        return $this->recentNewsCount;
    }

    /**
     * @param int $recentNewsCount
     * @return Section
     */
    public function setRecentNewsCount(int $recentNewsCount): self
    {
        $this->recentNewsCount = $recentNewsCount;

        return $this;
    }

    /**
     * @return int
     */
    public function getActiveNewsCount(): int
    {
        return $this->activeNewsCount;
    }

    /**
     * @param int $activeNewsCount
     * @return Section
     */
    public function setActiveNewsCount(int $activeNewsCount): self
    {
        $this->activeNewsCount = $activeNewsCount;

        return $this;
    }

    /**
     * @return int
     */
    public function getOutdatedNewsCount(): int
    {
        return $this->outdatedNewsCount;
    }

    /**
     * @param int $outdatedNewsCount
     * @return Section
     */
    public function setOutdatedNewsCount(int $outdatedNewsCount): self
    {
        $this->outdatedNewsCount = $outdatedNewsCount;

        return $this;
    }

    /**
     * @return int
     */
    public function getAllNewsCount(): int
    {
        return $this->allNewsCount;
    }

    /**
     * @param $allNewsCount
     * @return Section
     */
    public function setAllNewsCount($allNewsCount): self
    {
        $this->allNewsCount = $allNewsCount;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getEnabledNews(): Collection
    {
        return $this->news->filter(function (News $news) {
            return true === $news->getEnabled();
        });
    }

    /**
     * @param NewsCollection $recentNewsCollection
     */
    public function appendNews(NewsCollection $recentNewsCollection): void
    {
        foreach ($recentNewsCollection as $recentNews) {
            if (!$this->news->contains($recentNews)) {
                $this->news->add($recentNews);
            }
        }
    }
}
