<?php
declare(strict_types=1);

namespace App\Entity;

use App\EntityCollection\NewsCollection;
use App\EntityCollection\Sections;
use DateTime;
use DateTimeInterface;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SourceRepository")
 */
class Source
{
    public const TYPE_HTTP = 'http';
    public const TYPE_FILE = 'file';

    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid", unique=true)
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Type("string")
     * @Assert\Length(max="255")
     */
    private $path;

    /**
     * @ORM\Column(type="string", length=10)
     * @Assert\NotBlank()
     * @Assert\Type("string")
     * @Assert\Length(max="10")
     */
    private $type;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Type("string")
     * @Assert\Length(max="255")
     */
    private $title;

    /**
     * @ORM\Column(type="datetime")
     * @Assert\NotBlank()
     * @Assert\Type("DateTimeInterface")
     */
    private $createdAt;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Section", mappedBy="source", cascade={"persist"})
     * @Assert\Valid()
     */
    private $sections;

    /**
     * @ORM\Column(type="integer", options={"default":0})
     * @Assert\Type("int")
     */
    private int $recentNewsCount = 0;

    /**
     * @ORM\Column(type="integer", options={"default":0})
     * @Assert\Type("int")
     */
    private int $activeNewsCount = 0;

    /**
     * @ORM\Column(type="integer", options={"default":0})
     * @Assert\Type("int")
     */
    private int $outdatedNewsCount = 0;

    /**
     * @ORM\Column(type="integer", options={"default":0})
     * @Assert\Type("int")
     */
    private int $allNewsCount = 0;

    /**
     * Source constructor.
     * @param UuidInterface $id
     * @param string $path
     * @param string $type
     * @param string $title
     */
    public function __construct(UuidInterface $id, string $path, string $type, string $title)
    {
        $this->id = $id;
        $this->path = $path;
        $this->type = $type;
        $this->title = $title;
        $this->sections = new Sections();
        $this->createdAt = new DateTime();
    }

    /**
     * @return UuidInterface
     */
    public function getId(): UuidInterface
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return DateTimeInterface
     */
    public function getCreatedAt(): DateTimeInterface
    {
        return $this->createdAt;
    }

    /**
     * @return int
     * @Serializer\VirtualProperty()
     */
    public function getDaysSinceInTheSystem(): int
    {
        return $this->createdAt->diff(new DateTime())->days;
    }

    /**
     * @return Collection
     */
    public function getSections(): Collection
    {
        return $this->sections;
    }

    /**
     * @param Section $section
     */
    public function addSection(Section $section): void
    {
        if (!$this->sections->contains($section)) {
            $this->sections->add($section);
        }
    }

    /**
     * @return int
     */
    public function getRecentNewsCount(): int
    {
        return $this->recentNewsCount;
    }

    /**
     * @param int $recentNewsCount
     * @return Source
     */
    public function setRecentNewsCount(int $recentNewsCount): self
    {
        $this->recentNewsCount = $recentNewsCount;

        return $this;
    }

    /**
     * @return int
     */
    public function getActiveNewsCount(): int
    {
        return $this->activeNewsCount;
    }

    /**
     * @param int $activeNewsCount
     * @return Source
     */
    public function setActiveNewsCount(int $activeNewsCount): self
    {
        $this->activeNewsCount = $activeNewsCount;

        return $this;
    }

    /**
     * @return int
     */
    public function getOutdatedNewsCount(): int
    {
        return $this->outdatedNewsCount;
    }

    /**
     * @param int $outdatedNewsCount
     * @return Source
     */
    public function setOutdatedNewsCount(int $outdatedNewsCount): self
    {
        $this->outdatedNewsCount = $outdatedNewsCount;

        return $this;
    }

    /**
     * @return int
     */
    public function getAllNewsCount(): int
    {
        return $this->allNewsCount;
    }

    /**
     * @param $allNewsCount
     * @return Source
     */
    public function setAllNewsCount($allNewsCount): self
    {
        $this->allNewsCount = $allNewsCount;

        return $this;
    }

    /**
     * @return NewsCollection
     */
    public function getAllNews(): NewsCollection
    {
        $sourceNewsCollection = [];

        /** @var Sections $sectionNewsCollection */
        foreach ($this->sections as $section) {
            $sectionNewsCollection = $section->getNews()->toArray();
            $sourceNewsCollection = array_merge($sourceNewsCollection, $sectionNewsCollection);
        }

        return new NewsCollection($sourceNewsCollection);
    }
}
