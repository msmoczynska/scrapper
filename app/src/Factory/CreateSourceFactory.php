<?php
declare(strict_types=1);

namespace App\Factory;

use App\Exception\FileNotFoundException;
use App\Exception\FileValidationException;
use App\File\FileUploader;
use App\Message\Source\CreateSource;
use Symfony\Component\HttpFoundation\Request;

class CreateSourceFactory
{
    private FileUploader $fileUploader;

    /**
     * SourceFactory constructor.
     * @param FileUploader $fileUploader
     */
    public function __construct(FileUploader $fileUploader)
    {
        $this->fileUploader = $fileUploader;
    }

    /**
     * @param Request $request
     * @return CreateSource
     */
    public function createFromHttp(Request $request): CreateSource
    {
        $data = json_decode($request->getContent(), true);

        return CreateSource::create($data['path'], $data['sections']);
    }

    /**
     * @param Request $request
     * @return CreateSource
     * @throws FileNotFoundException
     * @throws FileValidationException
     */
    public function createFromFile(Request $request): CreateSource
    {
        $sections = json_decode($request->request->get('sections'), true);
        $file = $request->files->get('file');

        if (null === $file) {
            throw new FileNotFoundException();
        }

        $data['path'] = $this->fileUploader->upload($file);
        $data['sections'] = $sections;

        return CreateSource::create($data['path'], $data['sections']);
    }
}
