<?php
declare(strict_types=1);

namespace App\Tests\functional\Controller\Web;

use App\Entity\User;
use App\Exception\MissingScrapperException;
use App\Repository\UserRepository;
use App\Tests\support\CustomWebTestCase;

class RegistrationControllerTest extends CustomWebTestCase
{
    /**
     * @throws MissingScrapperException
     */
    public function testRenderRegistrationView(): void
    {
        $this->client->request('GET', '/register');

        $this->assertResponseStatusCodeSame(200);
        $this->assertStringContainsString('<h1>Register:</h1>', $this->getResponseContent());
    }

    /**
     * @throws MissingScrapperException
     */
    public function testRenderConfirmRegistrationView(): void
    {
        $userRepository = $this->getService(UserRepository::class);

        /** @var User $user */
        $user = $userRepository->findOneBy(['email' => 'email0@email.pl']);

        $this->client->request('GET', '/confirm-registration/' . $user->getConfirmationToken());

        $this->assertResponseStatusCodeSame(200);
        $this->assertStringContainsString('Your registration at httpscrapper.com has been confirmed.', $this->getResponseContent());
    }

    /**
     * @throws MissingScrapperException
     */
    public function testRenderConfirmRegistrationViewWhenTokenNotFound(): void
    {
        $this->client->request('GET', '/confirm-registration/wrongConfirmationToken');

        $this->assertStringContainsString('Confirmation of your registration on httpscrapper.com has failed.', $this->getResponseContent());
    }
}
