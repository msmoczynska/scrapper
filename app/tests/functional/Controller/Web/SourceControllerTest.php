<?php
declare(strict_types=1);

namespace App\Tests\functional\Controller\Web;

use App\Exception\MissingScrapperException;
use App\Tests\support\CustomWebTestCase;

class SourceControllerTest extends CustomWebTestCase
{
    /**
     * @throws MissingScrapperException
     */
    public function testCreateFromHttp(): void
    {
        $this->client->request('GET', '/add-new-http');

        $this->assertResponseStatusCodeSame(200);
        $this->assertStringContainsString('<ul class="navbar-nav">', $this->getResponseContent());
    }

    /**
     * @throws MissingScrapperException
     */
    public function testCreateFromFile(): void
    {
        $this->client->request('GET', '/add-new-file');

        $this->assertResponseStatusCodeSame(200);
        $this->assertStringContainsString('<ul class="navbar-nav">', $this->getResponseContent());
    }

    /**
     * @throws MissingScrapperException
     */
    public function testList(): void
    {
        $this->client->request('GET', '/source');

        $this->assertResponseStatusCodeSame(200);
        $this->assertStringContainsString('<div id="source-list-field">', $this->getResponseContent());
    }
}
