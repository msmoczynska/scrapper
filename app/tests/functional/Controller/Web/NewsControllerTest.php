<?php
declare(strict_types=1);

namespace App\Tests\functional\Controller\Web;

use App\Exception\MissingScrapperException;
use App\Tests\support\CustomWebTestCase;

class NewsControllerTest extends CustomWebTestCase
{
    /**
     * @throws MissingScrapperException
     */
    public function testList(): void
    {
        $this->client->request('GET', '/news');

        $this->assertResponseIsSuccessful();
        $this->assertStringContainsString('<div id="news-list-field">', $this->getResponseContent());
    }
}
