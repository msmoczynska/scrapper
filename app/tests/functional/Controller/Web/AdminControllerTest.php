<?php
declare(strict_types=1);

namespace App\Tests\functional\Controller\Web;

use App\Exception\MissingScrapperException;
use App\Tests\support\CustomWebTestCase;

class AdminControllerTest extends CustomWebTestCase
{
    /**
     * @throws MissingScrapperException
     */
    public function testAdminPanel(): void
    {
        $this->logIn();
        $this->client->request('GET', '/admin-panel');

        $this->assertResponseIsSuccessful();
        $this->assertStringContainsString('Welcome to the Scrapper admin panel!', $this->getResponseContent());
    }

    /**
     * @throws MissingScrapperException
     */
    public function testAdminPanelForUnauthorizedUser(): void
    {
        $this->logIn('email0@email.pl');
        $this->client->request('GET', '/admin-panel');

        $this->assertResponseStatusCodeSame(403);
        $this->assertStringContainsString('Access Denied', $this->getResponseContent());
    }
}
