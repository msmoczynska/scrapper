<?php
declare(strict_types=1);

namespace App\Tests\functional\Controller\Web;

use App\Entity\User;
use App\Exception\MissingScrapperException;
use App\Repository\UserRepository;
use App\Tests\support\CustomWebTestCase;

class PasswordControllerTest extends CustomWebTestCase
{
    /**
     * @throws MissingScrapperException
     */
    public function testRenderForgotPasswordView(): void
    {
        $this->client->request('GET', '/forgot-password');

        $this->assertResponseStatusCodeSame(200);
        $this->assertStringContainsString('<button id="reset-password"', $this->getResponseContent());
    }

    /**
     * @throws MissingScrapperException
     */
    public function testRenderResetPasswordViewForUser(): void
    {
        $userRepository = $this->getService(UserRepository::class);

        /** @var User $user */
        $user = $userRepository->findOneBy(['email' => 'email0@email.pl']);
        $user->setResetPasswordToken('resetPasswordToken');
        $userRepository->save();

        $this->client->request('GET', '/reset-password/' . $user->getResetPasswordToken());

        $this->assertResponseStatusCodeSame(200);
        $this->assertStringContainsString('Enter your password below to set your new password:', $this->getResponseContent());
    }

    /**
     * @throws MissingScrapperException
     */
    public function testRenderResetPasswordViewWhenTokenNotFound(): void
    {
        $this->client->request('GET', '/reset-password/wrongResetPasswordToken');

        $this->assertStringContainsString('Password reset has failed.', $this->getResponseContent());
    }

    /**
     * @throws MissingScrapperException
     */
    public function testRenderChangePasswordViewForUser(): void
    {
        $this->logIn();
        $this->client->request('GET', '/change-password');

        $this->assertResponseStatusCodeSame(200);
        $this->assertStringContainsString('<h3>You want to change your password?</h3>', $this->getResponseContent());
    }

    /**
     * @throws MissingScrapperException
     */
    public function testRenderChangePasswordViewForUnauthorizedUser(): void
    {
        $this->client->request('GET', '/change-password');

        $this->assertResponseStatusCodeSame(403);
        $this->assertStringContainsString('Access Denied', $this->getResponseContent());
    }
}
