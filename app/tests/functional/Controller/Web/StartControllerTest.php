<?php
declare(strict_types=1);

namespace App\Tests\functional\Controller\Web;

use App\Exception\MissingScrapperException;
use App\Tests\support\CustomWebTestCase;

class StartControllerTest extends CustomWebTestCase
{
    /**
     * @throws MissingScrapperException
     */
    public function testHomePage(): void
    {
        $this->client->request('GET', '/');

        $this->assertResponseStatusCodeSame(200);
        $this->assertStringContainsString('>Register</a>', $this->getResponseContent());
    }

    /**
     * @throws MissingScrapperException
     */
    public function testAfterLogin(): void
    {
        $this->logIn();
        $this->client->request('GET', '/after-login');

        $this->assertResponseStatusCodeSame(200);
        $this->assertStringContainsString('Now you can use the admin panel here:', $this->getResponseContent());
    }

    /**
     * @throws MissingScrapperException
     */
    public function testAfterLoginForUnauthorizedUser(): void
    {
        $this->client->request('GET', '/after-login');

        $this->assertResponseStatusCodeSame(403);
        $this->assertStringContainsString('Access Denied', $this->getResponseContent());
    }
}
