<?php
declare(strict_types=1);

namespace App\Tests\functional\Controller\Web;

use App\Exception\MissingScrapperException;
use App\Tests\support\CustomWebTestCase;

class AuthControllerTest extends CustomWebTestCase
{
    /**
     * @throws MissingScrapperException
     */
    public function testLogin(): void
    {
        $this->client->request('GET', '/login');

        $this->assertResponseStatusCodeSame(200);
        $this->assertStringContainsString('Please sign in:</h1>', $this->getResponseContent());
    }

    public function testLoginWhenUserLogged(): void
    {
        $this->logIn();

        $this->client->request('GET', '/login');

        $this->assertResponseStatusCodeSame(302);
        $this->assertResponseRedirects('/after-login');
    }

    public function testLogout(): void
    {
        $this->logIn();
        $this->client->request('GET', '/after-login');

        $this->assertResponseStatusCodeSame(200);

        $this->client->request('GET', '/logout');

        $this->assertResponseStatusCodeSame(302);

        $this->client->request('GET', '/after-login');

        $this->assertResponseStatusCodeSame(403);
    }
}
