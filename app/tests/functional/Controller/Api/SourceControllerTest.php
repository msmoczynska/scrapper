<?php
declare(strict_types=1);

namespace App\Tests\functional\Controller\Api;

use App\Entity\Source;
use App\Exception\MissingScrapperException;
use App\Repository\SourceRepository;
use App\Tests\support\ContentFetcherMock;
use App\Tests\support\CustomWebTestCase;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class SourceControllerTest extends CustomWebTestCase
{
    /**
     * @throws MissingScrapperException
     */
    public function testCreateFromHttp(): void
    {
        $this->logIn('admin@email.pl');

        $data = [
            'path' => 'www.test-url.com',
            'sections' => [
                'section1-name' => '/<p class="tytul">\s+(?P<ref>[^"]*)<\/p>/',
                'section2-name' => '/<p class="tytul">\s+(?P<ref>[^"]*)<\/p>/',
            ]
        ];

        $content = "<p class=\"tytul\">News1</p><p class=\"tytul\">News2</p><p class=\"tytul\">News3</p><title>Api-test-Source-title</title>";

        ContentFetcherMock::addNewContent($data['path'], $content);

        $this->client->request(
            'POST',
            '/api/add-new-http',
            [],
            [],
            ['headers' => ['Content-Type' => 'application/json']],
            json_encode($data)
        );

        $this->assertResponseIsSuccessful();
        $this->assertResponseHeaderSame('content-type', 'application/json');

        $this->assertStringContainsString('Source created', $this->getResponseContent());

        $sourceRepository = $this->getService(SourceRepository::class);

        /** @var Source $source */
        $source = $sourceRepository->findOneBy(['path' => $data['path']]);
        $sections = $source->getSections();

        $this->assertInstanceOf(Source::class, $source);
        $this->assertEquals($data['path'], $source->getPath());
        $this->assertInstanceOf(Collection::class, $sections);
        $this->assertCount(count($data['sections']), $sections);
    }

    /**
     * @throws MissingScrapperException
     */
    public function testCreateFromFile(): void
    {
        $this->logIn('admin@email.pl');

        $testSections = [
            'section1-name' => '/<p class="tytul">\s+(?P<ref>[^"]*)<\/p>/',
            'section2-name' => '/<p class="tytul">\s+(?P<ref>[^"]*)<\/p>/',
        ];

        $path = $this->prepareFileForTest("CosmeticNews", $this->projectDir . "/resources/fixtures/CosmeticNews.html");
        $pathForContentFetcherMock = $this->projectDir . "/var/files/CosmeticNews.html";
        $content = "<p class=\"tytul\">News1</p><p class=\"tytul\">News2</p><p class=\"tytul\">News3</p><title>Api-test-Source-title</title>";

        ContentFetcherMock::addNewContent($pathForContentFetcherMock, $content);

        $this->client->request(
            'POST',
            '/api/add-new-file',
            ['sections' => json_encode($testSections)],
            ['file' => new UploadedFile($path, 'CosmeticNews.html')],
        );

        $this->assertResponseIsSuccessful();
        $this->assertStringContainsString('Source created', $this->getResponseContent());

        $sourceRepository = $this->getService(SourceRepository::class);

        /** @var Source $source */
        $source = $sourceRepository->findOneBy(['path' => $pathForContentFetcherMock]);
        $sections = $source->getSections();

        $this->assertInstanceOf(Source::class, $source);
        $this->assertEquals($pathForContentFetcherMock, $source->getPath());
        $this->assertInstanceOf(Collection::class, $sections);
        $this->assertCount(count($testSections), $sections);
    }

    public function testCreateFromFileWithFileNull(): void
    {
        $this->logIn('admin@email.pl');

        $testSections = [
            'section1-name' => '/<p class="tytul">\s+(?P<ref>[^"]*)<\/p>/',
            'section2-name' => '/<p class="tytul">\s+(?P<ref>[^"]*)<\/p>/',
        ];

        $this->client->request(
            'POST',
            '/api/add-new-file',
            ['sections' => json_encode($testSections)],
            [],
            $this->asyncRequest()
        );

        $this->assertEquals("File not found.", $this->grabJsonBodyFromResponse()["message"]);
    }

    public function testList(): void
    {
        $this->client->request(
            'GET',
            '/api/source',
            ['limit' => '10', 'page' => '1']
        );

        $content = $this->grabJsonBodyFromResponse();

        $this->assertResponseIsSuccessful();
        $this->assertCount(5 , $content["items"]);
        $this->assertEquals(
            [
                "limit" => 10,
                "page" => 1,
                "pages" => 1,
                "selected" => 5,
                "total" => 5,
                "firstOrdinalNumber" => 1,
            ],
            $content["pagination"]
        );
    }
}
