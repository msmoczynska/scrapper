<?php
declare(strict_types=1);

namespace App\Tests\functional\Controller\Api;

use App\Tests\support\CustomWebTestCase;

class NewsControllerTest extends CustomWebTestCase
{
    public function testList(): void
    {
        $this->client->request(
            'GET',
            '/api/news',
            ['limit' => '10', 'page' => '4']
        );

        $content = $this->grabJsonBodyFromResponse();

        $this->assertResponseIsSuccessful();
        $this->assertCount(10 , $content["items"]);
        $this->assertEquals(
            [
                "limit" => 10,
                "page" => 4,
                "pages" => 5,
                "selected" => 10,
                "total" => 48,
                "firstOrdinalNumber" => 31,
            ],
            $content["pagination"]
        );
    }
}
