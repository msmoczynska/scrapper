<?php
declare(strict_types=1);

namespace App\Tests\functional\Controller\Api;

use App\Entity\User;
use App\Exception\MissingScrapperException;
use App\Repository\UserRepository;
use App\Tests\support\CustomWebTestCase;
use App\Tests\support\SwiftMailerAdapterMock;

class RegistrationControllerTest extends CustomWebTestCase
{
    /**
     * @throws MissingScrapperException
     */
    public function testRegister(): void
    {
        $this->client->request(
            'POST',
            '/api/register',
            [
                'email' => 'newEmail@email.com',
                'password' => 'qwerty1234',
                'firstName' => 'newFirstName',
                'secondName' => 'newSecondName',
                'birthDate' => '1998-11-22',
                'agreeTerms' => 'on',
            ],
        );

        $this->assertResponseStatusCodeSame(201);
        $this->assertStringContainsString('User created', $this->getResponseContent());
        $this->assertStringContainsString('To confirm your registration', SwiftMailerAdapterMock::getLastSentMessage()->toString());

        $userRepository = $this->getService(UserRepository::class);

        /** @var User $user */
        $user = $userRepository->findOneBy(['email' => 'newEmail@email.com']);

        $this->assertInstanceOf(User::class, $user);
        $this->assertEquals('newFirstName', $user->getFirstName());
    }
}
