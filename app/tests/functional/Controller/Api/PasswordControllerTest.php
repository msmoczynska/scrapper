<?php
declare(strict_types=1);

namespace App\Tests\functional\Controller\Api;

use App\Entity\User;
use App\Exception\MissingScrapperException;
use App\Repository\UserRepository;
use App\Tests\support\CustomWebTestCase;
use App\Tests\support\SwiftMailerAdapterMock;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class PasswordControllerTest extends CustomWebTestCase
{
    /**
     * @throws MissingScrapperException
     */
    public function testSendResetPasswordEmail(): void
    {
        $this->client->request(
            'GET',
            '/api/forgot-password/email0@email.pl',
        );

        $this->assertResponseStatusCodeSame(200);
        $this->assertStringContainsString('Check your e-mail box', $this->getResponseContent());
        $this->assertStringContainsString('To reset your password, click on the link:', SwiftMailerAdapterMock::getLastSentMessage()->toString());

        $userRepository = $this->getService(UserRepository::class);

        /** @var User $user */
        $user = $userRepository->findOneBy(['email' => 'email0@email.pl']);

        $this->assertNotNull($user->getResetPasswordToken());
        $this->assertIsString($user->getResetPasswordToken());
    }

    /**
     * @throws MissingScrapperException
     */
    public function testForgotPasswordWhenUserNotFound(): void
    {
        $this->client->request(
            'GET',
            '/api/forgot-password/wrong@email.com',
            [],
            [],
            $this->asyncRequest()
        );

        $this->assertStringContainsString('User not found.', $this->getResponseContent());
    }

    /**
     * @throws MissingScrapperException
     */
    public function testResetPassword(): void
    {
        $this->logIn('email0@email.pl');

        $this->client->request(
            'POST',
            '/api/reset-password',
            [
                'newPassword' => 'qwerty1234',
                'confirmNewPassword' => 'qwerty1234',
            ],
        );

        $this->assertResponseStatusCodeSame(200);
        $this->assertStringContainsString('New password set', $this->getResponseContent());

        $userRepository = $this->getService(UserRepository::class);

        /** @var User $user */
        $user = $userRepository->findOneBy(['email' => 'email0@email.pl']);

        $this->assertNull($user->getResetPasswordToken());
    }

    /**
     * @throws MissingScrapperException
     */
    public function testChangePassword(): void
    {
        $this->logIn('email0@email.pl');

        $newPassword = 'qwerty1234';

        $this->client->request(
            'POST',
            '/api/change-password',
            [
                'oldPassword' => 'aqwerty0',
                'newPassword' => $newPassword,
                'confirmNewPassword' => $newPassword,
            ],
        );

        $this->assertResponseStatusCodeSame(200);
        $this->assertStringContainsString('Password changed successfully', $this->getResponseContent());

        $userRepository = $this->getService(UserRepository::class);

        /** @var User $user */
        $user = $userRepository->findOneBy(['email' => 'email0@email.pl']);

        $passwordEncoder = $this->getService(UserPasswordEncoderInterface::class);

        $this->assertTrue($passwordEncoder->isPasswordValid($user, $newPassword));
    }
}
