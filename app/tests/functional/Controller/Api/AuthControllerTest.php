<?php
declare(strict_types=1);

namespace App\Tests\functional\Controller\Api;

use App\Exception\MissingScrapperException;
use App\Tests\support\CustomWebTestCase;

class AuthControllerTest extends CustomWebTestCase
{
    /**
     * @throws MissingScrapperException
     */
    public function testLogin(): void
    {
        $this->client->request('GET', '/after-login',);

        $this->assertResponseStatusCodeSame(403);
        $this->assertStringContainsString('Access Denied', $this->getResponseContent());

        $this->client->request(
            'POST',
            '/api/login',
            [
                'email' => 'admin@email.pl',
                'password' => 'qwerty1234',
            ],
        );

        $this->assertResponseStatusCodeSame(200);
        $this->assertStringContainsString('You are successfully logged in!', $this->getResponseContent());

        $this->client->request('GET', '/after-login',);

        $this->assertResponseStatusCodeSame(200);
        $this->assertStringContainsString('Now you can use the admin panel here:', $this->getResponseContent());
    }
}
