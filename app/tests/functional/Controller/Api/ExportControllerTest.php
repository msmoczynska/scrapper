<?php
declare(strict_types=1);

namespace App\Tests\functional\Controller\Api;

use App\Enum\ExportDetails;
use App\Tests\support\CustomWebTestCase;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;

class ExportControllerTest extends CustomWebTestCase
{
    public function testExportSourceList(): void
    {
        $this->client->request(
            'GET',
            '/source/' . ExportDetails::XLSX_FORMAT . '/' . ExportDetails::SOURCE_TYPE
        );

        $fileName = $this->client->getResponse()->getFile()->getFilename();

        $this->assertResponseStatusCodeSame(200);
        $this->assertStringContainsString(ExportDetails::SOURCE_TYPE . 'List.xlsx', $fileName);

        $reader = new Xlsx();
        $filePath = $this->client->getResponse()->getFile()->getPath();
        $spreadsheet = $reader->load($filePath . '/' . $fileName);

        $this->assertEquals('Section', $spreadsheet->getActiveSheet()->getCellByColumnAndRow(3, 1));
        $this->assertEquals('Outdated news', $spreadsheet->getActiveSheet()->getCellByColumnAndRow(7, 1));
        $this->assertStringContainsString('CosmeticNews', (string)$spreadsheet->getActiveSheet()->getCellByColumnAndRow(2, 2));
        $this->assertEquals('5', $spreadsheet->getActiveSheet()->getCellByColumnAndRow(1, 10));
        $this->assertEquals('H', $spreadsheet->getActiveSheet()->getCellCollection()->getHighestColumn());
        $this->assertEquals(12, $spreadsheet->getActiveSheet()->getCellCollection()->getHighestRow());
    }
}
