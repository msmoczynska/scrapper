<?php
declare(strict_types=1);

namespace App\Tests\integration\ParamConverter;

use App\Enum\ExportDetails;
use App\Message\Export\GenerateExportFile;
use App\Message\MessageInterface;
use App\Message\User\CreateUser;
use App\ParamConverter\RequestToMessageParamConverter;
use App\Tests\support\CustomWebTestCase;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;

class RequestToMessageParamConverterTest extends CustomWebTestCase
{
    /**
     * @dataProvider provideDataForTestIsDataAddedToRequest
     * @param Request $request
     * @param ParamConverter $configuration
     * @param MessageInterface $expectedMessage
     */
    public function testIsDataAddedToRequest(Request $request, ParamConverter $configuration, MessageInterface $expectedMessage): void
    {
        $requestToMessageParamConverter = $this->getService(RequestToMessageParamConverter::class);
        $requestToMessageParamConverter->apply($request, $configuration);

        $this->assertEquals(
            $expectedMessage,
            $request->attributes->get("message")
        );
    }

    /**
     * @return array
     */
    public function provideDataForTestIsDataAddedToRequest(): array
    {
        $request1 = new Request(
            [],
            [],
            ["format" => ExportDetails::XLSX_FORMAT, "type" => ExportDetails::SOURCE_TYPE]
        );

        $configuration1 = new ParamConverter([]);
        $configuration1->setName("message");
        $configuration1->setClass(GenerateExportFile::class);

        $message1 = GenerateExportFile::create(
            ExportDetails::XLSX_FORMAT,
            ExportDetails::SOURCE_TYPE
        );

        $request2 = new Request(
            [],
            [
                "email" => "test@email.com",
                "password" => "testPassword",
                "firstName" => "firstName",
                "secondName" => "secondName",
                "birthDate" => "1987-12-02",
            ]
        );

        $configuration2 = new ParamConverter([]);
        $configuration2->setName("message");
        $configuration2->setClass(CreateUser::class);

        $message2 = CreateUser::create(
            "test@email.com",
            "testPassword",
            "firstName",
            "secondName",
            "1987-12-02"
        );

        return [
            "generateExportData" => [$request1, $configuration1, $message1],
            "createUserData" => [$request2, $configuration2, $message2],
        ];
    }
}
