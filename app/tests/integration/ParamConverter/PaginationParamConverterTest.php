<?php
declare(strict_types=1);

namespace App\Tests\integration\ParamConverter;

use App\Pagination\PaginationParams;
use App\ParamConverter\PaginationParamConverter;
use App\Tests\support\CustomWebTestCase;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;

class PaginationParamConverterTest extends CustomWebTestCase
{
    /**
     * @dataProvider provideDataForTestIsPaginationParamsAddedToRequest
     * @param Request $request
     * @param ParamConverter $configuration
     * @param PaginationParams $expectedPaginationParams
     */
    public function testIsPaginationParamsAddedToRequest(
        Request $request,
        ParamConverter $configuration,
        PaginationParams $expectedPaginationParams
    ): void
    {
        $paginationParamConverter  = $this->getService(PaginationParamConverter::class);
        $paginationParamConverter->apply($request, $configuration);

        $this->assertEquals($expectedPaginationParams, $request->attributes->get('paginationParams'));
    }

    /**
     * @return array
     */
    public function provideDataForTestIsPaginationParamsAddedToRequest(): array
    {
        $request1 = new Request(["limit" => "20", "page" => "5"]);
        $request2 = new Request(["limit" => "10", "page" => "2", "search" => "test-search-phrase"]);

        $configuration = new ParamConverter([]);

        $paginationParams1 = new PaginationParams(20, 5);

        $paginationParams2 = new PaginationParams(10, 2);
        $paginationParams2->setSearchPhrase("test-search-phrase");

        return [
            "paginationData" => [$request1, $configuration, $paginationParams1],
            "paginationDataWithSearchPhrase" => [$request2, $configuration, $paginationParams2],
        ];
    }
}
