<?php
declare(strict_types=1);

namespace App\Tests\intagration\MessageHandler\Export;

use App\Enum\ExportDetails;
use App\Exception\MissingContentCreatorException;
use App\Exception\MissingExportGeneratorException;
use App\Message\Export\GenerateExportFile;
use App\MessageHandler\Export\GenerateExportFileHandler;
use App\Tests\support\CustomWebTestCase;

class GenerateExportFileHandlerTest extends CustomWebTestCase
{
    /**
     * @dataProvider provideDataForGenerateExportFile
     * @param GenerateExportFile $generateExportFile
     * @throws MissingContentCreatorException
     * @throws MissingExportGeneratorException
     */
    public function testGenerateExportFile(GenerateExportFile $generateExportFile): void
    {
        /** @var GenerateExportFileHandler $generateExportFileHandler */
        $generateExportFileHandler = $this->getService(GenerateExportFileHandler::class);

        $this->assertStringContainsString($generateExportFile->type . 'List.' . $generateExportFile->format, $generateExportFileHandler($generateExportFile));
    }

    /**
     * @return array
     */
    public function provideDataForGenerateExportFile(): array
    {
        return [
            "sourceXlsx" => [GenerateExportFile::create(ExportDetails::XLSX_FORMAT, ExportDetails::SOURCE_TYPE)],
            "sourceCsv" => [GenerateExportFile::create(ExportDetails::CSV_FORMAT, ExportDetails::SOURCE_TYPE)],
        ];
    }
}
