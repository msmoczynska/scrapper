<?php
declare(strict_types=1);

namespace App\Tests\integration\MessageHandler\User;

use App\Message\User\ChangePassword;
use App\MessageHandler\User\ChangePasswordHandler;
use App\Repository\UserRepository;
use App\Tests\support\CustomWebTestCase;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class ChangePasswordHandlerTest extends CustomWebTestCase
{
    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function testChangePassword(): void
    {
        $userRepository = $this->getService(UserRepository::class);
        $user = $userRepository->findOneBy(['email' => 'email1@email.pl']);

        $newPassword = 'newPassword';
        $message = ChangePassword::create('aqwerty1', $newPassword, $newPassword);

        /** @var ChangePasswordHandler $changePasswordHandler */
        $changePasswordHandler = $this->getService(ChangePasswordHandler::class);

        $changePasswordHandler($message->setUser($user));

        $passwordEncoder = $this->getService(UserPasswordEncoderInterface::class);

        $this->assertTrue($passwordEncoder->isPasswordValid($user, $newPassword));
    }
}
