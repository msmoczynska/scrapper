<?php
declare(strict_types=1);

namespace App\Tests\integration\MessageHandler\User;

use App\Entity\User;
use App\Exception\UserAlreadyExistException;
use App\Message\User\CreateUser;
use App\MessageHandler\User\CreateUserHandler;
use App\Tests\support\CustomWebTestCase;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Ramsey\Uuid\Uuid;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class CreateUserHandlerTest extends CustomWebTestCase
{
    /**
     * @dataProvider provideDataForCreateUser
     * @param CreateUser $createUser
     * @throws UserAlreadyExistException
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function testCreateUser(CreateUser $createUser): void
    {
        /** @var CreateUserHandler $createUserHandler */
        $createUserHandler = $this->getService(CreateUserHandler::class);

        $user = $createUserHandler($createUser);

        $this->assertInstanceOf(User::class, $user);
    }

    /**
     * @throws LoaderError
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws RuntimeError
     * @throws SyntaxError
     * @throws UserAlreadyExistException
     */
    public function testCreateUserWhenUserExist(): void
    {
        $createUser = CreateUser::create(
            'email1@email.pl',
            'password',
            'firstname',
            'secondname',
            '2000-01-01'
        );

        /** @var CreateUserHandler $createUserHandler */
        $createUserHandler = $this->getService(CreateUserHandler::class);

        $this->expectException(UserAlreadyExistException::class);

        $createUserHandler($createUser);
    }

    /**
     * @return array
     */
    public function provideDataForCreateUser(): array
    {
        $createUser1 = CreateUser::create(
            'email1@test.pl',
            'password1',
            'firstname1',
            'secondname1',
            '2000-01-01'
        );

        $createUser1->id = Uuid::uuid4();

        $createUser2 = CreateUser::create(
            'email2@test.pl',
            'password2',
            'firstname2',
            'secondname2',
            '2000-02-02'
        );

        $createUser2->id = Uuid::uuid4();

        return [
            "user1" => [$createUser1],
            "user2" => [$createUser2],
        ];
    }
}
