<?php
declare(strict_types=1);

namespace App\Tests\integration\MessageHandler\User;

use App\Exception\UserNotFoundException;
use App\Message\User\ResetPasswordRequest;
use App\MessageHandler\User\ResetPasswordRequestHandler;
use App\Repository\UserRepository;
use App\Tests\support\CustomWebTestCase;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class ResetPasswordRequestHandlerTest extends CustomWebTestCase
{
    /**
     * @throws UserNotFoundException
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function testIsRestPasswordTokenSet(): void
    {
        $userRepository = $this->getService(UserRepository::class);
        $user = $userRepository->findOneBy(['email' => 'email1@email.pl']);

        $message = ResetPasswordRequest::create($user->getEmail());

        /** @var ResetPasswordRequestHandler $resetPasswordRequestHandler */
        $resetPasswordRequestHandler = $this->getService(ResetPasswordRequestHandler::class);

        $resetPasswordRequestHandler($message);

        $this->assertNotNull($user->getResetPasswordToken());
        $this->assertIsString($user->getResetPasswordToken());
    }
}
