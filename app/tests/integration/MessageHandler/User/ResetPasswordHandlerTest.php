<?php
declare(strict_types=1);

namespace App\Tests\integration\MessageHandler\User;

use App\Message\User\ResetPassword;
use App\MessageHandler\User\ResetPasswordHandler;
use App\Repository\UserRepository;
use App\Tests\support\CustomWebTestCase;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class ResetPasswordHandlerTest extends CustomWebTestCase
{
    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function testResetPassword(): void
    {
        $userRepository = $this->getService(UserRepository::class);
        $user = $userRepository->findOneBy(['email' => 'email1@email.pl']);

        $newPassword = 'newPassword';
        $message = ResetPassword::create($newPassword, $newPassword);

        /** @var ResetPasswordHandler $resetPasswordHandler */
        $resetPasswordHandler = $this->getService(ResetPasswordHandler::class);

        $resetPasswordHandler($message->setUser($user));

        $this->assertNull($user->getResetPasswordToken());

        $passwordEncoder = $this->getService(UserPasswordEncoderInterface::class);

        $this->assertTrue($passwordEncoder->isPasswordValid($user, $newPassword));
    }
}
