<?php
declare(strict_types=1);

namespace App\Tests\integration\MessageHandler\User;

use App\Entity\User;
use App\Exception\ConfirmationTokenNotFoundException;
use App\Message\User\ConfirmUser;
use App\MessageHandler\User\ConfirmUserHandler;
use App\Repository\UserRepository;
use App\Tests\support\CustomWebTestCase;
use DateTime;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Ramsey\Uuid\Uuid;

class ConfirmUserHandlerTest extends CustomWebTestCase
{
    /**
     * @throws ConfirmationTokenNotFoundException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function testConfirmUser(): void
    {
        $user = new User(
            Uuid::uuid4(),
            'email@test.pl',
            'firstName',
            'secondName',
            new DateTime('2000-02-02'),
            'confirmationToken'
        );

        $user->setPassword('password');

        $userRepository = $this->getService(UserRepository::class);

        $userRepository->persist($user);
        $userRepository->save();

        /** @var ConfirmUserHandler $confirmUserHandler */
        $confirmUserHandler = $this->getService(ConfirmUserHandler::class);
        $message = ConfirmUser::create('confirmationToken');

        $user = $confirmUserHandler($message);

        $this->assertTrue($user->isVerified());
    }

    /**
     * @throws ConfirmationTokenNotFoundException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function testConfirmUserWhenTokenNotFound(): void
    {
        /** @var ConfirmUserHandler $confirmUserHandler */
        $confirmUserHandler = $this->getService(ConfirmUserHandler::class);
        $message = ConfirmUser::create('wrongToken');

        $this->expectException(ConfirmationTokenNotFoundException::class);

        $confirmUserHandler($message);
    }
}
