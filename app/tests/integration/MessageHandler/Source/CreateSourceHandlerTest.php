<?php
declare(strict_types=1);

namespace App\Tests\integration\MessageHandler\Source;

use App\Entity\Source;
use App\Exception\SourceAlreadyExistException;
use App\Exception\ValidationException;
use App\Message\Source\CreateSource;
use App\MessageHandler\Source\CreateSourceHandler;
use App\Tests\support\ContentFetcherMock;
use App\Tests\support\CustomWebTestCase;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;

class CreateSourceHandlerTest extends CustomWebTestCase
{
    /**
     * @throws ORMException
     * @throws ValidationException
     * @throws SourceAlreadyExistException
     * @throws OptimisticLockException
     */
    public function testCreateSource(): void
    {
        $createSource = CreateSource::create(
            'www.test-url.com',
            [
                "news" => "newsRegex",
                "sport" => "sportRegex",
                "business" => "businessRegex"
            ]
        );

        $content = "<p class=\"tytul\">              News1    </p>
         <p class=\"tytul\">              News2    </p>
         <p class=\"tytul\">              News3    </p>
        <title>Http-Source-title</title>";

        ContentFetcherMock::addNewContent($createSource->path, $content);

        /** @var CreateSourceHandler $createSourceHandler */
        $createSourceHandler = $this->getService(CreateSourceHandler::class);

        $source = $createSourceHandler($createSource);

        $this->assertInstanceOf(Source::class, $source);
        $this->assertEquals('www.test-url.com', $source->getPath());
    }
}
