<?php
declare(strict_types=1);

namespace App\Tests\integration\support;

use App\Exception\MissingContentException;
use App\Tests\support\ContentFetcherMock;
use App\Tests\support\CustomWebTestCase;

class ContentFetcherMockTest extends CustomWebTestCase
{
    public function testFetch(): void
    {
        $path = $this->projectDir . "/tests/data/fakeContent.html";
        $content = "class=\"title\">Fake news 1</a> class=\"title\">Fake news 2</a> class=\"title\">Fake news 3</a>";

        $contentFetcherMock = $this->getService(ContentFetcherMock::class);
        $contentFetcherMock::addNewContent($path, $content);

        $this->assertEquals($content, $contentFetcherMock->fetch($path));
    }

    public function testFetchWithMissingContentException(): void
    {
        $contentFetcherMock = $this->getService(ContentFetcherMock::class);

        $this->expectException(MissingContentException::class);
        $this->expectExceptionMessage("Content for given path is not exist.");

        $contentFetcherMock->fetch("/tests/data/fakeContent.html");
    }
}
