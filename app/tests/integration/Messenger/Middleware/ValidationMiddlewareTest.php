<?php
declare(strict_types=1);

namespace App\Tests\integration\Messenger\Middleware;

use App\Exception\ValidationException;
use App\Message\User\ResetPassword;
use App\Messenger\Middleware\ValidationMiddleware;
use App\Repository\UserRepository;
use App\Tests\support\CustomWebTestCase;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\Middleware\StackMiddleware;
use Symfony\Component\Messenger\Stamp\BusNameStamp;

class ValidationMiddlewareTest extends CustomWebTestCase
{
    public function testValidation(): void
    {
        $validationMiddleware = $this->getService(ValidationMiddleware::class);

        $userRepository = $this->getService(UserRepository::class);
        $user = $userRepository->findOneBy(['email' => 'email1@email.pl']);

        $message = ResetPassword::create('asdf1234', 'qwer1234');
        $stamps = [new BusNameStamp('messenger.bus.default')];

        $envelope = new Envelope($message->setUser($user), $stamps);

        $stack = new StackMiddleware();

        try {
            $validationMiddleware->handle($envelope, $stack);
        } catch (ValidationException $exception) {
            $violation = $exception->getErrors()[0];

            $validationMessage = $violation->getPropertyPath() . ": " . $violation->getMessage();
            $exceptionThrown = true;

            $this->assertEquals("confirmNewPassword: Passwords must be identical!", $validationMessage);
            $this->assertTrue($exceptionThrown);
        }
    }
}

