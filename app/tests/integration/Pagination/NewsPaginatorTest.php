<?php
declare(strict_types=1);

namespace App\Tests\integration\Pagination;

use App\Pagination\NewsPaginator;
use App\Pagination\PaginationParams;
use App\Tests\support\CustomWebTestCase;

class NewsPaginatorTest extends CustomWebTestCase
{
    /**
     * @dataProvider provideDataFotGetPaginatedList
     * @param PaginationParams $paginationParams
     * @param array $expectedPagination
     */
    public function testGetPaginatedList(PaginationParams $paginationParams, array $expectedPagination): void
    {
        $newsPaginator = $this->getService(NewsPaginator::class);

        $this->assertEquals($expectedPagination, $newsPaginator->getPaginatedList($paginationParams)->pagination);
    }

    /**
     * @return array
     */
    public function provideDataFotGetPaginatedList(): array
    {
        $paginationParams1 = new PaginationParams(10, 5);
        $expectedPagination1 = [
            "limit" => 10,
            "page" => 5,
            "pages" => 5,
            "selected" => 8,
            "total" => 48,
            "firstOrdinalNumber" => 41,
        ];

        $paginationParams2 = new PaginationParams(5, 5);
        $expectedPagination2 = [
            "limit" => 5,
            "page" => 5,
            "pages" => 10,
            "selected" => 5,
            "total" => 48,
            "firstOrdinalNumber" => 21,
        ];

        return [
            "limit10" => [$paginationParams1, $expectedPagination1],
            "limit5" => [$paginationParams2, $expectedPagination2],
        ];
    }
}
