<?php
declare(strict_types=1);

namespace App\Tests\integration\Pagination;

use App\Pagination\PaginationParams;
use App\Pagination\SourcePaginator;
use App\Tests\support\CustomWebTestCase;

class SourcePaginatorTest extends CustomWebTestCase
{
    /**
     * @dataProvider provideDataFotGetPaginatedList
     * @param PaginationParams $paginationParams
     * @param array $expectedPagination
     */
    public function testGetPaginatedList(PaginationParams $paginationParams, array $expectedPagination): void
    {
        $sourcePaginator = $this->getService(SourcePaginator::class);

        $this->assertEquals($expectedPagination, $sourcePaginator->getPaginatedList($paginationParams)->pagination);
    }

    /**
     * @return array
     */
    public function provideDataFotGetPaginatedList(): array
    {
        $paginationParams1 = new PaginationParams(10, 1);
        $expectedPagination1 = [
            "limit" => 10,
            "page" => 1,
            "pages" => 1,
            "selected" => 5,
            "total" => 5,
            "firstOrdinalNumber" => 1,
        ];

        $paginationParams2 = new PaginationParams(5, 1);
        $expectedPagination2 = [
            "limit" => 5,
            "page" => 1,
            "pages" => 1,
            "selected" => 5,
            "total" => 5,
            "firstOrdinalNumber" => 1,
        ];

        return [
            "limit10" => [$paginationParams1, $expectedPagination1],
            "limit5" => [$paginationParams2, $expectedPagination2],
        ];
    }
}
