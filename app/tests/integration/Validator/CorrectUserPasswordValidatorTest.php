<?php
declare(strict_types=1);

namespace App\Tests\integration\Validator;

use App\Message\User\ChangePassword;
use App\Repository\UserRepository;
use App\Tests\support\CustomWebTestCase;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class CorrectUserPasswordValidatorTest extends CustomWebTestCase
{
    public function testValidateForWrongPassword(): void
    {
        $userRepository = $this->getService(UserRepository::class);
        $user = $userRepository->findOneBy(['email' => 'email0@email.pl']);

        $newPassword = 'newPassword';
        $wrongOldPassword = 'wrongPassword';

        $changePassword = ChangePassword::create($wrongOldPassword, $newPassword, $newPassword)->setUser($user);

        $validator = $this->getService(ValidatorInterface::class);

        $errors = $validator->validate($changePassword);

        $this->assertCount(1, $errors);
        $this->assertStringContainsString('User password is not correct', $errors->get(0)->getMessage());
    }

    public function testValidateForCorrectPassword(): void
    {
        $userRepository = $this->getService(UserRepository::class);
        $user = $userRepository->findOneBy(['email' => 'email0@email.pl']);

        $newPassword = 'newPassword';
        $correctOldPassword = 'aqwerty0';

        $changePassword = ChangePassword::create($correctOldPassword, $newPassword, $newPassword)->setUser($user);

        $validator = $this->getService(ValidatorInterface::class);

        $errors = $validator->validate($changePassword);

        $this->assertCount(0, $errors);
    }
}
