<?php
declare(strict_types=1);

namespace App\Tests\integration\Repository;

use App\Repository\NewsRepository;
use App\Tests\support\CustomWebTestCase;

class NewsRepositoryTest extends CustomWebTestCase
{
    public function testFindItemsForPage(): void
    {
        $newsRepository = $this->getService(NewsRepository::class);

        $limit = 10;
        $firstResult = 0;
        $searchPhrase = null;

        $this->assertCount(10, $newsRepository->findItemsForPage($limit, $firstResult, $searchPhrase));
    }

    public function testFindItemsForPageWithSearchPhrase(): void
    {
        $newsRepository = $this->getService(NewsRepository::class);

        $limit = 10;
        $firstResult = 0;
        $searchPhrase = "test";

        $this->assertCount(4, $newsRepository->findItemsForPage($limit, $firstResult, $searchPhrase));
    }

    public function testCountAllItems(): void
    {
        $newsRepository = $this->getService(NewsRepository::class);
        $searchPhrase = null;

        $this->assertEquals(48, $newsRepository->countAllItems($searchPhrase));
    }

    public function testCountAllItemsWithSearchPhrase(): void
    {
        $newsRepository = $this->getService(NewsRepository::class);
        $searchPhrase = "test";

        $this->assertEquals(4, $newsRepository->countAllItems($searchPhrase));
    }
}
