<?php
declare(strict_types=1);

namespace App\Tests\integration\Repository;

use App\Entity\Source;
use App\Repository\SourceRepository;
use App\Tests\support\CustomWebTestCase;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;

class SourceRepositoryTest extends CustomWebTestCase
{
    public function testFindItemsForPage(): void
    {
        /** @var SourceRepository $sourceRepository */
        $sourceRepository = $this->getService(SourceRepository::class);

        $limit = 10;
        $firstResult = 0;
        $searchPhrase = null;

        $this->assertCount(5, $sourceRepository->findItemsForPage($limit, $firstResult));
    }

    public function testFindItemsForPageWithSearchPhrase(): void
    {
        /** @var SourceRepository $sourceRepository */
        $sourceRepository = $this->getService(SourceRepository::class);

        $limit = 10;
        $firstResult = 0;
        $searchPhrase = "lenkowo";

        $this->assertCount(1, $sourceRepository->findItemsForPage($limit, $firstResult, $searchPhrase));
    }

    /**
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function testCountAllItems(): void
    {
        /** @var SourceRepository $sourceRepository */
        $sourceRepository = $this->getService(SourceRepository::class);
        $searchPhrase = null;

        $this->assertEquals(5, $sourceRepository->countAllItems());
    }

    /**
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function testCountAllItemsWithSearchPhrase(): void
    {
        /** @var SourceRepository $sourceRepository */
        $sourceRepository = $this->getService(SourceRepository::class);
        $searchPhrase = "lenkowo";

        $this->assertEquals(1, $sourceRepository->countAllItems($searchPhrase));
    }

    public function testFindItemsForExport(): void
    {
        /** @var SourceRepository $sourceRepository */
        $sourceRepository = $this->getService(SourceRepository::class);

        $itemsForExport = $sourceRepository->findItemsForExport();

        $this->assertIsArray($itemsForExport);
        $this->assertInstanceOf(Source::class, $itemsForExport[0]);
        $this->assertCount(5, $itemsForExport);
    }
}
