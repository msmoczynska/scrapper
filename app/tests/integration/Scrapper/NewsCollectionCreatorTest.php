<?php
declare(strict_types=1);

namespace App\Tests\integration\Scrapper;

use App\Entity\News;
use App\Entity\Section;
use App\Entity\Source;
use App\EntityCollection\NewsCollection;
use App\Scrapper\NewsCollectionCreator;
use App\Tests\support\ContentFetcherMock;
use App\Tests\support\CustomWebTestCase;
use Ramsey\Uuid\Uuid;

class NewsCollectionCreatorTest extends CustomWebTestCase
{
    /**
     * @dataProvider provideDataForCreate
     * @param Source $source
     * @param string $path
     * @param string $content
     * @param int $newsCount
     */
    public function testCreate(
        Source $source,
        string $path,
        string $content,
        int $newsCount
    ): void
    {
        ContentFetcherMock::addNewContent($path, $content);

        $newsCollectionCreator = $this->getService(NewsCollectionCreator::class);

        $newsCollection = $newsCollectionCreator->create($source);

        $this->assertInstanceOf(NewsCollection::class, $newsCollection);
        $this->assertCount($newsCount, $newsCollection);
    }

    public function testCreateWithOutdatedNews(): void
    {
        $path = "www.test-url.com";
        $source = new Source(Uuid::uuid4(), $path, Source::TYPE_HTTP, 'http-source-title');

        $section = new Section(
            Uuid::uuid4(),
            'section-name',
            "/<p class=\"tytul\">\s+(?P<ref>[^\"]*)<\/p>/",
            $source
        );

        $source->addSection($section);

        $newsToBeOutdated = new News(Uuid::uuid4(), 'outdated-news-title', $section);

        $recentNewsCollection = new NewsCollection();
        $recentNewsCollection->add($newsToBeOutdated);

        $section->appendNews($recentNewsCollection);

        $content = "<p class=\"tytul\">              News1    </p>
         <p class=\"tytul\">              News2    </p>
         <p class=\"tytul\">              News3    </p>";

        ContentFetcherMock::addNewContent($path, $content);
        $newsCollectionCreator = $this->getService(NewsCollectionCreator::class);

        $newsCollection = $newsCollectionCreator->create($source);

        $this->assertInstanceOf(NewsCollection::class, $newsCollection);
        $this->assertCount(4, $newsCollection);
        $this->assertCount(1, $newsCollection->getOutdatedNews());
    }

    /**
     * @return array
     */
    public function provideDataForCreate(): array
    {
        $path1 = "www.test-url.com";
        $newsCount1 = 3;

        $source1 = new Source(Uuid::uuid4(), $path1, Source::TYPE_HTTP, 'http-source-title');

        $section1 = new Section(
            Uuid::uuid4(),
            'section-name',
            "/<p class=\"tytul\">\s+(?P<ref>[^\"]*)<\/p>/",
            $source1
        );

        $source1->addSection($section1);

        $path2 = realpath($_SERVER["DOCUMENT_ROOT"]) . "/resources/fixtures/CosmeticNews.html";
        $newsCount2 = 3;

        $source2 = new Source(Uuid::uuid4(), $path2, Source::TYPE_FILE, 'file-source-title');

        $section2 = new Section(
            Uuid::uuid4(),
            'section-name',
            "/class=\"title\">(?P<ref>[^\"]*)<\/a>/",
            $source2
        );

        $source2->addSection($section2);

        $contentHttp = "<p class=\"tytul\">              News1    </p>
         <p class=\"tytul\">              News2    </p>
         <p class=\"tytul\">              News3    </p>";

        $contentFile = "class=\"title\">News1</a> class=\"title\">News2</a> class=\"title\">News3</a>";

        return [
            "httpData"
            => [$source1, $path1, $contentHttp, $newsCount1],
            "fileData"
            => [$source2, $path2, $contentFile, $newsCount2],
        ];
    }
}
