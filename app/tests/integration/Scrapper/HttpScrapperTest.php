<?php
declare(strict_types=1);

namespace App\Tests\integration\Scrapper;

use App\Entity\News;
use App\Entity\Section;
use App\Entity\Source;
use App\EntityCollection\NewsCollection;
use App\Scrapper\HttpScrapper;
use App\Tests\support\ContentFetcherMock;
use App\Tests\support\CustomWebTestCase;
use Ramsey\Uuid\Uuid;

class HttpScrapperTest extends CustomWebTestCase
{
    /**
     * @dataProvider provideDataForGetNews
     * @param string $path
     * @param Section $section
     * @param int $newsCountBeforeGetNews
     * @param int $newsCountAfterGetNews
     * @param string $content
     */
    public function testGetNews(
        string $path,
        Section $section,
        int $newsCountBeforeGetNews,
        int $newsCountAfterGetNews,
        string $content
    ): void
    {
        ContentFetcherMock::addNewContent($path, $content);

        $httpScrapper = $this->getService(HttpScrapper::class);

        $this->assertEquals($newsCountBeforeGetNews, $section->getNews()->count());
        $newsCollection = $httpScrapper->getNews($path, $section);

        $this->assertInstanceOf(NewsCollection::class, $newsCollection);
        $this->assertEquals($newsCountAfterGetNews, $newsCollection->count());
    }

    public function testSupport(): void
    {
        $source = new Source(
            Uuid::uuid4(),
            "www.test-url.com",
            Source::TYPE_HTTP,
            'source-title'
        );

        $httpScrapper = $this->getService(HttpScrapper::class);

        $this->assertTrue($httpScrapper->support($source));
    }

    /**
     * @return array
     */
    public function provideDataForGetNews(): array
    {
        $path = "www.test-url.com";
        $source = new Source(Uuid::uuid4(), $path, Source::TYPE_HTTP, 'source-title');

        $section1 = new Section(
            Uuid::uuid4(),
            'section1-name',
            "/<p class=\"tytul\">\s+(?P<ref>[^\"]*)<\/p>/",
            $source
        );

        $newsCountBeforeGetNews1 = 0;
        $newsCountAfterGetNews1 = 3;

        $section2 = new Section(
            Uuid::uuid4(),
            'section2-name',
            "/<p class=\"tytul\">\s+(?P<ref>[^\"]*)<\/p>/",
            $source
        );

        $news = new News(Uuid::uuid4(), 'news-title', $section2);

        $newsCollection = new NewsCollection();
        $newsCollection->add($news);

        $section2->appendNews($newsCollection);

        $newsCountBeforeGetNews2 = 1;
        $newsCountAfterGetNews2 = 3;

        $content = "<p class=\"tytul\">              News1    </p>
         <p class=\"tytul\">              News2    </p>
         <p class=\"tytul\">              News3    </p>";

        return [
            "newsNullData"
            => [$path, $section1, $newsCountBeforeGetNews1, $newsCountAfterGetNews1, $content],
            "newsExistData"
            => [$path, $section2, $newsCountBeforeGetNews2, $newsCountAfterGetNews2, $content],
        ];
    }
}
