<?php
declare(strict_types=1);

namespace App\Tests\integration\Scrapper;

use App\Entity\News;
use App\Entity\Section;
use App\Entity\Source;
use App\EntityCollection\NewsCollection;
use App\Scrapper\FileScrapper;
use App\Tests\support\ContentFetcherMock;
use App\Tests\support\CustomWebTestCase;
use Ramsey\Uuid\Uuid;

class FileScrapperTest extends CustomWebTestCase
{
    /**
     * @dataProvider provideDataForGetNews
     * @param string $path
     * @param Section $section
     * @param int $newsCountBeforeGetNews
     * @param int $newsCountAfterGetNews
     * @param string $content
     */
    public function testGetNews(
        string $path,
        Section $section,
        int $newsCountBeforeGetNews,
        int $newsCountAfterGetNews,
        string $content
    ): void
    {
        ContentFetcherMock::addNewContent($path, $content);

        $fileScrapper = $this->getService(FileScrapper::class);

        $this->assertEquals($newsCountBeforeGetNews, $section->getNews()->count());
        $newsCollection = $fileScrapper->getNews($path, $section);

        $this->assertInstanceOf(NewsCollection::class, $newsCollection);
        $this->assertEquals($newsCountAfterGetNews, $newsCollection->count());
    }

    public function testSupport(): void
    {
        $source = new Source(
            Uuid::uuid4(),
            $this->projectDir . "/resources/fixtures/CosmeticNews.html",
            Source::TYPE_FILE,
            'source-title'
        );

        $fileScrapper = $this->getService(FileScrapper::class);

        $this->assertTrue($fileScrapper->support($source));
    }

    /**
     * @return array
     */
    public function provideDataForGetNews(): array
    {
        $path = realpath($_SERVER["DOCUMENT_ROOT"]) . "/resources/fixtures/CosmeticNews.html";
        $source = new Source(Uuid::uuid4(), $path, Source::TYPE_FILE, 'source-title');

        $section1 = new Section(
            Uuid::uuid4(),
            'section1-name',
            "/class=\"title\">(?P<ref>[^\"]*)<\/a>/",
            $source
        );

        $newsCountBeforeGetNews1 = 0;
        $newsCountAfterGetNews1 = 3;

        $section2 = new Section(
            Uuid::uuid4(),
            'section2-name',
            "/class=\"title\">(?P<ref>[^\"]*)<\/a>/",
            $source
        );

        $news = new News(Uuid::uuid4(), 'news-title', $section2);

        $newsCollection = new NewsCollection();
        $newsCollection->add($news);

        $section2->appendNews($newsCollection);

        $newsCountBeforeGetNews2 = 1;
        $newsCountAfterGetNews2 = 3;

        $content = "class=\"title\">News1</a> class=\"title\">News2</a> class=\"title\">News3</a>";

        return [
            "newsNullData"
                => [$path, $section1, $newsCountBeforeGetNews1, $newsCountAfterGetNews1, $content],
            "newsExistData"
                => [$path, $section2, $newsCountBeforeGetNews2, $newsCountAfterGetNews2, $content],
        ];
    }
}
