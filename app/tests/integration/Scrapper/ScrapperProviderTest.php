<?php
declare(strict_types=1);

namespace App\Tests\integration\Scrapper;

use App\Entity\Source;
use App\Exception\MissingScrapperException;
use App\Scrapper\FileScrapper;
use App\Scrapper\HttpScrapper;
use App\Scrapper\ScrapperProvider;
use App\Tests\support\CustomWebTestCase;
use Ramsey\Uuid\Uuid;

class ScrapperProviderTest extends CustomWebTestCase
{
    /**
     * @dataProvider provideDataForProvideScrapper
     * @param Source $source
     * @param string $scrapperClassName
     */
    public function testProvideScrapper(Source $source, string $scrapperClassName): void
    {
        $scrapperProvider = $this->getService(ScrapperProvider::class);

        $this->assertInstanceOf($scrapperClassName, $scrapperProvider->provideScrapper($source));
    }

    public function testProvideScrapperWithMissingScrapperException(): void
    {
        $source = new Source(
            Uuid::uuid4(),
            "www.test-url.com",
            'wrong type',
            'source-title'
        );

        $scrapperProvider = $this->getService(ScrapperProvider::class);

        $this->expectException(MissingScrapperException::class);

        $scrapperProvider->provideScrapper($source);
    }

    /**
     * @return array
     */
    public function provideDataForProvideScrapper(): array
    {
        $source1 = new Source(
            Uuid::uuid4(),
            "www.test-url.com",
            Source::TYPE_HTTP,
            'http-source-title'
        );

        $scrapperClassName1 = HttpScrapper::class;

        $source2 = new Source(
            Uuid::uuid4(),
            realpath($_SERVER["DOCUMENT_ROOT"]) . "/resources/fixtures/CosmeticNews.html",
            Source::TYPE_FILE,
            'file-source-title'
        );

        $scrapperClassName2 = FileScrapper::class;

        return [
            "httpTypeData" => [$source1, $scrapperClassName1],
            "fileTypeData" => [$source2, $scrapperClassName2],
        ];
    }
}
