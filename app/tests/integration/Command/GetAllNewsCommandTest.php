<?php
declare(strict_types=1);

namespace App\Tests\integration\Command;

use App\Tests\support\CustomWebTestCase;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Tester\CommandTester;

class GetAllNewsCommandTest extends CustomWebTestCase
{
    public function testExecute(): void
    {
        $kernel = static::createKernel();
        $application = new Application($kernel);

        $command = $application->find('app:scrapper:getAllNews');
        $commandTester = new CommandTester($command);
        $commandTester->execute([]);

        $output = $commandTester->getDisplay();
        $this->assertContains(' news added to the database, ', $output);
    }
}
