<?php
declare(strict_types=1);

namespace App\Tests\integration\Service;

use App\Entity\Section;
use App\Entity\Source;
use App\Exception\ValidationException;
use App\Service\SourceValidator;
use App\Tests\support\CustomWebTestCase;
use Ramsey\Uuid\Uuid;

class SourceValidatorTest extends CustomWebTestCase
{
    /**
     * @dataProvider provideDataForValidateSource
     * @param Source $source
     */
    public function testValidateSource(Source $source): void
    {
        $sourceValidator = $this->getService(SourceValidator::class);

        $exceptionThrown = false;

        try {
            $sourceValidator->validateSource($source);
        } catch (ValidationException $exception) {
            $message = "";
            foreach ($exception->getErrors() as $key => $violation) {
                $message = $message . $violation->getPropertyPath() . ": " . $violation->getMessage();
            }
            $exceptionThrown = true;
        }

        $this->assertEquals("sections[0].regex: This value should not be blank.", $message);
        $this->assertTrue($exceptionThrown);
    }

    /**
     * @return array
     */
    public function provideDataForValidateSource(): array
    {
        $source1 = new Source(
            Uuid::uuid4(),
            "www.test-url.com",
            Source::TYPE_HTTP,
            'http-source-title'
        );

        $section1 = new Section(Uuid::uuid4(), 'section-name', '', $source1);

        $source1->addSection($section1);

        $source2 = new Source(
            Uuid::uuid4(),
            realpath($_SERVER["DOCUMENT_ROOT"]) . "/resources/fixtures/CosmeticNews.html",
            Source::TYPE_FILE,
            'file-source-title'
        );

        $section2 = new Section(Uuid::uuid4(), 'section-name', '', $source2);

        $source2->addSection($section2);

        return [
            "httpData" => [$source1],
            "httpFile" => [$source2],
        ];
    }
}
