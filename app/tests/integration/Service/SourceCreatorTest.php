<?php
declare(strict_types=1);

namespace App\Tests\integration\Service;

use App\Entity\Source;
use App\Exception\SourceAlreadyExistException;
use App\Exception\ValidationException;
use App\Service\SourceCreator;
use App\Tests\support\ContentFetcherMock;
use App\Tests\support\CustomWebTestCase;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class SourceCreatorTest extends CustomWebTestCase
{
    /**
     * @dataProvider provideDataForCreateSource
     * @param UuidInterface $id
     * @param string $path
     * @param array $sectionArray
     * @param string $content
     * @param string $expectedType
     * @param string $expectedTitle
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws ValidationException
     * @throws SourceAlreadyExistException
     */
    public function testCreateSource(
        UuidInterface $id,
        string $path,
        array $sectionArray,
        string $content,
        string $expectedType,
        string $expectedTitle
    ): void
    {
        ContentFetcherMock::addNewContent($path, $content);

        /** @var SourceCreator $sourceCreator */
        $sourceCreator = $this->getService(SourceCreator::class);

        $source = $sourceCreator->createSource($id, $path, $sectionArray);

        $this->assertInstanceOf(Source::class, $source);
        $this->assertEquals($expectedTitle, $source->getTitle());
        $this->assertEquals($expectedType, $source->getType());
    }

    /**
     * @return array
     */
    public function provideDataForCreateSource(): array
    {
        $id1 = Uuid::uuid4();
        $path1 = "www.test-url.com";
        $sectionsArray1 = ["section1-name" => "/<p class=\"tytul\">\s+(?P<ref>[^\"]*)<\/p>/"];
        $type1 = Source::TYPE_HTTP;
        $title1 = "Http-Source-title";

        $id2 = Uuid::uuid4();
        $path2 = realpath($_SERVER["DOCUMENT_ROOT"]) . "/resources/fixtures/CosmeticNews.html";
        $sectionsArray2 = ["section2-name" => "/class=\"title\">(?P<ref>[^\"]*)<\/a>/"];
        $type2 = Source::TYPE_FILE;
        $title2 = "File-Source-title";

        $contentHttp = "<p class=\"tytul\">              News1    </p>
         <p class=\"tytul\">              News2    </p>
         <p class=\"tytul\">              News3    </p>
        <title>Http-Source-title</title>";

        $contentFile = "class=\"title\">News1</a> class=\"title\">News2</a> class=\"title\">News3</a>
<title>File-Source-title</title>";

        return [
            "httpData" => [$id1, $path1, $sectionsArray1, $contentHttp, $type1, $title1],
            "fileData" => [$id2, $path2, $sectionsArray2, $contentFile, $type2, $title2],
        ];
    }
}
