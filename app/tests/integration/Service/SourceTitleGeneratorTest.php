<?php
declare(strict_types=1);

namespace App\Tests\integration\Service;

use App\Service\SourceTitleGenerator;
use App\Tests\support\ContentFetcherMock;
use App\Tests\support\CustomWebTestCase;
use LogicException;

class SourceTitleGeneratorTest extends CustomWebTestCase
{
    public function testGenerateTitle(): void
    {
        $path = "www.test-url.com";
        $expectedTitle = "Source title";
        $content = "<title>Source title</title>";

        ContentFetcherMock::addNewContent($path, $content);

        $sourceTitleGenerator = $this->getService(SourceTitleGenerator::class);

        $this->assertEquals($expectedTitle, $sourceTitleGenerator->generateFromFilePath($path));
    }

    public function testGenerateTitleWithWrongRegexException(): void
    {
        $path = $this->projectDir . "/tests/data/fakeContent.html";
        $content = "fake content";

        ContentFetcherMock::addNewContent($path, $content);

        $sourceTitleGenerator = $this->getService(SourceTitleGenerator::class);

        $this->expectException(LogicException::class);
        $this->expectExceptionMessage("The application couldn't get the title of the website based on owned regexes");

        $sourceTitleGenerator->generateFromFilePath($path);
    }
}
