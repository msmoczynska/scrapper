<?php
declare(strict_types=1);

namespace App\Tests\integration\ExportGenerator;

use App\Enum\ExportDetails;
use App\ExportGenerator\XlsxExportGenerator;
use App\Tests\support\CustomWebTestCase;

class XlsxExportGeneratorTest extends CustomWebTestCase
{
    /**
     * @dataProvider provideDataForGenerate
     * @param array $exportContent
     * @param string $type
     */
    public function testGenerate(array $exportContent, string $type): void
    {
        $xlsxExportGenerator = $this->getService(XlsxExportGenerator::class);

        $fileName = $xlsxExportGenerator->generate($exportContent, $type);

        $this->assertIsString($fileName);
        $this->assertStringContainsString($type . 'List.xlsx', $fileName);
    }

    public function testSupport(): void
    {
        $xlsxExportGenerator = $this->getService(XlsxExportGenerator::class);

        $this->assertTrue($xlsxExportGenerator->support(ExportDetails::XLSX_FORMAT));
    }

    /**
     * @return array
     */
    public function provideDataForGenerate(): array
    {
        $exportContent1 = [];
        $type1 = ExportDetails::SOURCE_TYPE;

        return [
            "sourceExport" => [$exportContent1, $type1]
        ];
    }
}
