<?php
declare(strict_types=1);

namespace App\Tests\integration\ExportGenerator\Content;

use App\ExportGenerator\Content\SourceContentCreator;
use App\Tests\support\CustomWebTestCase;

class SourceContentCreatorTest extends CustomWebTestCase
{
    public function testCreate(): void
    {
        $sourceContentCreator = $this->getService(SourceContentCreator::class);

        $content = $sourceContentCreator->create();
        $headers = $content[0];
        $source1 = $content[1];

        $this->assertIsArray($content);
        $this->assertEquals('Days since in the system', $headers[3]);
        $this->assertStringContainsString('CosmeticNews.html', $source1[1]);
    }
}
