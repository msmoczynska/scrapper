<?php
declare(strict_types=1);

namespace App\Tests\integration\ExportGenerator\Content;

use App\Enum\ExportDetails;
use App\Exception\MissingContentCreatorException;
use App\ExportGenerator\Content\ContentCreator;
use App\Tests\support\CustomWebTestCase;
use ReflectionClass;
use ReflectionException;

class ContentCreatorTest extends CustomWebTestCase
{
    /**
     * @dataProvider provideDataForCreateContentForProperItemsType
     * @param string $type
     * @throws ReflectionException
     */
    public function testCreateContentForProperItemsType(string $type): void
    {
        $contentCreator = $this->getService(ContentCreator::class);

        $this->assertIsArray($contentCreator->createContentForProperItemsType($type));

        $class = new ReflectionClass($contentCreator);
        $contentCreators = $class->getProperty('contentCreators');
        $contentCreators->setAccessible(true);

        $this->assertCount(1, $contentCreators->getValue($contentCreator));
    }

    public function testCreateContentForProperItemsTypeWithMissingContentCreatorException(): void
    {
        $contentCreator = $this->getService(ContentCreator::class);

        $this->expectException(MissingContentCreatorException::class);

        $contentCreator->createContentForProperItemsType("wrongType");
    }

    /**
     * @return array
     */
    public function provideDataForCreateContentForProperItemsType(): array
    {
        return [
            "sourceType" => [ExportDetails::SOURCE_TYPE]
        ];
    }
}
