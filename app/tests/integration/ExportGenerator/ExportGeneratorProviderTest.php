<?php
declare(strict_types=1);

namespace App\Tests\integration\ExportGenerator;

use App\Enum\ExportDetails;
use App\Exception\MissingExportGeneratorException;
use App\ExportGenerator\CsvExportGenerator;
use App\ExportGenerator\ExportGeneratorProvider;
use App\ExportGenerator\XlsxExportGenerator;
use App\Tests\support\CustomWebTestCase;
use ReflectionClass;
use ReflectionException;

class ExportGeneratorProviderTest extends CustomWebTestCase
{
    /**
     * @dataProvider provideDataForProvideExportGenerator
     * @param string $exportFormat
     * @param string $exportGeneratorClassName
     * @throws ReflectionException
     */
    public function testProvideExportGenerator(string $exportFormat, string $exportGeneratorClassName): void
    {
        $exportGeneratorProvider = $this->getService(ExportGeneratorProvider::class);

        $this->assertInstanceOf($exportGeneratorClassName, $exportGeneratorProvider->provide($exportFormat));

        $class = new ReflectionClass($exportGeneratorProvider);
        $exportGenerators = $class->getProperty('exportGenerators');
        $exportGenerators->setAccessible(true);

        $this->assertCount(2, $exportGenerators->getValue($exportGeneratorProvider));
    }

    public function testProvideExportGeneratorWithMissingExportGeneratorException(): void
    {
        $exportGeneratorProvider = $this->getService(ExportGeneratorProvider::class);

        $this->expectException(MissingExportGeneratorException::class);

        $exportGeneratorProvider->provide("wrongFormat");
    }

    /**
     * @return array
     */
    public function provideDataForProvideExportGenerator(): array
    {
        return [
            "xlsxFormat" => [ExportDetails::XLSX_FORMAT, XlsxExportGenerator::class],
            "csvFormat" => [ExportDetails::CSV_FORMAT, CsvExportGenerator::class],
        ];
    }
}
