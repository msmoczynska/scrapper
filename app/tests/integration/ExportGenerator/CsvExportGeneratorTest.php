<?php
declare(strict_types=1);

namespace App\Tests\integration\ExportGenerator;

use App\Enum\ExportDetails;
use App\ExportGenerator\CsvExportGenerator;
use App\Tests\support\CustomWebTestCase;

class CsvExportGeneratorTest extends CustomWebTestCase
{
    /**
     * @dataProvider provideDataForGenerate
     * @param array $exportContent
     * @param string $type
     */
    public function testGenerate(array $exportContent, string $type): void
    {
        $csvExportGenerator = $this->getService(CsvExportGenerator::class);

        $fileName = $csvExportGenerator->generate($exportContent, $type);

        $this->assertIsString($fileName);
        $this->assertStringContainsString($type . 'List.csv', $fileName);
    }

    public function testSupport(): void
    {
        $csvExportGenerator = $this->getService(CsvExportGenerator::class);

        $this->assertTrue($csvExportGenerator->support(ExportDetails::CSV_FORMAT));
    }

    /**
     * @return array
     */
    public function provideDataForGenerate(): array
    {
        $exportContent1 = [];
        $type1 = ExportDetails::SOURCE_TYPE;

        return [
            "sourceExport" => [$exportContent1, $type1]
        ];
    }
}
