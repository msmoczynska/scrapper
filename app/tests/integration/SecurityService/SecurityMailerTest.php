<?php
declare(strict_types=1);

namespace App\Tests\integration\SecurityService;

use App\Entity\User;
use App\SecurityService\SecurityMailer;
use App\Tests\support\CustomWebTestCase;
use DateTime;
use Ramsey\Uuid\Uuid;

class SecurityMailerTest extends CustomWebTestCase
{
    public function testSendConfirmRegistrationEmail():void
    {
        $user = new User(
            Uuid::uuid4(),
            'email@test.pl',
            'firstName',
            'secondName',
            new DateTime('2000-02-02'),
            'confirmationToken'
        );

        $user->setPassword('password');

        $securityMailer = $this->getService(SecurityMailer::class);

        $sent = $securityMailer->sendConfirmRegistrationEmail($user);

        $this->assertIsInt($sent);
        $this->assertEquals(1, $sent);
    }

    public function testSendResetPasswordEmail(): void
    {
        $user = new User(
            Uuid::uuid4(),
            'email@test.pl',
            'firstName',
            'secondName',
            new DateTime('2000-02-02'),
            'confirmationToken'
        );

        $user
            ->setPassword('password')
            ->setVerified(true)
            ->setResetPasswordToken('resetPasswordToken');

        $securityMailer = $this->getService(SecurityMailer::class);

        $sent = $securityMailer->sendResetPasswordEmail($user);

        $this->assertIsInt($sent);
        $this->assertEquals(1, $sent);
    }
}
