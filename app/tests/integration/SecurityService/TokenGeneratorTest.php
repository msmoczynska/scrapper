<?php
declare(strict_types=1);

namespace App\Tests\integration\SecurityService;

use App\SecurityService\TokenGenerator;
use App\Tests\support\CustomWebTestCase;

class TokenGeneratorTest extends CustomWebTestCase
{
    public function testGenerate(): void
    {
        $tokenGenerator = $this->getService(TokenGenerator::class);
        $token = $tokenGenerator->generate();

        $this->assertIsString($token);
        $this->assertEquals(20, strlen($token));
        $this->assertNotSame($token, $tokenGenerator->generate());
    }
}
