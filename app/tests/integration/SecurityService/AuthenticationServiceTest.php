<?php
declare(strict_types=1);

namespace App\Tests\integration\SecurityService;

use App\Repository\UserRepository;
use App\SecurityService\AuthenticationService;
use App\Tests\support\CustomWebTestCase;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

class AuthenticationServiceTest extends CustomWebTestCase
{
    public function testLogInUser(): void
    {
        $userRepository = $this->getService(UserRepository::class);
        $user = $userRepository->findOneBy(['email' => 'email1@email.pl']);

        $authenticationService = $this->getService(AuthenticationService::class);

        $authenticationService->logInUser($user);
        $tokenStorage = $this->getService(TokenStorageInterface::class);

        $this->assertInstanceOf(TokenInterface::class, $tokenStorage->getToken());
    }
}
