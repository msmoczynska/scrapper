<?php
declare(strict_types=1);

namespace App\Tests\support;

use App\DataFixtures\SourceFixtures;
use App\DataFixtures\UserFixtures;
use App\Entity\User;
use App\Exception\MissingScrapperException;
use Liip\TestFixturesBundle\Test\FixturesTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\BrowserKit\AbstractBrowser;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class FixturesAwareWebTestCase extends WebTestCase
{
    use FixturesTrait;

    protected bool $loadFixtures = true;
    protected array $fixturesToLoad = [SourceFixtures::class, UserFixtures::class];
    protected string $projectDir;
    protected ?AbstractBrowser $client;

    public function setUp(): void
    {
        self::ensureKernelShutdown();

        $this->client = $this->createClient();

        if (null === self::$kernel) {
            self::bootKernel();
        }

        $this->projectDir = self::$kernel->getProjectDir();

        SwiftMailerAdapterMock::clearStack();
        ContentFetcherMock::clearStack();

        if (true === $this->loadFixtures) {
            foreach (SourceFixtures::SOURCE_PROPERTIES as $sourceProperty) {
                ContentFetcherMock::addNewContent(
                    $this->projectDir . $sourceProperty["path"],
                    file_get_contents($this->projectDir . $sourceProperty["path"])
                );
            }

            $this->loadFixtures($this->fixturesToLoad);
        }
    }

    /**
     * @param string $serviceName
     * @return object
     */
    public function getService(string $serviceName): object
    {
        if (null === self::$container) {
            self::bootKernel()->getContainer();
        }

        return self::$container->get($serviceName);
    }

    /**
     * @param String|null $email
     */
    protected function logIn(String $email = null): void
    {
        $email = (null === $email) ? 'admin@email.pl' : $email;
        $session = $this->client->getContainer()->get('session');
        $repository = $this->client->getContainer()->get('doctrine')->getRepository(User::class);

        $user = $repository->findOneBy(['email' => $email]);

        $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
        $session->set('_security_'.'main', serialize($token));
        $session->save();

        $cookie = new Cookie($session->getName(), $session->getId());
        $this->client->getCookieJar()->set($cookie);
    }


    /**
     * @return string
     * @throws MissingScrapperException
     */
    public function getResponseContent(): string
    {
        $content = $this->client->getResponse()->getContent();

        if ($content) {
            return $content;
        }

        throw new MissingScrapperException("Content is not exist.");
    }

    /**
     * @return array
     */
    public function asyncRequest(): array
    {
        return [
            'HTTP_X-Requested-With' => 'XMLHttpRequest'
        ];
    }
}
