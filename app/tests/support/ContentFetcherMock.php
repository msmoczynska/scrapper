<?php
declare(strict_types=1);

namespace App\Tests\support;

use App\Exception\MissingContentException;
use App\Service\ContentFetcher;

class ContentFetcherMock extends ContentFetcher
{
    private static array $availableContents = [];

    /**
     * @param string $path
     * @param string $content
     */
    public static function addNewContent(string $path, string $content): void
    {
        self::$availableContents[$path] = $content;
    }

    public static function clearStack(): void
    {
        self::$availableContents = [];
    }

    /**
     * @param string $path
     * @return string
     * @throws MissingContentException
     */
    public function fetch(string $path): string
    {
        if (array_key_exists($path, self::$availableContents)) {
            return self::$availableContents[$path];
        }

        throw new MissingContentException();
    }
}
