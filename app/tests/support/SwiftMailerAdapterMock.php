<?php
declare(strict_types=1);

namespace App\Tests\support;

use App\Service\MailerInterface;
use Swift_Mime_SimpleMessage;

class SwiftMailerAdapterMock implements MailerInterface
{
    private static array $sentMessages = [];

    /**
     * @param Swift_Mime_SimpleMessage $message
     * @return int
     */
    public function send(Swift_Mime_SimpleMessage $message): int
    {
        self::$sentMessages[] = $message;

        return 1;
    }

    /**
     * @return Swift_Mime_SimpleMessage
     */
    public static function getLastSentMessage(): Swift_Mime_SimpleMessage
    {
        return self::$sentMessages[count(self::$sentMessages) - 1];
    }

    public static function clearStack(): void
    {
        self::$sentMessages = [];
    }
}
