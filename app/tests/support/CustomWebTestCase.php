<?php
declare(strict_types=1);

namespace App\Tests\support;

use Throwable;

class CustomWebTestCase extends FixturesAwareWebTestCase
{
    /**
     * teardown() overwriting because FixtureTrait is throwing unhandled exception during the cleanup process
     */
    protected function tearDown(): void
    {
        try {
            parent::tearDown();
        } catch (Throwable $e) {
        }
    }

    /**
     * @return array
     */
    public function grabJsonBodyFromResponse(): array
    {
        $content = json_decode($this->client->getResponse()->getContent(), true);

        if (json_last_error() === JSON_ERROR_NONE) {
            return $content;
        }

        return [];
    }

    /**
     * @param string $fileName
     * @param string $sourcePath
     * @return string
     */
    public function prepareFileForTest(string $fileName, string $sourcePath): string
    {
        copy($sourcePath, $this->projectDir . "/tests/data/" . $fileName);

        return $this->projectDir . "/tests/data/" . $fileName;
    }
}
