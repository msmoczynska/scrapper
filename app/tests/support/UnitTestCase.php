<?php
declare(strict_types=1);

namespace App\Tests\support;

use Mockery;
use PHPUnit\Framework\TestCase;

class UnitTestCase extends TestCase
{
    protected string $projectDir;

    public function setUp(): void
    {
        $this->projectDir = realpath($_SERVER["DOCUMENT_ROOT"]);
    }

    public function tearDown():void
    {
        Mockery::close();
    }
}
