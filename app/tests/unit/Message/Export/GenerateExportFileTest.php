<?php
declare(strict_types=1);

namespace App\Tests\unit\Message\Export;

use App\Enum\ExportDetails;
use App\Message\Export\GenerateExportFile;
use App\Tests\support\UnitTestCase;

class GenerateExportFileTest extends UnitTestCase
{
    /**
     * @param GenerateExportFile $generateExportFile
     * @param string $expectedFormat
     * @param string $expectedType
     * @dataProvider provideDataForTestCreate
     */
    public function testCreate(
        GenerateExportFile $generateExportFile,
        string $expectedFormat,
        string $expectedType
    ): void
    {
        $this->assertInstanceOf(GenerateExportFile::class, $generateExportFile);
        $this->assertEquals($expectedFormat, $generateExportFile->format);
        $this->assertEquals($expectedType, $generateExportFile->type);
    }

    /**
     * @return array
     */
    public function provideDataForTestCreate(): array
    {
        return [
            "xlsx" => [
                GenerateExportFile::create(ExportDetails::XLSX_FORMAT, ExportDetails::SOURCE_TYPE),
                ExportDetails::XLSX_FORMAT,
                ExportDetails::SOURCE_TYPE
            ],
            "csv" => [
                GenerateExportFile::create(ExportDetails::CSV_FORMAT, ExportDetails::SOURCE_TYPE),
                ExportDetails::CSV_FORMAT,
                ExportDetails::SOURCE_TYPE
            ],
        ];
    }
}