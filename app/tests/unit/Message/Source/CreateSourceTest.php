<?php
declare(strict_types=1);

namespace App\Tests\unit\Message\Source;

use App\Message\Source\CreateSource;
use App\Tests\support\UnitTestCase;

class CreateSourceTest extends UnitTestCase
{
    public function testCreate(): void
    {
        $path = 'www.test.url.com';
        $sections = [
            "news" => "newsRegex",
            "sport" => "sportRegex",
            "business" => "businessRegex"
        ];

        $createSource = CreateSource::create($path, $sections);

        $this->assertInstanceOf(CreateSource::class, $createSource);
        $this->assertEquals($path, $createSource->path);
        $this->assertEquals($sections, $createSource->sections);
    }
}
