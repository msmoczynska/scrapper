<?php
declare(strict_types=1);

namespace App\Tests\unit\Message\User;

use App\Message\User\ConfirmUser;
use App\Tests\support\UnitTestCase;

class ConfirmUserTest extends UnitTestCase
{
    public function testCreate(): void
    {
        $confirmationToken = 'confirmationToken';

        $confirmUser = ConfirmUser::create($confirmationToken);

        $this->assertInstanceOf(ConfirmUser::class, $confirmUser);
        $this->assertEquals($confirmationToken, $confirmUser->confirmationToken);
    }
}
