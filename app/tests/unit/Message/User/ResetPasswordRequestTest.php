<?php
declare(strict_types=1);

namespace App\Tests\unit\Message\User;

use App\Message\User\ResetPasswordRequest;
use App\Tests\support\UnitTestCase;

class ResetPasswordRequestTest extends UnitTestCase
{
    public function testCreate(): void
    {
        $email = 'email@email.com';

        $resetPasswordRequest = ResetPasswordRequest::create($email);

        $this->assertInstanceOf(ResetPasswordRequest::class, $resetPasswordRequest);
        $this->assertEquals($email, $resetPasswordRequest->email);
    }
}
