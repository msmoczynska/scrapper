<?php
declare(strict_types=1);

namespace App\Tests\unit\Message\User;

use App\Message\User\CreateUser;
use App\Tests\support\UnitTestCase;
use DateTime;

class CreateUserTest extends UnitTestCase
{
    public function testCreate(): void
    {
        $email = 'email@email.com';
        $password = 'password';
        $firstName = 'firstname';
        $secondName = 'secondname';
        $birthDate = new DateTime("2000-01-01");

        $createUser = CreateUser::create($email, $password, $firstName, $secondName, $birthDate);

        $this->assertInstanceOf(CreateUser::class, $createUser);
        $this->assertEquals($email, $createUser->email);
        $this->assertEquals($password, $createUser->password);
        $this->assertEquals($firstName, $createUser->firstName);
        $this->assertEquals($secondName, $createUser->secondName);
        $this->assertEquals($birthDate, $createUser->birthDate);
    }
}
