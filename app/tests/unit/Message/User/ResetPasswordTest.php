<?php
declare(strict_types=1);

namespace App\Tests\unit\Message\User;

use App\Message\User\ResetPassword;
use App\Entity\User;
use App\Tests\support\UnitTestCase;
use DateTime;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class ResetPasswordTest extends UnitTestCase
{
    public function testCreate(): void
    {
        $password = 'password';
        $confirmPassword = 'password';

        $resetPassword = ResetPassword::create($password, $confirmPassword);

        $this->assertInstanceOf(ResetPassword::class, $resetPassword);
        $this->assertEquals($password, $resetPassword->newPassword);
        $this->assertEquals($confirmPassword, $resetPassword->confirmNewPassword);
    }

    public function testGetAndSetUserId(): void
    {
        $id = Uuid::uuid4();

        $user = new User(
            $id,
            'email@test.pl',
            'firstName',
            'secondName',
            new DateTime('2000-01-01'),
            'confirmationToken'
        );

        $user->setPassword('oldPassword');

        $newPassword = 'newPassword';
        $confirmNewPassword = 'newPassword';

        $resetPassword = ResetPassword::create($newPassword, $confirmNewPassword);

        $resetPassword->setUser($user);
        $userID = $resetPassword->getUserId();

        $this->assertNotNull($userID);
        $this->assertInstanceOf(UuidInterface::class, $userID);
        $this->assertEquals($id, $userID);
    }
}
