<?php
declare(strict_types=1);

namespace App\Tests\unit\Message\User;

use App\Entity\User;
use App\Message\User\ChangePassword;
use App\Tests\support\UnitTestCase;
use DateTime;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class ChangePasswordTest extends UnitTestCase
{
    public function testCreate(): void
    {
        $oldPassword = 'oldPassword';
        $newPassword = 'newPassword';
        $confirmNewPassword = 'newPassword';

        $changePassword = ChangePassword::create($oldPassword, $newPassword, $confirmNewPassword);

        $this->assertInstanceOf(ChangePassword::class, $changePassword);
        $this->assertEquals($newPassword, $changePassword->newPassword);
        $this->assertEquals($confirmNewPassword, $changePassword->confirmNewPassword);
    }

    public function testGetAndSetUserId(): void
    {
        $id = Uuid::uuid4();

        $user = new User(
            $id,
            'email@test.pl',
            'firstName',
            'secondName',
            new DateTime('2000-01-01'),
            'confirmationToken'
        );

        $user->setPassword('oldPassword');

        $oldPassword = 'oldPassword';
        $newPassword = 'newPassword';
        $confirmNewPassword = 'newPassword';

        $changePassword = ChangePassword::create($oldPassword, $newPassword, $confirmNewPassword);

        $changePassword->setUser($user);
        $userID = $changePassword->getUserId();

        $this->assertNotNull($userID);
        $this->assertInstanceOf(UuidInterface::class, $userID);
        $this->assertEquals($id, $userID);
    }
}
