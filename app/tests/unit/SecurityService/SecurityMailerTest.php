<?php
declare(strict_types=1);

namespace App\Tests\unit\SecurityService;

use App\Entity\User;
use App\SecurityService\SecurityMailer;
use App\Service\MailerInterface;
use App\Tests\support\UnitTestCase;
use DateTime;
use Mockery;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Routing\RouterInterface;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class SecurityMailerTest extends UnitTestCase
{
    /**
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function testSendConfirmRegistrationEmail(): void
    {
        $user = new User(
            Uuid::uuid4(),
            'email@test.pl',
            'firstName',
            'secondName',
            new DateTime('2000-02-02'),
            'confirmationToken'
        );

        $user
            ->setPassword('password');

        $securityMailer = $this->prepareSecurityMailer();

        $sent = $securityMailer->sendConfirmRegistrationEmail($user);

        $this->assertIsInt($sent);
        $this->assertEquals(1, $sent);
    }

    /**
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function testSendResetPasswordEmail(): void
    {
        $user = new User(
            Uuid::uuid4(),
            'email@test.pl',
            'firstName',
            'secondName',
            new DateTime('2000-02-02'),
            'confirmationToken'
        );

        $user
            ->setPassword('password')
            ->setVerified(true);

        $securityMailer = $this->prepareSecurityMailer();

        $sent = $securityMailer->sendResetPasswordEmail($user);

        $this->assertIsInt($sent);
        $this->assertEquals(1, $sent);
    }

    /**
     * @return SecurityMailer
     */
    public function prepareSecurityMailer(): SecurityMailer
    {
        $templating = Mockery::mock(Environment::class);
        $templating
            ->shouldReceive('render')
            ->andReturn('emailMessageBody');

        $router = Mockery::mock(RouterInterface::class);
        $router
            ->shouldReceive('generate')
            ->andReturn('generatedUrl');

        $mailer = Mockery::mock(MailerInterface::class);
        $mailer
            ->shouldReceive('send')
            ->andReturn(1);

        return new SecurityMailer($templating, $router, $mailer);
    }
}
