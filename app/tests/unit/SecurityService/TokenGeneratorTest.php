<?php
declare(strict_types=1);

namespace App\Tests\unit\SecurityService;

use App\SecurityService\TokenGenerator;
use App\Tests\support\UnitTestCase;

class TokenGeneratorTest extends UnitTestCase
{
    public function testGenerate(): void
    {
        $tokenGenerator = new TokenGenerator();
        $token = $tokenGenerator->generate();

        $this->assertIsString($token);
        $this->assertEquals(20, strlen($token));
        $this->assertNotSame($token, $tokenGenerator->generate());
    }
}
