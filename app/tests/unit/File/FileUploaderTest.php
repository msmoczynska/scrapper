<?php
declare(strict_types=1);

namespace App\Tests\unit\File;

use App\Exception\FileValidationException;
use App\File\FileUploader;
use App\Tests\support\UnitTestCase;
use Mockery;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class FileUploaderTest extends UnitTestCase
{
    /**
     * @throws FileValidationException
     */
    public function testUpload(): void
    {
        $file = Mockery::mock(UploadedFile::class);
        $file
            ->shouldReceive('getClientOriginalName')
            ->andReturn('testFileName.html');
        $file->shouldReceive('move');

        $fileErrors = new ConstraintViolationList();

        $validator = Mockery::mock(ValidatorInterface::class);
        $validator
            ->shouldReceive('validate')
            ->andReturn($fileErrors);

        $fileUploader = new FileUploader(realpath($_SERVER["DOCUMENT_ROOT"]), $validator);

        $this->assertEquals(realpath($_SERVER["DOCUMENT_ROOT"]) . '/var/files/testFileName.html', $fileUploader->upload($file));
    }

    /**
     * @throws FileValidationException
     */
    public function testUploadWithFileValidationException(): void
    {
        $file = Mockery::mock(UploadedFile::class);
        $fileErrors = new ConstraintViolationList(['violations' => new ConstraintViolation(
            'message',
            '',
            [],
            $file,
            '',
            $file
        )]);

        $validator = Mockery::mock(ValidatorInterface::class);
        $validator
            ->shouldReceive('validate')
            ->andReturn($fileErrors);

        $fileUploader = new FileUploader(realpath($_SERVER["DOCUMENT_ROOT"]), $validator);

        $this->expectException(FileValidationException::class);

        $fileUploader->upload($file);
    }
}
