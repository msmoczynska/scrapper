<?php
declare(strict_types=1);

namespace App\Tests\unit\EntityCollection;

use App\Entity\News;
use App\Entity\Section;
use App\Entity\Source;
use App\EntityCollection\NewsCollection;
use DateTime;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\Uuid;

class NewsCollectionTest extends TestCase
{
    public function testGetRecentNews(): void
    {
        $this->assertCount(2, $this->getTestNewsCollection()->getRecentNews());
    }

    public function testGetActiveNews(): void
    {
        $this->assertCount(3, $this->getTestNewsCollection()->getActiveNews());
    }

    public function testGetUpdatedNews(): void
    {
        $this->assertCount(2, $this->getTestNewsCollection()->getUpdatedNews());
    }

    public function testGetOutdatedNews(): void
    {
        $this->assertCount(1, $this->getTestNewsCollection()->getOutdatedNews());
    }

    /**
     * @return NewsCollection
     */
    public function getTestNewsCollection(): NewsCollection
    {
        $section = new Section(
            Uuid::uuid4(),
            "Section-name",
            "/<p class=\"tytul\">\s+(?P<ref>[^\"]*)<\/p>/",
            new Source(Uuid::uuid4(), "www.test-url.com", Source::TYPE_HTTP, 'source-title')
        );

        $news1 = new News(Uuid::uuid4(), 'news1-title', $section);
        $news2 = new News(Uuid::uuid4(), 'news2-title', $section);

        $news3 = new News(Uuid::uuid4(), 'news3-title', $section);
        $news3->setUpdatedAt(new DateTime());

        $news4 = new News(Uuid::uuid4(), 'news4-title', $section, false);
        $news4->setUpdatedAt(new DateTime());

        return $newsCollection = new NewsCollection([$news1, $news2, $news3, $news4]);
    }
}
