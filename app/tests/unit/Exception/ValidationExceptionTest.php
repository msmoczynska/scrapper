<?php
declare(strict_types=1);

namespace App\Tests\unit\Exception;

use App\Exception\ValidationException;
use App\Tests\support\UnitTestCase;
use Mockery;
use Symfony\Component\Validator\ConstraintViolationListInterface;

class ValidationExceptionTest extends UnitTestCase
{
    public function testGetErrors():void
    {
        $expectedErrors = Mockery::mock(ConstraintViolationListInterface::class);

        $validationException = new ValidationException($expectedErrors);

        $this->assertEquals($expectedErrors, $validationException->getErrors());
    }
}
