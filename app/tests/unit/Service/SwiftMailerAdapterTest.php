<?php
declare(strict_types=1);

namespace App\Tests\unit\Service;

use App\Service\SwiftMailerAdapter;
use App\Tests\support\UnitTestCase;
use Mockery;
use Swift_Mailer;
use Swift_Message;

class SwiftMailerAdapterTest extends UnitTestCase
{
    public function testSend(): void
    {
        $message = (new Swift_Message('Subject'));

        $mailer = Mockery::mock(Swift_Mailer::class);
        $mailer
            ->shouldReceive('send')
            ->andReturn(1);

        $swiftMailerAdapter = new SwiftMailerAdapter($mailer);

        $sent = $swiftMailerAdapter->send($message);

        $this->assertIsInt($sent);
        $this->assertEquals(1, $sent);
    }
}
