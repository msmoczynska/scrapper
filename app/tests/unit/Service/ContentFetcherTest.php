<?php
declare(strict_types=1);

namespace App\Tests\unit\Service;

use App\Service\ContentFetcher;
use App\Tests\support\UnitTestCase;

class ContentFetcherTest extends UnitTestCase
{
    public function testFetch(): void
    {
        $path = $this->projectDir . "/tests/data/fakeContent.html";
        $expectedContent = "class=\"title\">Fake news 1</a> class=\"title\">Fake news 2</a> class=\"title\">Fake news 3</a>";

        $contentFetcher = new ContentFetcher();

        $this->assertEquals($expectedContent, $contentFetcher->fetch($path));
    }
}
