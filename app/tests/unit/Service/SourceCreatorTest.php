<?php
declare(strict_types=1);

namespace App\Tests\unit\Service;

use App\Entity\Section;
use App\Entity\Source;
use App\Exception\SourceAlreadyExistException;
use App\Exception\ValidationException;
use App\Repository\SectionRepository;
use App\Repository\SourceRepository;
use App\Service\SourceCreator;
use App\Service\SourceTitleGenerator;
use App\Service\SourceValidator;
use App\Tests\support\UnitTestCase;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Mockery;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class SourceCreatorTest extends UnitTestCase
{
    /**
     * @dataProvider provideDataForCreateSourceWhenSourceNotExist
     * @param Source|null $source
     * @param Section|null $section
     * @param UuidInterface $id
     * @param string $path
     * @param array $sectionsAr
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws ValidationException
     * @throws SourceAlreadyExistException
     */
    public function testCreateSourceWhenSourceNotExist(
        ?Source $source,
        ?Section $section,
        UuidInterface $id,
        string $path,
        array $sectionsAr
    ): void
    {
        $sourceCreator = $this->prepareSourceCreator($source, $section);

        $source = $sourceCreator->createSource($id, $path, $sectionsAr);

        $this->assertInstanceOf(Source::class, $source);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws SourceAlreadyExistException
     * @throws ValidationException
     */
    public function testCreateSourceWhenSourceExist(): void
    {
        $id = Uuid::uuid4();
        $path = "www.test-url.com";
        $sectionsAr = ["section-name" => "/<p class=\"tytul\">\s+(?P<ref>[^\"]*)<\/p>/"];
        $source = new Source($id, $path, Source::TYPE_HTTP, 'http-source-title');
        $section = new Section(Uuid::uuid4(), "section-name", $sectionsAr["section-name"], $source);

        $sourceCreator = $this->prepareSourceCreator($source, $section);

        $this->expectException(SourceAlreadyExistException::class);

        $sourceCreator->createSource($id, $path, $sectionsAr);
    }

    /**
     * @return array
     */
    public function provideDataForCreateSourceWhenSourceNotExist(): array
    {
        $projectDir = realpath($_SERVER["DOCUMENT_ROOT"]);

        $id1 = Uuid::uuid4();
        $path1 = "www.test-url.com";
        $sectionsAr1 = ["section1-name" => "/<p class=\"tytul\">\s+(?P<ref>[^\"]*)<\/p>/"];
        $source1 = null;
        $section1 = null;

        $id2 = Uuid::uuid4();
        $path2 = $projectDir . "/resources/fixtures/CosmeticNews.html";
        $sectionsAr2 = ["section2-name" => "/class=\"title\">(?P<ref>[^\"]*)<\/a>/"];
        $source2 = null;
        $section2 = null;

        return [
            "httpDataSourceAndSectionNotExist"
            => [$source1, $section1, $id1, $path1, $sectionsAr1],
            "fileDataSourceAndSectionNotExist"
            => [$source2, $section2, $id2, $path2, $sectionsAr2],
        ];
    }

    /**
     * @param Source|null $source
     * @param Section|null $section
     * @return SourceCreator
     */
    private function prepareSourceCreator(?Source $source, ?Section $section): SourceCreator
    {
        $sourceTitleGenerator = Mockery::mock(SourceTitleGenerator::class);
        $sourceTitleGenerator
            ->shouldReceive('generateFromFilePath')
            ->andReturn('title');

        $sourceRepository = Mockery::mock(SourceRepository::class);
        $sourceRepository
            ->shouldReceive('findOneBy')
            ->andReturn($source);
        $sourceRepository->shouldReceive('persist');
        $sourceRepository->shouldReceive('save');

        $sourceValidator = Mockery::mock(SourceValidator::class);
        $sourceValidator
            ->shouldReceive('validateSource');

        $sectionRepository = Mockery::mock(SectionRepository::class);
        $sectionRepository
            ->shouldReceive('findOneBy')
            ->andReturn($section);

        return new SourceCreator(
            $sourceTitleGenerator,
            $sourceRepository,
            $sourceValidator,
            $sectionRepository
        );
    }
}
