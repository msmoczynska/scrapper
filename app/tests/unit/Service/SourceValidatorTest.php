<?php
declare(strict_types=1);

namespace App\Tests\unit\Service;

use App\Entity\Source;
use App\Exception\ValidationException;
use App\Service\SourceValidator;
use App\Tests\support\UnitTestCase;
use Mockery;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class SourceValidatorTest extends UnitTestCase
{
    /**
     * @dataProvider provideDataForValidateSource
     * @param Source $source
     * @param string $regex
     * @throws ValidationException
     */
    public function testValidateSource(Source $source, string $regex): void
    {
        $sourceValidator = $this->prepareSourceValidator($source, $regex);

        $this->expectException(ValidationException::class);

        $sourceValidator->validateSource($source);
    }

    /**
     * @return array
     */
    public function provideDataForValidateSource(): array
    {
        $source1 = new Source(
            Uuid::uuid4(),
            "www.test-url.com",
            Source::TYPE_HTTP,
            'http-source-title'
        );

        $source2 = new Source(
            Uuid::uuid4(),
            realpath($_SERVER["DOCUMENT_ROOT"]) . "/resources/fixtures/CosmeticNews.html",
            Source::TYPE_FILE,
            'file-source-title'
        );

        $regex = "";

        return [
            "httpData" => [$source1, $regex],
            "httpFile" => [$source2, $regex],
        ];
    }

    /**
     * @param Source $source
     * @param string $regex
     * @return SourceValidator
     */
    private function prepareSourceValidator(Source $source, string $regex): SourceValidator
    {
        $constraintViolationList = new ConstraintViolationList();
        $constraintViolationList->add(new ConstraintViolation(
            "This value should not be blank",
            "This value should not be blank",
            [],
            $source,
            "regex",
            $regex,
            null,
            "c1051bb4-d103-4f74-8988-acbcafc7fdc3",
            new NotBlank(),
            null
        ));

        $validator = Mockery::mock(ValidatorInterface::class);
        $validator
            ->shouldReceive('validate')
            ->andReturn($constraintViolationList);

        return new SourceValidator($validator);
    }
}
