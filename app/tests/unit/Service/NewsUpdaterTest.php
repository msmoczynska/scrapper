<?php
declare(strict_types=1);

namespace App\Tests\unit\Service;

use App\Entity\News;
use App\Entity\Section;
use App\Entity\Source;
use App\EntityCollection\NewsCollection;
use App\Exception\MissingScrapperException;
use App\Repository\SourceRepository;
use App\Scrapper\NewsCollectionCreator;
use App\Service\AllSourceStats;
use App\Service\NewsUpdater;
use DateTime;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Mockery;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Ramsey\Uuid\Uuid;

class NewsUpdaterTest extends TestCase
{
    /**
     * @throws MissingScrapperException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function testUpdate(): void
    {
        $source1 = new Source(
            Uuid::uuid4(),
            "www.test-url1.com",
            Source::TYPE_HTTP,
            'source-title1'
        );

        $source2 = new Source(
            Uuid::uuid4(),
            "www.test-url2.com",
            Source::TYPE_HTTP,
            'source-title2'
        );

        $section1 = new Section(
            Uuid::uuid4(),
            'section1-name',
            "/<p class=\"tytul\">\s+(?P<ref>[^\"]*)<\/p>/",
            $source1
        );

        $section2 = new Section(
            Uuid::uuid4(),
            'section2-name',
            "/<p class=\"tytul\">\s+(?P<ref>[^\"]*)<\/p>/",
            $source2
        );

        $news1 = new News(Uuid::uuid4(), 'news-title1', $section1);
        $news2 = new News(Uuid::uuid4(), 'news-title2', $section1);

        $news3 = new News(Uuid::uuid4(), 'news-title3', $section2);
        $news3->setUpdatedAt(new DateTime());

        $news4 = new News(Uuid::uuid4(), 'news-title4', $section2);
        $news4->setUpdatedAt(new DateTime());

        $sourceNewsCollection = new NewsCollection([$news1, $news2, $news3, $news4]);

        $sourceRepository = Mockery::mock(SourceRepository::class);
        $sourceRepository
            ->shouldReceive('findAll')
            ->andReturn([$source1, $source2]);
        $sourceRepository->shouldReceive('save');

        $newsCollectionCreator = Mockery::mock(NewsCollectionCreator::class);
        $newsCollectionCreator
            ->shouldReceive('create')
            ->andReturn($sourceNewsCollection);

        $cronLogger = Mockery::mock(LoggerInterface::class);
        $cronLogger->shouldReceive('notice');

        $newsUpdater = new NewsUpdater($sourceRepository, $newsCollectionCreator, $cronLogger);
        $allSourceStats = $newsUpdater->update();

        $this->assertInstanceOf(AllSourceStats::class, $allSourceStats);
        $this->assertEquals(4, $allSourceStats->getRecentNewsCount());
        $this->assertEquals(8, $allSourceStats->getActiveNewsCount());
        $this->assertEquals(0, $allSourceStats->getOutdatedNewsCount());
        $this->assertEquals(8, $allSourceStats->getAllNewsCount());
    }
}
