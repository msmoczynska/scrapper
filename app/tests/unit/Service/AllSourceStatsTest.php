<?php
declare(strict_types=1);

namespace App\Tests\unit\Service;

use App\Service\AllSourceStats;
use PHPUnit\Framework\TestCase;

class AllSourceStatsTest extends TestCase
{
    /**
     * @dataProvider provideDataForGettersAndSettersTest
     * @param AllSourceStats $allSourceStats
     * @param array $expectedNewsCount
     */
    public function testGettersAndSetters(AllSourceStats $allSourceStats, array $expectedNewsCount): void
    {
        $this->assertIsInt($allSourceStats->getRecentNewsCount());
        $this->assertEquals($expectedNewsCount["all-source-recent"], $allSourceStats->getRecentNewsCount());
        $this->assertIsInt($allSourceStats->getActiveNewsCount());
        $this->assertEquals($expectedNewsCount["all-source-active"], $allSourceStats->getActiveNewsCount());
        $this->assertIsInt($allSourceStats->getOutdatedNewsCount());
        $this->assertEquals($expectedNewsCount["all-source-outdated"], $allSourceStats->getOutdatedNewsCount());
        $this->assertIsInt($allSourceStats->getAllNewsCount());
        $this->assertEquals($expectedNewsCount["all-source-all"], $allSourceStats->getAllNewsCount());
    }

    public function testAppendMethods(): void
    {
        $allSourceStats = new AllSourceStats();
        $allSourceStats
            ->setRecentNewsCount(4)
            ->setActiveNewsCount(10)
            ->setOutdatedNewsCount(40)
            ->setAllNewsCount(50);

        $this->assertEquals(4, $allSourceStats->getRecentNewsCount());
        $this->assertEquals(10, $allSourceStats->getActiveNewsCount());
        $this->assertEquals(40, $allSourceStats->getOutdatedNewsCount());
        $this->assertEquals(50, $allSourceStats->getAllNewsCount());

        $allSourceStats->appendRecentNewsCount(2);
        $allSourceStats->appendActiveNewsCount(10);
        $allSourceStats->appendOutdatedNewsCount(20);
        $allSourceStats->appendAllNewsCount(30);

        $this->assertEquals(6, $allSourceStats->getRecentNewsCount());
        $this->assertEquals(20, $allSourceStats->getActiveNewsCount());
        $this->assertEquals(60, $allSourceStats->getOutdatedNewsCount());
        $this->assertEquals(80, $allSourceStats->getAllNewsCount());
    }

    /**
     * @return array|array[]
     */
    public function provideDataForGettersAndSettersTest(): array
    {
        $newsCount1 = [
            "all-source-recent" => 3,
            "all-source-active" => 8,
            "all-source-outdated" => 24,
            "all-source-all" => 32,
        ];

        $allSourceStats1 = new AllSourceStats();
        $allSourceStats1
            ->setRecentNewsCount($newsCount1["all-source-recent"])
            ->setActiveNewsCount($newsCount1["all-source-active"])
            ->setOutdatedNewsCount($newsCount1["all-source-outdated"])
            ->setAllNewsCount($newsCount1["all-source-all"]);

        $newsCount2 = [
            "all-source-recent" => 2,
            "all-source-active" => 10,
            "all-source-outdated" => 20,
            "all-source-all" => 30,
        ];

        $allSourceStats2 = new AllSourceStats();
        $allSourceStats2
            ->setRecentNewsCount($newsCount2["all-source-recent"])
            ->setActiveNewsCount($newsCount2["all-source-active"])
            ->setOutdatedNewsCount($newsCount2["all-source-outdated"])
            ->setAllNewsCount($newsCount2["all-source-all"]);

        return [
            'allSourceStats1' => [$allSourceStats1, $newsCount1],
            'allSourceStats2' => [$allSourceStats2, $newsCount2],
        ];
    }
}
