<?php
declare(strict_types=1);

namespace App\Tests\unit\Service;

use App\Resolver\RegexResolver;
use App\Resolver\RegexResults;
use App\Service\ContentFetcher;
use App\Service\SourceTitleGenerator;
use App\Tests\support\UnitTestCase;
use Mockery;
use LogicException;

class SourceTitleGeneratorTest extends UnitTestCase
{
    public function testGenerateTitle(): void
    {
        $regexResolver = Mockery::mock(RegexResolver::class);
        $regexResolver
            ->shouldReceive('resolve')
            ->andReturn(new RegexResults(['title']));

        $contentFetcher = Mockery::mock(ContentFetcher::class);
        $contentFetcher
            ->shouldReceive('fetch')
            ->andReturn("<title>Aktualności fryzjerskie</title>");

        $sourceTitleGenerator = new SourceTitleGenerator($regexResolver, $contentFetcher);
        $path = "www.test-url.com";

        $this->assertEquals('title', $sourceTitleGenerator->generateFromFilePath($path));
    }

    public function testGenerateTitleWithWrongRegexException(): void
    {
        $regexResolver = Mockery::mock(RegexResolver::class);
        $regexResolver
            ->shouldReceive('resolve')
            ->andReturn(new RegexResults([]));

        $contentFetcher = Mockery::mock(ContentFetcher::class);
        $contentFetcher
            ->shouldReceive('fetch')
            ->andReturn("");

        $sourceTitleGenerator = new SourceTitleGenerator($regexResolver, $contentFetcher);
        $path = "www.test-url.com";

        $this->expectException(LogicException::class);
        $this->expectExceptionMessage("The application couldn't get the title of the website based on owned regexes");

        $sourceTitleGenerator->generateFromFilePath($path);
    }
}
