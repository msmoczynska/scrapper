<?php
declare(strict_types=1);

namespace App\Tests\unit\MessageHandler\User;

use App\Entity\User;
use App\Message\User\ResetPassword;
use App\MessageHandler\User\ResetPasswordHandler;
use App\Repository\UserRepository;
use App\Tests\support\UnitTestCase;
use DateTime;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Mockery;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class ResetPasswordHandlerTest extends UnitTestCase
{
    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function testResetPassword(): void
    {
        $user = new User(
            Uuid::uuid4(),
            'email@test.pl',
            'firstName',
            'secondName',
            new DateTime('2000-02-02'),
            'confirmationToken'
        );

        $user->setVerified(true);

        $userRepository = Mockery::mock(UserRepository::class);
        $userRepository->shouldReceive('save');
        $userRepository
            ->shouldReceive('find')
            ->andReturn($user);

        $passwordEncoder = Mockery::mock(UserPasswordEncoderInterface::class);
        $passwordEncoder
            ->shouldReceive('encodePassword')
            ->andReturn('encodedPassword');

        $resetPasswordHandler = new ResetPasswordHandler($userRepository, $passwordEncoder);

        $newPassword = 'newPassword';
        $message = ResetPassword::create($newPassword, $newPassword);

        $resetPasswordHandler($message->setUser($user));

        $this->assertNull($user->getResetPasswordToken());
        $this->assertEquals('encodedPassword', $user->getPassword());
        $this->assertNull($user->getResetPasswordToken());
    }
}
