<?php
declare(strict_types=1);

namespace App\Tests\unit\MessageHandler\User;

use App\Entity\User;
use App\Exception\UserNotFoundException;
use App\Message\User\ResetPasswordRequest;
use App\MessageHandler\User\ResetPasswordRequestHandler;
use App\Repository\UserRepository;
use App\SecurityService\SecurityMailer;
use App\SecurityService\TokenGenerator;
use App\Tests\support\UnitTestCase;
use DateTime;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Mockery;
use Ramsey\Uuid\Uuid;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class ResetPasswordRequestHandlerTest extends UnitTestCase
{
    /**
     * @throws UserNotFoundException
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function testIsRestPasswordTokenSet(): void
    {
        $user = new User(
            Uuid::uuid4(),
            'email@test.pl',
            'firstName',
            'secondName',
            new DateTime('2000-02-02'),
            'confirmationToken'
        );

        $user->setVerified(true);

        $userRepository = Mockery::mock(UserRepository::class);
        $userRepository->shouldReceive('save');
        $userRepository
            ->shouldReceive('findOneBy')
            ->andReturn($user);

        $securityMailer = Mockery::mock(SecurityMailer::class);
        $securityMailer
            ->shouldReceive('sendResetPasswordEmail');

        $tokenGenerator = Mockery::mock(TokenGenerator::class);
        $tokenGenerator
            ->shouldReceive('generate');

        $resetPasswordRequestHandler = new ResetPasswordRequestHandler($userRepository, $securityMailer, $tokenGenerator);

        $message = ResetPasswordRequest::create($user->getEmail());

        $resetPasswordRequestHandler($message);

        $this->assertNotNull($user->getResetPasswordToken());
        $this->assertIsString($user->getResetPasswordToken());
    }
}
