<?php
declare(strict_types=1);

namespace App\Tests\unit\MessageHandler\User;

use App\Entity\User;
use App\Exception\ConfirmationTokenNotFoundException;
use App\Message\User\ConfirmUser;
use App\MessageHandler\User\ConfirmUserHandler;
use App\Repository\UserRepository;
use App\Tests\support\UnitTestCase;
use DateTime;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Mockery;
use Ramsey\Uuid\Uuid;

class ConfirmUserHandlerTest extends UnitTestCase
{
    /**
     * @throws ConfirmationTokenNotFoundException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function testConfirmUser(): void
    {
        $user = new User(
            Uuid::uuid4(),
            'email@test.pl',
            'firstName',
            'secondName',
            new DateTime('2000-02-02'),
            'confirmationToken'
        );

        $userRepository = Mockery::mock(UserRepository::class);
        $userRepository
            ->shouldReceive('findOneBy')
            ->andReturn($user);
        $userRepository->shouldReceive('save');

        $confirmUserHandler = new ConfirmUserHandler($userRepository);
        $message = ConfirmUser::create('confirmationToken');

        $user = $confirmUserHandler($message);

        $this->assertTrue($user->isVerified());
    }

    /**
     * @throws ConfirmationTokenNotFoundException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function testConfirmUserWhenTokenNotFound(): void
    {
        $userRepository = Mockery::mock(UserRepository::class);
        $userRepository
            ->shouldReceive('findOneBy')
            ->andReturn(null);

        $confirmUserHandler = new ConfirmUserHandler($userRepository);
        $message = ConfirmUser::create('wrongToken');

        $this->expectException(ConfirmationTokenNotFoundException::class);

        $confirmUserHandler($message);
    }
}
