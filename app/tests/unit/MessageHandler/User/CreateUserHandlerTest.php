<?php
declare(strict_types=1);

namespace App\Tests\unit\MessageHandler\User;

use App\Entity\User;
use App\Exception\UserAlreadyExistException;
use App\Message\User\CreateUser;
use App\MessageHandler\User\CreateUserHandler;
use App\Repository\UserRepository;
use App\SecurityService\SecurityMailer;
use App\SecurityService\TokenGenerator;
use App\Tests\support\UnitTestCase;
use DateTime;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Mockery;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class CreateUserHandlerTest extends UnitTestCase
{
    /**
     * @dataProvider provideDataForCreateUser
     * @param User|null $userFromRepo
     * @param string $encodedPassword
     * @param string $confirmationToken
     * @param CreateUser $createUser
     * @throws LoaderError
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws RuntimeError
     * @throws SyntaxError
     * @throws UserAlreadyExistException
     */
    public function testCreateUser(
        ?User $userFromRepo,
        string $encodedPassword,
        string $confirmationToken,
        CreateUser $createUser
    ): void
    {
        $createUserHandler = $this->prepareCreateUserHandler($userFromRepo, $encodedPassword, $confirmationToken);

        $user = $createUserHandler($createUser);

        $this->assertInstanceOf(User::class, $user);
    }

    /**
     * @throws LoaderError
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws RuntimeError
     * @throws SyntaxError
     * @throws UserAlreadyExistException
     */
    public function testCreateUserWhenUserExist(): void
    {
        $encodedPassword = 'ThisIsEncodedPassword';
        $confirmationToken = 'confirmationtoken';

        $userFromRepo = new User(
            Uuid::uuid4(),
            'email@test.pl',
            'firstName',
            'secondName',
            new DateTime('2000-01-01'),
            $confirmationToken
        );

        $userFromRepo
            ->setPassword('password')
            ->setVerified(true)
            ->setResetPasswordToken('resetpasswordtoken');

        $createUser = CreateUser::create(
            'email@test.pl',
            'password',
            'firstname',
            'secondname',
            '2000-01-01'
        );

        $createUserHandler = $this->prepareCreateUserHandler($userFromRepo, $encodedPassword, $confirmationToken);

        $this->expectException(UserAlreadyExistException::class);

        $createUserHandler($createUser);
    }

    /**
     * @return array
     */
    public function provideDataForCreateUser(): array
    {
        $userFromRepo1 = null;
        $encodedPassword1 = 'ThisIsEncodedPassword1';
        $confirmationToken1 = 'confirmationtoken1';

        $createUser1 = CreateUser::create(
            'email1@test.pl',
            'password1',
            'firstname1',
            'secondname1',
            '2000-01-01'
        );

        $createUser1->id = Uuid::uuid4();

        $userFromRepo2 = null;
        $encodedPassword2 = 'ThisIsEncodedPassword2';
        $confirmationToken2 = 'confirmationtoken2';

        $createUser2 = CreateUser::create(
            'email2@test.pl',
            'password2',
            'firstname2',
            'secondname2',
            '2000-02-02'
        );

        $createUser2->id = Uuid::uuid4();

        return [
            "user1" => [$userFromRepo1, $encodedPassword1, $confirmationToken1, $createUser1],
            "user2" => [$userFromRepo2, $encodedPassword2, $confirmationToken2, $createUser2],
        ];
    }

    /**
     * @param User $userFromRepo
     * @param string $encodedPassword
     * @param string $token
     * @return CreateUserHandler
     */
    public function prepareCreateUserHandler(?User $userFromRepo, string $encodedPassword, string $token): CreateUserHandler
    {
        $userRepository = Mockery::mock(UserRepository::class);
        $userRepository
            ->shouldReceive('findOneBy')
            ->andReturn($userFromRepo);
        $userRepository->shouldReceive('persist');
        $userRepository->shouldReceive('save');

        $passwordEncoder = Mockery::mock(UserPasswordEncoderInterface::class);
        $passwordEncoder
            ->shouldReceive('encodePassword')
            ->andReturn($encodedPassword);

        $tokenGenerator = Mockery::mock(TokenGenerator::class);
        $tokenGenerator
            ->shouldReceive('generate')
            ->andReturn($token);

        $securityMailer = Mockery::mock(SecurityMailer::class);
        $securityMailer->shouldReceive('sendConfirmRegistrationEmail');

        return new CreateUserHandler($userRepository, $passwordEncoder, $tokenGenerator, $securityMailer);
    }
}
