<?php
declare(strict_types=1);

namespace App\Tests\unit\MessageHandler\User;

use App\Entity\User;
use App\Message\User\ChangePassword;
use App\MessageHandler\User\ChangePasswordHandler;
use App\Repository\UserRepository;
use App\Tests\support\UnitTestCase;
use DateTime;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Mockery;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class ChangePasswordHandlerTest extends UnitTestCase
{
    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function testChangePassword(): void
    {
        $user = new User(
            Uuid::uuid4(),
            'email@test.pl',
            'firstName',
            'secondName',
            new DateTime('2000-02-02'),
            'confirmationToken'
        );

        $user
            ->setPassword('oldPassword')
            ->setVerified(true);

        $userRepository = Mockery::mock(UserRepository::class);
        $userRepository->shouldReceive('save');
        $userRepository
            ->shouldReceive('find')
            ->andReturn($user);

        $passwordEncoder = Mockery::mock(UserPasswordEncoderInterface::class);
        $passwordEncoder
            ->shouldReceive('encodePassword')
            ->andReturn('encodedPassword');

        $changePasswordHandler = new ChangePasswordHandler($userRepository, $passwordEncoder);

        $newPassword = 'newPassword';
        $message = ChangePassword::create('oldPassword', $newPassword, $newPassword);

        $changePasswordHandler($message->setUser($user));

        $this->assertNull($user->getResetPasswordToken());
        $this->assertEquals('encodedPassword', $user->getPassword());
    }
}
