<?php
declare(strict_types=1);

namespace App\Tests\unit\MessageHandler\Export;

use App\Enum\ExportDetails;
use App\Exception\MissingContentCreatorException;
use App\Exception\MissingExportGeneratorException;
use App\ExportGenerator\Content\ContentCreator;
use App\ExportGenerator\CsvExportGenerator;
use App\ExportGenerator\ExportGeneratorInterface;
use App\ExportGenerator\ExportGeneratorProvider;
use App\ExportGenerator\XlsxExportGenerator;
use App\Message\Export\GenerateExportFile;
use App\MessageHandler\Export\GenerateExportFileHandler;
use App\Tests\support\UnitTestCase;
use Mockery;

class GenerateExportFileHandlerTest extends UnitTestCase
{
    /**
     * @dataProvider provideDataForGenerateExportFile
     * @param ExportGeneratorInterface $exportGenerator
     * @param GenerateExportFile $generateExportFile
     * @throws MissingContentCreatorException
     * @throws MissingExportGeneratorException
     */
    public function testGenerateExportFile(ExportGeneratorInterface $exportGenerator, GenerateExportFile $generateExportFile): void
    {
        $generateExportFileHandler = $this->prepareGenerateExportFileHandler($exportGenerator);

        $this->assertStringContainsString($generateExportFile->type . 'List.' . $generateExportFile->format, $generateExportFileHandler($generateExportFile));
    }

    /**
     * @return array
     */
    public function provideDataForGenerateExportFile(): array
    {
        return [
            "sourceXlsx" => [
                new XlsxExportGenerator(),
                GenerateExportFile::create(ExportDetails::XLSX_FORMAT, ExportDetails::SOURCE_TYPE),
            ],
            "sourceCsv" => [
                new CsvExportGenerator(),
                GenerateExportFile::create(ExportDetails::CSV_FORMAT, ExportDetails::SOURCE_TYPE),
            ],
        ];
    }

    /**
     * @param ExportGeneratorInterface $exportGenerator
     * @return GenerateExportFileHandler
     */
    public function prepareGenerateExportFileHandler(ExportGeneratorInterface $exportGenerator): GenerateExportFileHandler
    {
        $exportGeneratorProvider = Mockery::mock(ExportGeneratorProvider::class);
        $exportGeneratorProvider
            ->shouldReceive("provide")
            ->andReturn($exportGenerator);

        $contentCreator = Mockery::mock(ContentCreator::class);
        $contentCreator
            ->shouldReceive("createContentForProperItemsType")
            ->andReturn([]);

        return new GenerateExportFileHandler($exportGeneratorProvider, $contentCreator);
    }
}