<?php
declare(strict_types=1);

namespace App\Tests\unit\Scrapper;

use App\Entity\Source;
use App\Exception\MissingScrapperException;
use App\Scrapper\FileScrapper;
use App\Scrapper\HttpScrapper;
use App\Scrapper\ScrapperInterface;
use App\Scrapper\ScrapperProvider;
use App\Tests\support\UnitTestCase;
use Iterator;
use Mockery;
use Ramsey\Uuid\Uuid;

class ScrapperProviderTest extends UnitTestCase
{
    /**
     * @dataProvider provideDataForProvideScrapper
     * @param Source $source
     * @param string $scrapperClassName
     * @param ScrapperInterface $scrapper
     * @throws MissingScrapperException
     */
    public function testProvideScrapper(
        Source $source,
        string $scrapperClassName,
        ScrapperInterface $scrapper
    ): void
    {
        $scrapper
            ->shouldReceive('support')
            ->andReturn(true);

        $scrapperProvider = $this->prepareScrapperProvider($scrapper);

        $this->assertInstanceOf($scrapperClassName, $scrapperProvider->provideScrapper($source));
    }

    /**
     * @throws MissingScrapperException
     */
    public function testProvideScrapperWithMissingScrapperException(): void
    {
        $source = new Source(
            Uuid::uuid4(),
            "www.test-url.com",
            'wrong type',
            'source-title'
        );

        $scrapperProvider = new ScrapperProvider([]);

        $this->expectException(MissingScrapperException::class);

        $scrapperProvider->provideScrapper($source);
    }

    /**
     * @return array
     */
    public function provideDataForProvideScrapper(): array
    {
        $source1 = new Source(
            Uuid::uuid4(),
            "www.test-url.com",
            Source::TYPE_HTTP,
            'http-source-title'
        );

        $scrapperClassName1 = HttpScrapper::class;

        $scrapper1 = Mockery::mock($scrapperClassName1);

        $source2 = new Source(
            Uuid::uuid4(),
            realpath($_SERVER["DOCUMENT_ROOT"]) . "/resources/fixtures/CosmeticNews.html",
            Source::TYPE_FILE,
            'file-source-title'
        );

        $scrapperClassName2 = FileScrapper::class;

        $scrapper2 = Mockery::mock($scrapperClassName2);

        return [
            "httpTypeData" => [$source1, $scrapperClassName1, $scrapper1],
            "fileTypeData" => [$source2, $scrapperClassName2, $scrapper2],
        ];
    }

    /**
     * @param ScrapperInterface $scrapper
     * @return ScrapperProvider
     */
    private function prepareScrapperProvider(ScrapperInterface $scrapper): ScrapperProvider
    {
        $scrappers = Mockery::mock(Iterator::class);
        $scrappers
            ->shouldReceive('next')
            ->andReturn($scrapper);
        $scrappers->shouldReceive('rewind');
        $scrappers
            ->shouldReceive('valid')
            ->andReturn(true);
        $scrappers
            ->shouldReceive('current')
            ->andReturn($scrapper);

        return new ScrapperProvider($scrappers);
    }
}
