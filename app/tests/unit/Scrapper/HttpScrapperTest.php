<?php
declare(strict_types=1);

namespace App\Tests\unit\Scrapper;

use App\Entity\News;
use App\Entity\Section;
use App\Entity\Source;
use App\EntityCollection\NewsCollection;
use App\Exception\WrongRegexException;
use App\Repository\NewsRepository;
use App\Resolver\RegexResolver;
use App\Resolver\RegexResults;
use App\Resolver\RegexResultsCleaner;
use App\Scrapper\HttpScrapper;
use App\Service\ContentFetcher;
use App\Tests\support\UnitTestCase;
use Mockery;
use Ramsey\Uuid\Uuid;

class HttpScrapperTest extends UnitTestCase
{
    /**
     * @dataProvider provideDataForGetNews
     * @param string $path
     * @param Section $section
     * @param News|null $news
     * @param int $newsCountBeforeGetNews
     * @param int $newsCountAfterGetNews
     * @throws WrongRegexException
     */
    public function testGetNews(
        string $path,
        Section $section,
        ?News $news,
        int $newsCountBeforeGetNews,
        int $newsCountAfterGetNews
    ): void
    {
        $httpScrapper = $this->prepareHttpScrapperWithMockedNews($news);

        $this->assertEquals($newsCountBeforeGetNews, $section->getNews()->count());
        $newsCollection = $httpScrapper->getNews($path, $section);

        $this->assertEquals($newsCountAfterGetNews, $newsCollection->count());

        $this->assertInstanceOf(NewsCollection::class, $newsCollection);
    }

    public function testSupport(): void
    {
        $source = new Source(
            Uuid::uuid4(),
            "www.test-url.com",
            Source::TYPE_HTTP,
            'source-title'
        );

        $httpScrapper = $this->prepareHttpScrapperWithoutMockedNews();

        $this->assertTrue($httpScrapper->support($source));
    }

    /**
     * @return array
     */
    public function provideDataForGetNews(): array
    {
        $path = "www.test-url.com";
        $source = new Source(Uuid::uuid4(), $path, Source::TYPE_HTTP, 'source-title');

        $section1 = new Section(
            Uuid::uuid4(),
            'section1-name',
            "/<p class=\"tytul\">\s+(?P<ref>[^\"]*)<\/p>/",
            $source
        );

        $news1 = null;

        $newsCountBeforeGetNews1 = 0;
        $newsCountAfterGetNews1 = 3;

        $section2 = new Section(
            Uuid::uuid4(),
            'section2-name',
            "/<p class=\"tytul\">\s+(?P<ref>[^\"]*)<\/p>/",
            $source
        );

        $news2 = new News(Uuid::uuid4(), 'news-title', $section2);

        $newsCollection = new NewsCollection();
        $newsCollection->add($news2);

        $section2->appendNews($newsCollection);

        $newsCountBeforeGetNews2 = 1;
        $newsCountAfterGetNews2 = 3;

        return [
            "newsNullData" => [
                $path,
                $section1,
                $news1,
                $newsCountBeforeGetNews1,
                $newsCountAfterGetNews1
            ],
            "newsExistData" => [
                $path,
                $section2,
                $news2,
                $newsCountBeforeGetNews2,
                $newsCountAfterGetNews2
            ],
        ];
    }

    /**
     * @param News|null $news
     * @return HttpScrapper
     */
    private function prepareHttpScrapperWithMockedNews(?News $news): HttpScrapper
    {
        $contentFetcher = Mockery::mock(ContentFetcher::class);
        $contentFetcher
            ->shouldReceive('fetch')
            ->andReturn("<p class=\"tytul\">              News1    </p>
         <p class=\"tytul\">              News2    </p>
         <p class=\"tytul\">              News3    </p>");

        $regexResolver = Mockery::mock(RegexResolver::class);
        $regexResolver
            ->shouldReceive('resolve')
            ->andReturn(new RegexResults(['news1-title', 'news2-title', 'news3-title']));

        $regexResultsCleaner = Mockery::mock(RegexResultsCleaner::class);
        $regexResultsCleaner
            ->shouldReceive('clean')
            ->andReturn(new RegexResults(['news1-title', 'news2-title', 'news3-title']));

        $newsRepository = Mockery::mock(NewsRepository::class);
        $newsRepository
            ->shouldReceive('findOneBy')
            ->andReturn($news);

        return new HttpScrapper($contentFetcher, $regexResolver, $regexResultsCleaner, $newsRepository);
    }

    /**
     * @return HttpScrapper
     */
    private function prepareHttpScrapperWithoutMockedNews(): HttpScrapper
    {
        $contentFetcher = Mockery::mock(ContentFetcher::class);
        $regexResolver = Mockery::mock(RegexResolver::class);
        $regexResultsCleaner = Mockery::mock(RegexResultsCleaner::class);
        $newsRepository = Mockery::mock(NewsRepository::class);

        return new HttpScrapper($contentFetcher, $regexResolver, $regexResultsCleaner, $newsRepository);
    }
}
