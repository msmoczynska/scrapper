<?php
declare(strict_types=1);

namespace App\Tests\unit\Scrapper;

use App\Entity\News;
use App\Entity\Section;
use App\Entity\Source;
use App\EntityCollection\NewsCollection;
use App\Exception\MissingScrapperException;
use App\Scrapper\FileScrapper;
use App\Scrapper\HttpScrapper;
use App\Scrapper\NewsCollectionCreator;
use App\Scrapper\ScrapperProvider;
use App\Tests\support\UnitTestCase;
use Mockery;
use Psr\Log\LoggerInterface;
use Ramsey\Uuid\Uuid;

class NewsCollectionCreatorTest extends UnitTestCase
{
    /**
     * @dataProvider provideDataForCreate
     * @param Source $source
     * @param Section $section
     * @param int $newsCount
     * @param string $scrapperClassName
     * @throws MissingScrapperException
     */
    public function testCreate(
        Source $source,
        Section $section,
        int $newsCount,
        string $scrapperClassName
    ): void
    {
        $newsCollectionCreator = $this->prepareNewsCollectionCreator(
            $section,
            $newsCount,
            $scrapperClassName
        );

        $newsCollection = $newsCollectionCreator->create($source);

        $this->assertInstanceOf(NewsCollection::class, $newsCollection);
        $this->assertCount($newsCount, $newsCollection);
    }

    /**
     * @throws MissingScrapperException
     */
    public function testCreateWithOutdatedNews(): void
    {
        $source = new Source(Uuid::uuid4(), "www.test-url.com", Source::TYPE_HTTP, 'http-source-title');

        $section = new Section(
            Uuid::uuid4(),
            'section-name',
            "/<p class=\"tytul\">\s+(?P<ref>[^\"]*)<\/p>/",
            $source
        );

        $source->addSection($section);

        $outdatedNews = new News(Uuid::uuid4(), 'outdated-news-title', $section);

        $outdatedNewsCollection = new NewsCollection();
        $outdatedNewsCollection->add($outdatedNews);

        $section->appendNews($outdatedNewsCollection);

        $newsCollectionCreator = $this->prepareNewsCollectionCreator($section, 2, HttpScrapper::class);

        $newsCollection = $newsCollectionCreator->create($source);

        $this->assertInstanceOf(NewsCollection::class, $newsCollection);
        $this->assertCount(3, $newsCollection);
        $this->assertCount(1, $newsCollection->getOutdatedNews());
    }

    /**
     * @return array
     */
    public function provideDataForCreate(): array
    {
        $source1 = new Source(Uuid::uuid4(), "www.test-url.com", Source::TYPE_HTTP, 'http-source-title');

        $section1 = new Section(
            Uuid::uuid4(),
            'section-name',
            "/<p class=\"tytul\">\s+(?P<ref>[^\"]*)<\/p>/",
            $source1
        );

        $source1->addSection($section1);

        $newsCount1 = 2;
        $scrapperClassName1 = HttpScrapper::class;

        $source2 = new Source(
            Uuid::uuid4(),
            realpath($_SERVER["DOCUMENT_ROOT"]) . "/resources/fixtures/CosmeticNews.html",
            Source::TYPE_FILE,
            'file-source-title'
        );

        $section2 = new Section(
            Uuid::uuid4(),
            'section-name',
            "/class=\"title\">(?P<ref>[^\"]*)<\/a>/",
            $source2
        );

        $source2->addSection($section2);

        $newsCount2 = 3;
        $scrapperClassName2 = FileScrapper::class;

        return [
            "httpData" => [$source1, $section1, $newsCount1, $scrapperClassName1],
            "fileData" => [$source2, $section2, $newsCount2, $scrapperClassName2],
        ];
    }

    /**
     * @param Section $section
     * @param int $newsCount
     * @param string $scrapperClassName
     * @return NewsCollectionCreator
     */
    private function prepareNewsCollectionCreator(
        Section $section,
        int $newsCount,
        string $scrapperClassName
    ): NewsCollectionCreator
    {
        $newsCollection = new NewsCollection();

        for ($i = 1; $i <= $newsCount; $i++) {
            $news = new News(Uuid::uuid4(), 'news-title' . $i, $section);

            $newsCollection->add($news);
        }

        $scrapper = Mockery::mock($scrapperClassName);
        $scrapper
            ->shouldReceive('getNews')
            ->andReturn($newsCollection);

        $scrapperProvider = Mockery::mock(ScrapperProvider::class);
        $scrapperProvider
            ->shouldReceive('provideScrapper')
            ->andReturn($scrapper);

        $cronLogger = Mockery::mock(LoggerInterface::class);
        $cronLogger->shouldReceive('notice');

        return new NewsCollectionCreator($scrapperProvider, $cronLogger);
    }
}
