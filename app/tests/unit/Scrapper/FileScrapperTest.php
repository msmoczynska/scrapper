<?php
declare(strict_types=1);

namespace App\Tests\unit\Scrapper;

use App\Entity\News;
use App\Entity\Section;
use App\Entity\Source;
use App\EntityCollection\NewsCollection;
use App\Exception\WrongRegexException;
use App\Repository\NewsRepository;
use App\Resolver\RegexResolver;
use App\Resolver\RegexResults;
use App\Resolver\RegexResultsCleaner;
use App\Scrapper\FileScrapper;
use App\Service\ContentFetcher;
use App\Tests\support\UnitTestCase;
use Mockery;
use Ramsey\Uuid\Uuid;

class FileScrapperTest extends UnitTestCase
{
    /**
     * @dataProvider provideDataForGetNews
     * @param string $path
     * @param Section $section
     * @param News|null $news
     * @param int $newsCountBeforeGetNews
     * @param int $newsCountAfterGetNews
     * @throws WrongRegexException
     */
    public function testGetNews(
        string $path,
        Section $section,
        ?News $news,
        int $newsCountBeforeGetNews,
        int $newsCountAfterGetNews
    ): void
    {
        $fileScrapper = $this->prepareFileScrapperWithMockedNews($news);

        $this->assertEquals($newsCountBeforeGetNews, $section->getNews()->count());
        $newsCollection = $fileScrapper->getNews($path, $section);

        $this->assertEquals($newsCountAfterGetNews, $newsCollection->count());

        $this->assertInstanceOf(NewsCollection::class, $newsCollection);
    }

    public function testSupport(): void
    {
        $source = new Source(
            Uuid::uuid4(),
            $this->projectDir . "/resources/fixtures/CosmeticNews.html",
            Source::TYPE_FILE,
            'source-title'
        );

        $fileScrapper = $this->prepareFileScrapperWithoutMockedNews();

        $this->assertTrue($fileScrapper->support($source));
    }

    /**
     * @return array
     */
    public function provideDataForGetNews(): array
    {
        $path = realpath($_SERVER["DOCUMENT_ROOT"]) . "/resources/fixtures/CosmeticNews.html";
        $source = new Source(Uuid::uuid4(), $path, Source::TYPE_FILE, 'source-title');

        $section1 = new Section(
            Uuid::uuid4(),
            'section1-name',
            "/class=\"title\">(?P<ref>[^\"]*)<\/a>/",
            $source
        );

        $news1 = null;

        $newsCountBeforeGetNews1 = 0;
        $newsCountAfterGetNews1 = 3;

        $section2 = new Section(
            Uuid::uuid4(),
            'section2-name',
            "/class=\"title\">(?P<ref>[^\"]*)<\/a>/",
            $source
        );

        $news2 = new News(Uuid::uuid4(), 'news-title', $section2);

        $newsCollection = new NewsCollection();
        $newsCollection->add($news2);

        $section2->appendNews($newsCollection);

        $newsCountBeforeGetNews2 = 1;
        $newsCountAfterGetNews2 = 3;

        return [
            "newsNullData" => [
                $path,
                $section1,
                $news1,
                $newsCountBeforeGetNews1,
                $newsCountAfterGetNews1
            ],
            "newsExistData" => [
                $path,
                $section2,
                $news2,
                $newsCountBeforeGetNews2,
                $newsCountAfterGetNews2
            ],
        ];
    }

    /**
     * @param News|null $news
     * @return FileScrapper
     */
    private function prepareFileScrapperWithMockedNews(?News $news): FileScrapper
    {
        $contentFetcher = Mockery::mock(ContentFetcher::class);
        $contentFetcher
            ->shouldReceive('fetch')
            ->andReturn("class=\"title\">News1</a> class=\"title\">News2</a> class=\"title\">News3</a>");

        $regexResolver = Mockery::mock(RegexResolver::class);
        $regexResolver
            ->shouldReceive('resolve')
            ->andReturn(new RegexResults(['news1-title', 'news2-title', 'news3-title']));

        $regexResultsCleaner = Mockery::mock(RegexResultsCleaner::class);
        $regexResultsCleaner
            ->shouldReceive('clean')
            ->andReturn(new RegexResults(['news1-title', 'news2-title', 'news3-title']));

        $newsRepository = Mockery::mock(NewsRepository::class);
        $newsRepository
            ->shouldReceive('findOneBy')
            ->andReturn($news);

        return new FileScrapper($contentFetcher, $regexResolver, $regexResultsCleaner,$newsRepository);
    }

    /**
     * @return FileScrapper
     */
    private function prepareFileScrapperWithoutMockedNews(): FileScrapper
    {
        $contentFetcher = Mockery::mock(ContentFetcher::class);
        $regexResolver = Mockery::mock(RegexResolver::class);
        $regexResultsCleaner = Mockery::mock(RegexResultsCleaner::class);
        $newsRepository = Mockery::mock(NewsRepository::class);

        return new FileScrapper($contentFetcher, $regexResolver, $regexResultsCleaner, $newsRepository);
    }
}
