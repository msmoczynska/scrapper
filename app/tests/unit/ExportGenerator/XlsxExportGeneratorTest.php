<?php
declare(strict_types=1);

namespace App\Tests\unit\ExportGenerator;

use App\Enum\ExportDetails;
use App\ExportGenerator\XlsxExportGenerator;
use App\Tests\support\UnitTestCase;
use PhpOffice\PhpSpreadsheet\Writer\Exception;

class XlsxExportGeneratorTest extends UnitTestCase
{
    /**
     * @dataProvider provideDataForGenerate
     * @param array $exportContent
     * @param string $type
     * @throws Exception
     */
    public function testGenerate(array $exportContent, string $type): void
    {
        $xlsxExportGenerator = new XlsxExportGenerator();

        $fileName = $xlsxExportGenerator->generate($exportContent, $type);

        $this->assertIsString($fileName);
        $this->assertStringContainsString($type . 'List.xlsx', $fileName);
    }

    public function testSupport(): void
    {
        $xlsxExportGenerator = new XlsxExportGenerator();

        $this->assertTrue($xlsxExportGenerator->support(ExportDetails::XLSX_FORMAT));
    }

    /**
     * @return array
     */
    public function provideDataForGenerate(): array
    {
        $exportContent1 = [];
        $type1 = ExportDetails::SOURCE_TYPE;

        return [
            "sourceExport" => [$exportContent1, $type1]
        ];
    }
}
