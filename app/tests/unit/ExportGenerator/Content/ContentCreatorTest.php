<?php
declare(strict_types=1);

namespace App\Tests\unit\ExportGenerator\Content;

use App\Enum\ExportDetails;
use App\Exception\MissingContentCreatorException;
use App\ExportGenerator\Content\ContentCreator;
use App\ExportGenerator\Content\ContentCreatorInterface;
use App\ExportGenerator\Content\SourceContentCreator;
use App\Tests\support\UnitTestCase;
use Iterator;
use Mockery;

class ContentCreatorTest extends UnitTestCase
{
    /**
     * @dataProvider provideDataForCreateContentForProperItemsType
     * @param string $type
     * @param ContentCreatorInterface $properContentCreator
     * @throws MissingContentCreatorException
     */
    public function testCreateContentForProperItemsType(string $type, ContentCreatorInterface $properContentCreator): void
    {
        $properContentCreator
            ->shouldReceive('support')
            ->andReturn(true);

        $properContentCreator
            ->shouldReceive('create')
            ->andReturn([]);

        $contentCreator = $this->prepareContentCreator($properContentCreator);

        $this->assertIsArray($contentCreator->createContentForProperItemsType($type));
    }

    /**
     * @throws MissingContentCreatorException
     */
    public function testCreateContentForProperItemsTypeWithMissingContentCreatorException(): void
    {
        $contentCreator = new ContentCreator([]);

        $this->expectException(MissingContentCreatorException::class);

        $contentCreator->createContentForProperItemsType("wrongType");
    }

    /**
     * @return array
     */
    public function provideDataForCreateContentForProperItemsType(): array
    {
        return [
            "sourceType" => [ExportDetails::SOURCE_TYPE, Mockery::mock(SourceContentCreator::class)]
        ];
    }

    /**
     * @param ContentCreatorInterface $contentCreator
     * @return ContentCreator
     */
    private function prepareContentCreator(ContentCreatorInterface $contentCreator): ContentCreator
    {
        $contentCreators = Mockery::mock(Iterator::class);
        $contentCreators
            ->shouldReceive('next')
            ->andReturn($contentCreator);
        $contentCreators->shouldReceive('rewind');
        $contentCreators
            ->shouldReceive('valid')
            ->andReturn(true);
        $contentCreators
            ->shouldReceive('current')
            ->andReturn($contentCreator);

        return new ContentCreator($contentCreators);
    }
}
