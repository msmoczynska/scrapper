<?php
declare(strict_types=1);

namespace App\Tests\unit\ExportGenerator;

use App\Enum\ExportDetails;
use App\ExportGenerator\CsvExportGenerator;
use App\Tests\support\UnitTestCase;

class CsvExportGeneratorTest extends UnitTestCase
{
    /**
     * @dataProvider provideDataForGenerate
     * @param array $exportContent
     * @param string $type
     */
    public function testGenerate(array $exportContent, string $type): void
    {
        $csvExportGenerator = new CsvExportGenerator();

        $fileName = $csvExportGenerator->generate($exportContent, $type);

        $this->assertIsString($fileName);
        $this->assertStringContainsString($type . 'List.csv', $fileName);
    }

    public function testSupport(): void
    {
        $csvExportGenerator = new CsvExportGenerator();

        $this->assertTrue($csvExportGenerator->support(ExportDetails::CSV_FORMAT));
    }

    /**
     * @return array
     */
    public function provideDataForGenerate(): array
    {
        $exportContent1 = [];
        $type1 = ExportDetails::SOURCE_TYPE;

        return [
            "sourceExport" => [$exportContent1, $type1]
        ];
    }
}
