<?php
declare(strict_types=1);

namespace App\Tests\unit\ExportGenerator;

use App\Enum\ExportDetails;
use App\Exception\MissingExportGeneratorException;
use App\ExportGenerator\CsvExportGenerator;
use App\ExportGenerator\ExportGeneratorInterface;
use App\ExportGenerator\ExportGeneratorProvider;
use App\ExportGenerator\XlsxExportGenerator;
use App\Tests\support\UnitTestCase;
use Iterator;
use Mockery;

class ExportGeneratorProviderTest extends UnitTestCase
{
    /**
     * @dataProvider provideDataForProvideExportGenerator
     * @param string $exportFormat
     * @param string $exportGeneratorClassName
     * @param ExportGeneratorInterface $exportGenerator
     * @throws MissingExportGeneratorException
     */
    public function testProvideExportGenerator(
        string $exportFormat,
        string $exportGeneratorClassName,
        ExportGeneratorInterface $exportGenerator
    ): void
    {
        $exportGenerator
            ->shouldReceive('support')
            ->andReturn(true);

        $exportGeneratorProvider = $this->prepareExportGeneratorProvider($exportGenerator);

        $this->assertInstanceOf($exportGeneratorClassName, $exportGeneratorProvider->provide($exportFormat));
    }

    /**
     * @throws MissingExportGeneratorException
     */
    public function testProvideExportGeneratorWithMissingExportGeneratorException(): void
    {
        $exportGeneratorProvider = new ExportGeneratorProvider([]);

        $this->expectException(MissingExportGeneratorException::class);

        $exportGeneratorProvider->provide("wrongFormat");
    }

    /**
     * @return array
     */
    public function provideDataForProvideExportGenerator(): array
    {
        return [
            "xlsxFormat" => [
                ExportDetails::XLSX_FORMAT,
                XlsxExportGenerator::class,
                Mockery::mock(XlsxExportGenerator::class),
            ],
            "csvFormat" => [
                ExportDetails::CSV_FORMAT,
                CsvExportGenerator::class,
                Mockery::mock(CsvExportGenerator::class),
            ],
        ];
    }

    /**
     * @param ExportGeneratorInterface $exportGenerator
     * @return ExportGeneratorProvider
     */
    private function prepareExportGeneratorProvider(ExportGeneratorInterface $exportGenerator): ExportGeneratorProvider
    {
        $exportGenerators = Mockery::mock(Iterator::class);
        $exportGenerators
            ->shouldReceive('next')
            ->andReturn($exportGenerator);
        $exportGenerators->shouldReceive('rewind');
        $exportGenerators
            ->shouldReceive('valid')
            ->andReturn(true);
        $exportGenerators
            ->shouldReceive('current')
            ->andReturn($exportGenerator);

        return new ExportGeneratorProvider($exportGenerators);
    }
}
