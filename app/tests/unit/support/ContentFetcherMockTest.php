<?php
declare(strict_types=1);

namespace App\Tests\unit\support;

use App\Exception\MissingContentException;
use App\Tests\support\ContentFetcherMock;
use App\Tests\support\UnitTestCase;

class ContentFetcherMockTest extends UnitTestCase
{
    /**
     * @throws MissingContentException
     */
    public function testFetch(): void
    {
        $path = $this->projectDir . "/tests/data/fakeContent.html";
        $content = "class=\"title\">Fake news 1</a> class=\"title\">Fake news 2</a> class=\"title\">Fake news 3</a>";

        $contentFetcherMock = new ContentFetcherMock();
        $contentFetcherMock::addNewContent($path, $content);

        $this->assertEquals($content, $contentFetcherMock->fetch($path));
    }

    /**
     * @throws MissingContentException
     */
    public function testFetchWithMissingContentException(): void
    {
        $contentFetcherMock = new ContentFetcherMock();

        $this->expectException(MissingContentException::class);
        $this->expectExceptionMessage("Content for given path is not exist.");

        $contentFetcherMock->fetch("/tests/data/fakeContent.html");
    }
}
