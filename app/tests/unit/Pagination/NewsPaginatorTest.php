<?php
declare(strict_types=1);

namespace App\Tests\unit\Pagination;

use App\Entity\News;
use App\Entity\Section;
use App\Entity\Source;
use App\Pagination\NewsPaginator;
use App\Pagination\PaginationParams;
use App\Repository\NewsRepository;
use App\Tests\support\UnitTestCase;
use Doctrine\ORM\NonUniqueResultException;
use JMS\Serializer\SerializerInterface;
use Mockery;
use Ramsey\Uuid\Uuid;

class NewsPaginatorTest extends UnitTestCase
{
    /**
     * @throws NonUniqueResultException
     */
    public function testGetPaginatedList(): void
    {
        $section = new Section(
            Uuid::uuid4(),
            "Section-name",
            "/<p class=\"tytul\">\s+(?P<ref>[^\"]*)<\/p>/",
            new Source(Uuid::uuid4(), "www.test-url.com", Source::TYPE_HTTP, 'source-title')
        );

        $newsRepository = Mockery::mock(NewsRepository::class);
        $newsRepository
            ->shouldReceive('countAllItems')
            ->andReturn(41);

        $newsRepository
            ->shouldReceive('findItemsForPage')
            ->andReturn([new News(Uuid::uuid4(), 'news-title', $section)]);

        $serializer = Mockery::mock(SerializerInterface::class);
        $serializer
            ->shouldReceive('toArray')
            ->andReturn(['news']);

        $newsPaginator = new NewsPaginator($newsRepository, $serializer);
        $limit = 10;
        $page = 5;

        $this->assertEquals(
            [
                "limit" => 10,
                "page" => 5,
                "pages" => 5,
                "selected" => 1,
                "total" => 41,
                "firstOrdinalNumber" => 41,
            ],
            $newsPaginator->getPaginatedList(new PaginationParams($limit, $page))->pagination
        );
    }
}
