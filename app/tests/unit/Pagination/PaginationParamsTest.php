<?php
declare(strict_types=1);

namespace App\Tests\unit\Pagination;

use App\Pagination\PaginationParams;
use PHPUnit\Framework\TestCase;

class PaginationParamsTest extends TestCase
{
    public function testGetSearchPhrase(): void
    {
        $paginationParams = new PaginationParams(10, 1);
        $paginationParams->setSearchPhrase("test");

        $this->assertEquals("test", $paginationParams->getSearchPhrase());
    }

    /**
     * @param PaginationParams $paginationParams
     * @param int $numberOfAllNews
     * @param array $expectedArray
     * @dataProvider provideData
     */
    public function testIsItOutputProperPaginationParamsArray(
        PaginationParams $paginationParams,
        int $numberOfAllNews,
        array $expectedArray
    ): void
    {
        $this->assertEquals($expectedArray, $paginationParams->createPaginationParamsArray($numberOfAllNews));
    }

    /**
     * @return array
     */
    public function provideData(): array
    {
        $paginationParams1 = new PaginationParams(10, 5);
        $expectedArray1 = [
            "limit" => 10,
            "page" => 5,
            "pages" => 5,
            "selected" => 1,
            "total" => 41,
            "firstOrdinalNumber" => 41,
        ];

        $paginationParams2 = new PaginationParams(5, 5);
        $expectedArray2 = [
            "limit" => 5,
            "page" => 5,
            "pages" => 9,
            "selected" => 5,
            "total" => 41,
            "firstOrdinalNumber" => 21,
        ];

        $paginationParams3 = new PaginationParams(10);
        $expectedArray3 = [
            "limit" => 10,
            "page" => 1,
            "pages" => 5,
            "selected" => 10,
            "total" => 41,
            "firstOrdinalNumber" => 1,
        ];

        $numberOfAllNews = 41;

        return [
            'paginationParams1' => [$paginationParams1, $numberOfAllNews, $expectedArray1],
            'paginationParams2' => [$paginationParams2, $numberOfAllNews, $expectedArray2],
            'paginationParams3' => [$paginationParams3, $numberOfAllNews, $expectedArray3]
        ];
    }
}
