<?php
declare(strict_types=1);

namespace App\Tests\unit\Pagination;

use App\Entity\Source;
use App\Pagination\PaginationParams;
use App\Pagination\SourcePaginator;
use App\Repository\SourceRepository;
use App\Tests\support\UnitTestCase;
use Doctrine\ORM\NonUniqueResultException;
use JMS\Serializer\SerializerInterface;
use Mockery;
use Ramsey\Uuid\Uuid;

class SourcePaginatorTest extends UnitTestCase
{
    /**
     * @throws NonUniqueResultException
     */
    public function testGetPaginatedList(): void
    {
        $sourceRepository = Mockery::mock(SourceRepository::class);
        $sourceRepository
            ->shouldReceive('countAllItems')
            ->andReturn(5);

        $sourceRepository
            ->shouldReceive('findItemsForPage')
            ->andReturn([new Source(Uuid::uuid4(), "www.test-url.com", Source::TYPE_HTTP, 'source-title')]);

        $serializer = Mockery::mock(SerializerInterface::class);
        $serializer
            ->shouldReceive('toArray')
            ->andReturn(['source']);

        $sourcePaginator = new SourcePaginator($sourceRepository, $serializer);
        $limit = 10;
        $page = 1;

        $this->assertEquals(
            [
                "limit" => 10,
                "page" => 1,
                "pages" => 1,
                "selected" => 5,
                "total" => 5,
                "firstOrdinalNumber" => 1,
            ],
            $sourcePaginator->getPaginatedList(new PaginationParams($limit, $page))->pagination
        );
    }
}
