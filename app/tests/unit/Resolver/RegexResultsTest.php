<?php
declare(strict_types=1);

namespace App\Tests\unit\Resolver;

use App\Resolver\RegexResults;
use PHPUnit\Framework\TestCase;

class RegexResultsTest extends TestCase
{
    public function testGetUniqueResults(): void
    {
        $regexResults = new RegexResults([1, 2, 2, 3, 4, 5, 5]);

        $uniqueResults = $regexResults->getUniqueResults();

        $this->assertCount(5, $uniqueResults);
        $this->assertEquals([1, 2, 3, 4, 5], $uniqueResults);

        $this->assertCount(7, $regexResults);
        $this->assertEquals([1, 2, 2, 3, 4, 5, 5], $regexResults->toArray());
    }
}
