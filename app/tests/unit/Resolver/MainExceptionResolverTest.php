<?php
declare(strict_types=1);

namespace App\Tests\unit\Resolver;

use App\Exception\ConfirmationTokenNotFoundException;
use App\Message\User\ConfirmUser;
use App\Resolver\MainExceptionResolver;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\Exception\HandlerFailedException;
use Throwable;

class MainExceptionResolverTest extends TestCase
{
    /**
     * @param Throwable $exception
     * @dataProvider provideDataForTestResolve
     */
    public function testResolve(Throwable $exception): void
    {
        $mainExceptionResolver = new MainExceptionResolver();

        $this->assertInstanceOf(ConfirmationTokenNotFoundException::class, $mainExceptionResolver->resolve($exception));
    }

    /**
     * @return array
     */
    public function provideDataForTestResolve(): array
    {
        $exception1 = new ConfirmationTokenNotFoundException();

        $exception2 = new HandlerFailedException(
            new Envelope(ConfirmUser::create("wrongConfirmationToken")),
            [new ConfirmationTokenNotFoundException()]
        );

        $exception3 = new HandlerFailedException(
            new Envelope(ConfirmUser::create("wrongConfirmationToken")),
            [new HandlerFailedException(
                new Envelope(ConfirmUser::create("wrongConfirmationToken")),
                [new ConfirmationTokenNotFoundException()]
            )]
        );

        return [
            "simpleException" => [$exception1],
            "nestedException" => [$exception2],
            "doubleNestedException" => [$exception3]
        ];
    }
}
