<?php
declare(strict_types=1);

namespace App\Tests\unit\Resolver;

use App\Resolver\RegexResults;
use App\Resolver\RegexResultsCleaner;
use App\Tests\support\UnitTestCase;

class RegexResultsCleanerTest extends UnitTestCase
{
    public function testClean(): void
    {
        $results = new RegexResults(["news1-'title   ", "news2-title    ", "news'3-title"]);
        $regexResultCleaner = new RegexResultsCleaner();

        $this->assertEquals(new RegexResults(['news1-title', 'news2-title', 'news3-title']), $regexResultCleaner->clean($results));
    }
}
