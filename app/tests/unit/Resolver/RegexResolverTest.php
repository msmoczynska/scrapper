<?php

namespace App\Tests\unit\Resolver;

use App\Exception\WrongRegexException;
use App\Resolver\RegexResolver;
use PHPUnit\Framework\TestCase;

class RegexResolverTest extends TestCase
{
    public function testResolve(): void
    {
        $regexResolver = new RegexResolver();
        $regex = '/class="title">(?P<ref>[^"]*)<\/a>/';
        $sourceContent = 'class="title">News1</a> class="title">News2</a>';

        $this->assertCount(2, $regexResolver->resolve($regex, $sourceContent));
    }

    /**
     * @throws WrongRegexException
     */
    public function testWrongRegexException(): void
    {
        $regexResolver = new RegexResolver();
        $regex = 'wrong-regex';
        $sourceContent = 'class="title">News1</a> class="title">News2</a>';

        $this->expectException(WrongRegexException::class);

        $regexResolver->resolve($regex, $sourceContent);
    }
}
