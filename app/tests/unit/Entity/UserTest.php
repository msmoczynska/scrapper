<?php
declare(strict_types=1);

namespace App\Tests\unit\Entity;

use App\Entity\User;
use App\Enum\Roles;
use DateTime;
use DateTimeInterface;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class UserTest extends TestCase
{
    /**
     * @param User $user
     * @param UuidInterface $expectedId
     * @param string $expectedEmail
     * @param array $expectedRoles
     * @param string $expectedPassword
     * @param string $expectedFirstName
     * @param string $expectedSecondName
     * @param DateTime $expectedBirthDate
     * @param string $expectedConfirmationToken
     * @param bool $expectedVerified
     * @param string $expectedResetPasswordToken
     * @dataProvider provideDataForGettersAndSettersTest
     */
    public function testGettersAndSetters(
        User $user,
        UuidInterface $expectedId,
        string $expectedEmail,
        array $expectedRoles,
        string $expectedPassword,
        string $expectedFirstName,
        string $expectedSecondName,
        DateTime $expectedBirthDate,
        string $expectedConfirmationToken,
        bool $expectedVerified,
        string $expectedResetPasswordToken
    ): void
    {
        $this->assertInstanceOf(UuidInterface::class, $user->getId());
        $this->assertEquals($expectedId, $user->getId());
        $this->assertIsString($user->getEmail());
        $this->assertEquals($expectedEmail, $user->getEmail());
        $this->assertIsString($user->getUsername());
        $this->assertEquals($expectedEmail, $user->getUsername());
        $this->assertIsArray($user->getRoles());
        $this->assertEquals($expectedRoles, $user->getRoles());
        $this->assertIsString($user->getPassword());
        $this->assertEquals($expectedPassword, $user->getPassword());
        $this->assertIsString($user->getFirstName());
        $this->assertEquals($expectedFirstName, $user->getFirstName());
        $this->assertIsString($user->getSecondName());
        $this->assertEquals($expectedSecondName, $user->getSecondName());
        $this->assertInstanceOf(DateTimeInterface::class, $user->getBirthDate());
        $this->assertEquals($expectedBirthDate, $user->getBirthDate());
        $this->assertIsString($user->getConfirmationToken());
        $this->assertEquals($expectedConfirmationToken, $user->getConfirmationToken());
        $this->assertIsBool($user->isVerified());
        $this->assertEquals($expectedVerified, $user->isVerified());
        $this->assertIsString($user->getResetPasswordToken());
        $this->assertEquals($expectedResetPasswordToken, $user->getResetPasswordToken());

    }

    /**
     * @return array
     */
    public function provideDataForGettersAndSettersTest(): array
    {
        $id1 = Uuid::uuid4();
        $email1 = "email1@test.pl";
        $roles1 = [Roles::ROLE_USER];
        $password1 = "password1";
        $firstName1 = "firstname1";
        $secondName1 = "secondname1";
        $birthDate1 = new DateTime("2000-01-01");
        $confirmationToken1 = 'confirmationtoken1';
        $verified1 = true;
        $resetPasswordToken1 = 'resetpasswordtoken1';


        $user1 = new User($id1, $email1, $firstName1, $secondName1, $birthDate1, $confirmationToken1);
        $user1
            ->setPassword($password1)
            ->setVerified($verified1)
            ->setResetPasswordToken($resetPasswordToken1);

        $id2 = Uuid::uuid4();
        $email2 = "email2@test.pl";
        $roles2 = [Roles::ROLE_USER];
        $password2 = "password2";
        $firstName2 = "firstname2";
        $secondName2 = "secondname2";
        $birthDate2 = new DateTime("2000-02-02");
        $confirmationToken2 = 'confirmationtoken2';
        $verified2 = true;
        $resetPasswordToken2 = 'resetpasswordtoken2';

        $user2 = new User($id2, $email2, $firstName2, $secondName2, $birthDate2, $confirmationToken2);
        $user2
            ->setPassword($password2)
            ->setVerified($verified2)
            ->setResetPasswordToken($resetPasswordToken2);

        $id3 = Uuid::uuid4();
        $email3 = "email3@test.pl";
        $roles3 = [Roles::ROLE_USER, Roles::ROLE_ADMIN];
        $password3 = "password3";
        $firstName3 = "firstname3";
        $secondName3 = "secondname3";
        $birthDate3 = new DateTime("2000-03-03");
        $confirmationToken3 = 'confirmationtoken3';
        $verified3 = true;
        $resetPasswordToken3 = 'resetpasswordtoken3';

        $user3 = new User($id3, $email3, $firstName3, $secondName3, $birthDate3, $confirmationToken3);
        $user3
            ->addRole(Roles::ROLE_ADMIN)
            ->setPassword($password3)
            ->setVerified($verified3)
            ->setResetPasswordToken($resetPasswordToken3);

        return [
            'user1' => [$user1, $id1, $email1, $roles1, $password1, $firstName1, $secondName1, $birthDate1, $confirmationToken1, $verified1, $resetPasswordToken1],
            'user2' => [$user2, $id2, $email2, $roles2, $password2, $firstName2, $secondName2, $birthDate2, $confirmationToken2, $verified2, $resetPasswordToken2],
            'user3' => [$user3, $id3, $email3, $roles3, $password3, $firstName3, $secondName3, $birthDate3, $confirmationToken3, $verified3, $resetPasswordToken3]
        ];
    }
}
