<?php
declare(strict_types=1);

namespace App\Tests\unit\Entity;

use App\Entity\News;
use App\Entity\Section;
use App\Entity\Source;
use App\EntityCollection\NewsCollection;
use DateTimeInterface;
use Doctrine\Common\Collections\Collection;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class SourceTest extends TestCase
{
    /**
     * @dataProvider provideDataForGettersAndSettersTest
     * @param Source $source
     * @param UuidInterface $expectedId
     * @param string $expectedPath
     * @param string $expectedType
     * @param string $expectedTitle
     * @param array $expectedNewsCount
     */
    public function testGettersAndSetters(
        Source $source,
        UuidInterface $expectedId,
        string $expectedPath,
        string $expectedType,
        string $expectedTitle,
        array $expectedNewsCount
    ): void
    {
        $this->assertInstanceOf(UuidInterface::class, $source->getId());
        $this->assertEquals($expectedId, $source->getId());
        $this->assertIsString($source->getPath());
        $this->assertEquals($expectedPath, $source->getPath());
        $this->assertIsString($source->getType());
        $this->assertEquals($expectedType, $source->getType());
        $this->assertIsString($source->getTitle());
        $this->assertEquals($expectedTitle, $source->getTitle());
        $this->assertInstanceOf(DateTimeInterface::class, $source->getCreatedAt());
        $this->assertInstanceOf(Collection::class, $source->getSections());
        $this->assertIsInt($source->getRecentNewsCount());
        $this->assertEquals($expectedNewsCount["source-recent"], $source->getRecentNewsCount());
        $this->assertIsInt($source->getActiveNewsCount());
        $this->assertEquals($expectedNewsCount["source-active"], $source->getActiveNewsCount());
        $this->assertIsInt($source->getOutdatedNewsCount());
        $this->assertEquals($expectedNewsCount["source-outdated"], $source->getOutdatedNewsCount());
        $this->assertIsInt($source->getAllNewsCount());
        $this->assertEquals($expectedNewsCount["source-all"], $source->getAllNewsCount());
    }

    public function testAddSection(): void
    {
        $source = new Source(
            Uuid::uuid4(),
            "www.test-url.com",
            Source::TYPE_HTTP,
            'source-title'
        );

        $section = new Section(
            Uuid::uuid4(),
            "Section-name",
            "/<p class=\"tytul\">\s+(?P<ref>[^\"]*)<\/p>/",
            $source
        );

        $this->assertEquals(0, $source->getSections()->count());
        $source->addSection($section);

        $this->assertEquals(1, $source->getSections()->count());
    }

    public function testGetAllNews(): void
    {
        $source = new Source(
            Uuid::uuid4(),
            "www.test-url.com",
            Source::TYPE_HTTP,
            'source-title'
        );

        $section = new Section(
            Uuid::uuid4(),
            "Section-name",
            "/<p class=\"tytul\">\s+(?P<ref>[^\"]*)<\/p>/",
            $source
        );

        $source->addSection($section);

        $testNews = new News(Uuid::uuid4(), 'test-news-title', $section);

        $newsCollection = new NewsCollection();
        $newsCollection->add($testNews);

        $this->assertEquals(0, $source->getAllNews()->count());
        $section->appendNews($newsCollection);

        $this->assertEquals(1, $source->getAllNews()->count());
    }

    /**
     * @return array
     */
    public function provideDataForGettersAndSettersTest(): array
    {
        $id1 = Uuid::uuid4();
        $path1 = "www.test-url1.com";
        $type1 = Source::TYPE_HTTP;
        $title1 = 'source1-title';
        $newsCount1 = [
            "source-recent" => 3,
            "source-active" => 8,
            "source-outdated" => 24,
            "source-all" => 32,
        ];

        $source1 = new Source($id1, $path1, $type1, $title1);
        $source1
            ->setRecentNewsCount($newsCount1["source-recent"])
            ->setActiveNewsCount($newsCount1["source-active"])
            ->setOutdatedNewsCount($newsCount1["source-outdated"])
            ->setAllNewsCount($newsCount1["source-all"]);

        $id2 = Uuid::uuid4();
        $path2 = "www.test-url2.com";
        $type2 = Source::TYPE_HTTP;
        $title2 = 'source2-title';
        $newsCount2 = [
            "source-recent" => 2,
            "source-active" => 10,
            "source-outdated" => 20,
            "source-all" => 30,
        ];

        $source2 = new Source($id2, $path2, $type2, $title2);
        $source2
            ->setRecentNewsCount($newsCount2["source-recent"])
            ->setActiveNewsCount($newsCount2["source-active"])
            ->setOutdatedNewsCount($newsCount2["source-outdated"])
            ->setAllNewsCount($newsCount2["source-all"]);

        $id3 = Uuid::uuid4();
        $path3 = "/resources/fixtures/CracowSchoolsNews.html";
        $type3 = Source::TYPE_FILE;
        $title3 = 'source3-title';
        $newsCount3 = [
            "source-recent" => 4,
            "source-active" => 10,
            "source-outdated" => 40,
            "source-all" => 50,
        ];

        $source3 = new Source($id3, $path3, $type3, $title3);
        $source3
            ->setRecentNewsCount($newsCount3["source-recent"])
            ->setActiveNewsCount($newsCount3["source-active"])
            ->setOutdatedNewsCount($newsCount3["source-outdated"])
            ->setAllNewsCount($newsCount3["source-all"]);

        return [
            'source1' => [$source1, $id1, $path1, $type1, $title1, $newsCount1],
            'source2' => [$source2, $id2, $path2, $type2, $title2, $newsCount2],
            'source3' => [$source3, $id3, $path3, $type3, $title3, $newsCount3],
        ];
    }
}
