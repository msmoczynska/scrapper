<?php
declare(strict_types=1);

namespace App\Tests\unit\Entity;

use App\Entity\News;
use App\Entity\Section;
use App\Entity\Source;
use DateTime;
use DateTimeInterface;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class NewsTest extends TestCase
{
    /**
     * @param News $news
     * @param UuidInterface $expectedId
     * @param string $expectedTitle
     * @param DateTime $expectedUpdatedAt
     * @param bool $expectedEnabled
     * @param Section $expectedSection
     * @param string $expectedOutdated
     * @param bool $expectedRecent
     * @dataProvider provideDataForNewsTest
     */
    public function testGettersAndSetters(
        News $news,
        UuidInterface $expectedId,
        string $expectedTitle,
        DateTime $expectedUpdatedAt,
        bool $expectedEnabled,
        Section $expectedSection,
        string $expectedOutdated,
        bool $expectedRecent
    ): void
    {
        $this->assertInstanceOf(UuidInterface::class, $news->getId());
        $this->assertEquals($expectedId, $news->getId());
        $this->assertIsString($news->getTitle());
        $this->assertEquals($expectedTitle, $news->getTitle());
        $this->assertInstanceOf(DateTimeInterface::class, $news->getCreatedAt());
        $this->assertInstanceOf(DateTimeInterface::class, $news->getUpdatedAt());
        $this->assertEquals($expectedUpdatedAt, $news->getUpdatedAt());
        $this->assertIsBool($news->getEnabled());
        $this->assertEquals($expectedEnabled, $news->getEnabled());
        $this->assertInstanceOf(Section::class, $news->getSection());
        $this->assertEquals($expectedSection, $news->getSection());
        $this->assertIsString($news->getOutdated());
        $this->assertEquals($expectedOutdated, $news->getOutdated());
        $this->assertIsBool($news->getRecent());
        $this->assertEquals($expectedRecent, $news->getRecent());
    }

    /**
     * @return array
     */
    public function provideDataForNewsTest(): array
    {
        $id1 = Uuid::uuid4();
        $id2 = Uuid::uuid4();
        $id3 = Uuid::uuid4();

        $title1 = 'news1-title';
        $title2 = 'news2-title';
        $title3 = 'news3-title';

        $updatedAt1 = new DateTime();
        $updatedAt2 = new DateTime();
        $updatedAt3 = new DateTime();

        $enabled1 = true;
        $enabled2 = true;
        $enabled3 = false;

        $section = new Section(
            Uuid::uuid4(),
            "Section-name",
            "/<p class=\"tytul\">\s+(?P<ref>[^\"]*)<\/p>/",
            new Source(Uuid::uuid4(), "www.test-url.com", Source::TYPE_HTTP, 'title')
        );

        $news1 = new News($id1, $title1, $section, $enabled1);
        $news1->setUpdatedAt($updatedAt1);

        $news2 = new News($id2, $title2, $section, $enabled2);
        $news2->setUpdatedAt($updatedAt2);

        $news3 = new News($id3, $title3, $section, $enabled3);
        $news3->setUpdatedAt($updatedAt3);

        $outdated1 = "-";
        $outdated2 = "-";
        $outdated3 = $updatedAt3->format("Y-m-d H:i");

        $recent = false;

        return [
            'news1' => [$news1, $id1, $title1, $updatedAt1, $enabled1, $section, $outdated1, $recent],
            'news2' => [$news2, $id2, $title2, $updatedAt2, $enabled2, $section, $outdated2, $recent],
            'news3' => [$news3, $id3, $title3, $updatedAt3, $enabled3, $section, $outdated3, $recent],
        ];
    }
}
