<?php
declare(strict_types=1);

namespace App\Tests\unit\Entity;

use App\Entity\News;
use App\Entity\Section;
use App\Entity\Source;
use App\EntityCollection\NewsCollection;
use DateTimeInterface;
use Doctrine\Common\Collections\Collection;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class SectionTest extends TestCase
{
    /**
     * @dataProvider provideDataForGettersAndSettersTest
     * @param Section $section
     * @param UuidInterface $expectedId
     * @param string $expectedName
     * @param string $expectedRegex
     * @param array $expectedNewsCount
     * @param Source $expectedSource
     */
    public function testGettersAndSetters(
        Section $section,
        UuidInterface $expectedId,
        string $expectedName,
        string $expectedRegex,
        array $expectedNewsCount,
        Source $expectedSource
    ): void
    {
        $this->assertInstanceOf(UuidInterface::class, $section->getId());
        $this->assertEquals($expectedId, $section->getId());
        $this->assertIsString($section->getName());
        $this->assertEquals($expectedName, $section->getName());
        $this->assertIsString($section->getRegex());
        $this->assertEquals($expectedRegex, $section->getRegex());
        $this->assertInstanceOf(DateTimeInterface::class, $section->getCreatedAt());
        $this->assertInstanceOf(Source::class, $section->getSource());
        $this->assertEquals($expectedSource, $section->getSource());
        $this->assertInstanceOf(Collection::class, $section->getNews());
        $this->assertIsInt($section->getRecentNewsCount());
        $this->assertEquals($expectedNewsCount["section-recent"], $section->getRecentNewsCount());
        $this->assertIsInt($section->getActiveNewsCount());
        $this->assertEquals($expectedNewsCount["section-active"], $section->getActiveNewsCount());
        $this->assertIsInt($section->getOutdatedNewsCount());
        $this->assertEquals($expectedNewsCount["section-outdated"], $section->getOutdatedNewsCount());
        $this->assertIsInt($section->getAllNewsCount());
        $this->assertEquals($expectedNewsCount["section-all"], $section->getAllNewsCount());
    }

    public function testGetEnabledNews(): void
    {
        $section = new Section(
            Uuid::uuid4(),
            "Section-name",
            "/<p class=\"tytul\">\s+(?P<ref>[^\"]*)<\/p>/",
            new Source(Uuid::uuid4(), "www.test-url.com", Source::TYPE_HTTP, 'source-title')
        );

        $testNews1 = new News(Uuid::uuid4(), 'test-news-title1', $section);
        $testNews2 = new News(Uuid::uuid4(), 'test-news-title2', $section);

        $section->getNews()->add($testNews1);
        $section->getNews()->add($testNews2);

        $this->assertEquals(2, $section->getEnabledNews()->count());
    }

    public function testAppendNewsIfNotIncluded(): void
    {
        $section = new Section(
            Uuid::uuid4(),
            "Section-name",
            "/<p class=\"tytul\">\s+(?P<ref>[^\"]*)<\/p>/",
            new Source(Uuid::uuid4(), "www.test-url.com", Source::TYPE_HTTP, 'source-title')
        );

        $testNews = new News(Uuid::uuid4(), 'test-news-title', $section);

        $newsCollection = new NewsCollection();
        $newsCollection->add($testNews);

        $this->assertEquals(0, $section->getNews()->count());
        $section->appendNews($newsCollection);

        $this->assertEquals(1, $section->getNews()->count());
    }

    public function testAppendNewsIfIncluded(): void
    {
        $section = new Section(
            Uuid::uuid4(),
            "Section-name",
            "/<p class=\"tytul\">\s+(?P<ref>[^\"]*)<\/p>/",
            new Source(Uuid::uuid4(), "www.test-url.com", Source::TYPE_HTTP, 'source-title')
        );

        $testNews = new News(Uuid::uuid4(), 'test-news-title', $section);

        $section->getNews()->add($testNews);

        $newsCollection = new NewsCollection();
        $newsCollection->add($testNews);

        $this->assertEquals(1, $section->getNews()->count());
        $section->appendNews($newsCollection);

        $this->assertEquals(1, $section->getNews()->count());
    }

    /**
     * @return array
     */
    public function provideDataForGettersAndSettersTest(): array
    {
        $source = new Source(
            Uuid::uuid4(),
            "www.test-url.com",
            Source::TYPE_HTTP,
            'source-title'
        );

        $id1 = Uuid::uuid4();
        $name1 = "Section-name1";
        $regex1 = "/<p class=\"tytul\">\s+(?P<ref>[^\"]*)<\/p>/";
        $newsCount1 = [
            "section-recent" => 3,
            "section-active" => 8,
            "section-outdated" => 24,
            "section-all" => 32,
        ];

        $section1 = new Section($id1, $name1, $regex1, $source);
        $section1
            ->setRecentNewsCount($newsCount1["section-recent"])
            ->setActiveNewsCount($newsCount1["section-active"])
            ->setOutdatedNewsCount($newsCount1["section-outdated"])
            ->setAllNewsCount($newsCount1["section-all"]);

        $id2 = Uuid::uuid4();
        $name2 = "Section-name2";
        $regex2 = "/<p class=\"tytul\">\s+(?P<ref>[^\"]*)<\/p>/";
        $newsCount2 = [
            "section-recent" => 2,
            "section-active" => 10,
            "section-outdated" => 20,
            "section-all" => 30,
        ];

        $section2 = new Section($id2, $name2, $regex2, $source);
        $section2
            ->setRecentNewsCount($newsCount2["section-recent"])
            ->setActiveNewsCount($newsCount2["section-active"])
            ->setOutdatedNewsCount($newsCount2["section-outdated"])
            ->setAllNewsCount($newsCount2["section-all"]);;

        return [
            'section1' => [$section1, $id1, $name1, $regex1, $newsCount1, $source],
            'section2' => [$section2, $id2, $name2, $regex2, $newsCount2, $source],
        ];
    }
}
