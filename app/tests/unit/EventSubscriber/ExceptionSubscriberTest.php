<?php
declare(strict_types=1);

namespace App\Tests\unit\EventSubscriber;

use App\Entity\Source;
use App\EventSubscriber\ExceptionSubscriber;
use App\Exception\FileNotFoundException;
use App\Exception\FileValidationException;
use App\Exception\ValidationException;
use App\Resolver\MainExceptionResolver;
use App\Tests\support\UnitTestCase;
use Exception;
use Mockery;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationList;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class ExceptionSubscriberTest extends UnitTestCase
{
    public function testGetSubscribedEvents(): void
    {
        $templating = Mockery::mock(Environment::class);
        $mainExceptionResolver = Mockery::mock(MainExceptionResolver::class);

        $exceptionSubscriber = new ExceptionSubscriber($templating, $mainExceptionResolver);

        $this->assertEquals(
            [KernelEvents::EXCEPTION => ['onKernelException', 10]],
            $exceptionSubscriber->getSubscribedEvents()
        );
    }

    /**
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function testOnKernelException(): void
    {
        $templating = Mockery::mock(Environment::class);
        $kernel = Mockery::mock(HttpKernelInterface::class);
        $request = Mockery::mock(Request::class);
        $request
            ->shouldReceive('isXmlHttpRequest')
            ->andReturn('true');

        $exception = new FileNotFoundException();

        $mainExceptionResolver = Mockery::mock(MainExceptionResolver::class);
        $mainExceptionResolver
            ->shouldReceive('resolve')
            ->andReturn($exception);

        $exceptionEvent = new ExceptionEvent($kernel, $request, HttpKernelInterface::MASTER_REQUEST, $exception);

        $exceptionSubscriber = new ExceptionSubscriber($templating, $mainExceptionResolver);
        $exceptionSubscriber->onKernelException($exceptionEvent);

        $this->assertEquals(["message" => "File not found."], json_decode($exceptionEvent->getResponse()->getContent(), true));
    }

    /**
     * @param Exception $exception
     * @param string $expectedMessage
     * @dataProvider provideDataForGenerateViolationMessage
     */
    public function testGenerateErrorMessage(Exception $exception, string $expectedMessage): void
    {
        $templating = Mockery::mock(Environment::class);
        $mainExceptionResolver = Mockery::mock(MainExceptionResolver::class);

        $exceptionSubscriber = new ExceptionSubscriber($templating, $mainExceptionResolver);

        $this->assertEquals($expectedMessage, $exceptionSubscriber->generateErrorMessage($exception));
    }

    /**
     * @return array
     */
    public function provideDataForGenerateViolationMessage(): array
    {
        $exception = new Exception("Test error message");

        $path = "www.test-url.com";
        $regex = "";

        $source = new Source(Uuid::uuid4(), $path, Source::TYPE_HTTP, 'source-title');

        $validationExceptionErrors = new ConstraintViolationList();
        $validationExceptionErrors->add(new ConstraintViolation(
            "This value should not be blank",
            "This value should not be blank",
            [],
            $source,
            "regex",
            $regex,
            null,
            "c1051bb4-d103-4f74-8988-acbcafc7fdc3",
            new NotBlank(),
            null
        ));

        $validationException = new ValidationException($validationExceptionErrors);

        $path = realpath($_SERVER["DOCUMENT_ROOT"]) . "/tests/data/TooLargeFile.png";
        $file = new UploadedFile($path, 'TooLargeFile.png');

        $fileValidationExceptionErrors = new ConstraintViolationList();
        $fileValidationExceptionErrors->add(new ConstraintViolation(
            "The file is too large (0.66 MB). Allowed maximum size is 0.3 MB.",
            "The file is too large ({{ size }} {{ suffix }}). Allowed maximum size is {{ limit }} {{ suffix }}.",
            [],
            $file,
            "",
            $file,
            null,
            "df8637af-d466-48c6-a59d-e7126250a654",
            new File(),
            null
        ));

        $fileValidationException = new FileValidationException($fileValidationExceptionErrors);

        $accessDeniedException = new AccessDeniedException();

        return [
            'exception' => [$exception, "Test error message"],
            'validationException' => [$validationException, "regex: This value should not be blank"],
            'fileValidationException' => [$fileValidationException, "The file is too large (0.66 MB). Allowed maximum size is 0.3 MB."],
            'accessDeniedException' => [$accessDeniedException, "Access denied."],
        ];
    }
}
