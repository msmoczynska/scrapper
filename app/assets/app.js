import 'bootstrap/dist/css/bootstrap.min.css';
import './styles/app.css';

const $ = require('jquery');
require('bootstrap');

$(document).ready(function() {
    $('[data-toggle="popover"]').popover();
});
