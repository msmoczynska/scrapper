$(document).on("click", "#set-new-password", setNewPassword);

function setNewPassword() {

    $.ajax({
        url: Routing.generate("api_reset_password", {}),
        type: 'post',
        data: $("#reset-password-form").serialize(),

        success: function () {
            alert('New password set.');
            $("#reset-password-form").hide();
        },
        error: function (data) {
            // TODO: handle error in proper way
            alert(data.responseJSON.message);
        }
    });

    return false;
}
