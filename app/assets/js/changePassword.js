$(document).on("click", "#change-password", changePassword);

function changePassword() {

    $.ajax({
        url: Routing.generate("api_change_password", {}),
        type: 'post',
        data: $("#change-password-form").serialize(),

        success: function () {
            alert('Password changed successfully.');
            $("#change-password-form").hide();
        },
        error: function (data) {
            // TODO: handle error in proper way
            alert(data.responseJSON.message);
        }
    });

    return false;
}
