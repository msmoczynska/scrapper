import {generatePagination} from "./utils/pagination";
import _ from "underscore";

const limit = 10;
const url = Routing.generate("api_source_list", {});

$(document).ready(function() {
    generateSourceListForPage(1);

    return false;
});

$(document).on("click", ".page", function() {
    generateSourceListForPage($(this).attr('value'));

    return false;
});

$(document).on('click', '#submit-search', function() {
    generateSourceListForPage(1);

    return false;
});

function generateSourceListForPage(page) {
    const search = $("input[name='search']").val();
    const query = {
        limit: limit,
        page: page,
    };

    if ("" !== search) {
        query["search"] = search;
    }

    // TODO: extract to a separate file
    $.ajax({
        url: url,
        type: 'get',
        data: $.param(query),
        processData: false,

        success: function (data) {
            generateSourceList(data);
            generatePagination(data);
        },
        error: function (data) {
            alert(data.responseJSON.message);
        }
    });

    return false;
}

function generateSourceList(data) {
    const sourceRowTemplate = _.template($("#source-row").html());
    const sectionRowTemplate = _.template($("#section-row").html());
    let sourceOrdinalNumber = data.pagination.firstOrdinalNumber;

    $("#source-list-table-body").html("");
    for (let i = 0; i < data.items.length; i++) {
        const pathRowContent = prepareTooltips(data.items[i].path);

        $("#source-list-table-body").append(
            sourceRowTemplate({
                ordinal_number : sourceOrdinalNumber,
                source_path: pathRowContent,
                source: data.items[i]
            })
        );

        for (let j = 0; j < data.items[i].sections.length; j++) {
            $("#source-list-table-body").append(
                sectionRowTemplate({ section: data.items[i].sections[j] })
            );
        }

        $('[data-toggle="tooltip"]').tooltip();

        sourceOrdinalNumber++;
    }
}

function prepareTooltips(path) {
    const sourceTooltipTemplate = _.template($("#source-tooltip").html());
    if (path.length > 80) {
        return sourceTooltipTemplate({ path: path });
    }

    return path;
}
