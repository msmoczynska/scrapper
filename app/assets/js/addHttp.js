import _ from 'underscore';

const url = Routing.generate("api_add_new_http", {});
const sectionFieldTemplate = _.template($("#section-field").html());
let sectionIndex = 0;

$(document).on("click", "#add-new-section", addSectionNameField);

function addSectionNameField() {
    $("#new-section-name-field").append(sectionFieldTemplate({ index : sectionIndex }));
    sectionIndex++;
    return false;
}

$(document).on("click", "#http-form-save", addSource);

function addSource() {

    const dataSerialized = $('#add-http-form').serializeArray();
    const path = dataSerialized[0].value;
    const sectionName = [];
    const regex = [];
    const sections = {};
    const dataObj = {
        path: path,
        sections: sections
    };
    let sectionNameIndex = 0;
    let regexIndex = 0;

    // TODO: refactor the method of retrieving data from the form
    for (let i = 1; i < dataSerialized.length; i = i + 2) {
        sectionName[sectionNameIndex] = dataSerialized[i].value;
        sectionNameIndex++;
    }

    for (let i = 2; i < dataSerialized.length; i = i + 2) {
        regex[regexIndex] = dataSerialized[i].value;
        regexIndex++;
    }

    for (let i = 0; i < sectionName.length; i++) {
        sections[sectionName[i]] = regex[i];
    }

    $.ajax({
        url: url,
        dataType: 'json',
        type: 'post',
        contentType: 'application/json',
        data: JSON.stringify(dataObj),
        processData: false,

        success: function () {
            alert('Source has been added to the database.');
        },
        error: function (data) {
            // TODO: handle error in proper way
            alert(data.responseJSON.message);
        }
    });

    return false;
}
