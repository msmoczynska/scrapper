$(document).on("click", "#register", register);

function register() {

    $.ajax({
        url: Routing.generate("api_register", {}),
        type: 'post',
        data: $("#registration-form").serialize(),

        success: function () {
            alert('Registration was successful. Check your email box and click on the link attached to confirm your registration.');
            $("#registration-form").hide();
        },
        error: function (data) {
            // TODO: handle error in proper way
            alert(data.responseJSON.message);
        }
    });

    return false;
}
