$(document).on("click", "#reset-password", setNewPassword);

function setNewPassword() {

    $.ajax({
        url: Routing.generate("api_forgot_password", {email: $("#email").val()}),
        processData: false,

        success: function () {
            alert('Check your e-mail box.');
            $("#forgot-password-form").hide();
        },
        error: function (data) {
            // TODO: handle error in proper way
            alert(data.responseJSON.message);
        }
    });

    return false;
}
