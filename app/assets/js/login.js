$(document).on("click", "#login", login);

function login() {

    $.ajax({
        url: Routing.generate("api_login", {}),
        type: 'post',
        data: $("#login-form").serialize(),

        success: function () {
            alert('You are successfully logged in!');
            window.location.href = Routing.generate("after_login", {});
        },
        error: function (data) {
            // TODO: handle error in proper way
            alert(data.responseJSON.message);
        }
    });

    return false;
}
