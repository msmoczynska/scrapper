import _ from 'underscore';

const url = Routing.generate("api_add_new_file", {});
const sectionFieldTemplate = _.template($("#section-field").html());
let sectionIndex = 0;

$(document).on("click", "#add-new-section", addSectionNameField);

function addSectionNameField() {
    $("#new-section-name-field").append(sectionFieldTemplate({ index : sectionIndex }));
    sectionIndex++;
    return false;
}

$(document).on('click', "#file-form-save", function() {
    const file_data = $('#file-form-load').prop('files')[0];
    const form_data = new FormData();
    const dataSerialized = $('#add-file-form').serializeArray();
    const sectionName = [];
    const regex = [];
    const sections = {};
    let sectionNameIndex = 0;
    let regexIndex = 0;
    let sectionsData;

    form_data.append('file', file_data);

    // TODO: refactor the method of retrieving data from the form
    for (let i = 0; i < dataSerialized.length; i = i + 2) {
        sectionName[sectionNameIndex] = dataSerialized[i].value;
        sectionNameIndex++;
    }

    for (let i = 1; i < dataSerialized.length; i = i + 2) {
        regex[regexIndex] = dataSerialized[i].value;
        regexIndex++;
    }

    for (let i = 0; i < sectionName.length; i++) {
        sections[sectionName[i]] = regex[i];
    }

    sectionsData = JSON.stringify(sections);
    form_data.append('sections', sectionsData);

    $.ajax({
        url: url,
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,
        type: 'post',
        success: function(){
            alert('Source has been added to the database.');
        },
        error: function (data) {
            // TODO: handle error in proper way
            alert(data.responseJSON.message);
        }
    });

    return false;
});
