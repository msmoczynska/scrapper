const generatePagination = function (data) {
    $("#pagination").html("");

    if (data.pagination.page > 1) {
        $("#pagination").append('<button class="page" value="1">First</button>');
        $("#pagination").append('<button class="page" value=' + (data.pagination.page - 1) + '>Previous</button>');
    } else {
        $("#pagination").append('<button class="page" value="1" disabled>First</button>');
        $("#pagination").append('<button class="page" value=' + (data.pagination.page - 1) + ' disabled>Previous</button>');
    }

    if (data.pagination.pages <= 9) {
        for (let i = 1; i <= data.pagination.pages; i++) {
            $("#pagination").append('<button class="page actual" value=' + i + '>' + i + '</button>');
        }
    }

    if (data.pagination.page <= 6 && data.pagination.pages > 9) {
        for (let i = 1; i <= 7; i++) {
            $("#pagination").append('<button class="page actual" value=' + i + '>' + i + '</button>');
        }
        $("#pagination").append('<button class="page" value="..." disabled>...</button>');
        $("#pagination").append('<button class="page" value=' + data.pagination.pages + '>' + data.pagination.pages + '</button>');
    }

    if (data.pagination.page > 6 && data.pagination.page < data.pagination.pages - 5 && data.pagination.pages > 9) {
        $("#pagination").append('<button class="page" value="1">1</button>');
        $("#pagination").append('<button class="page" value="..." disabled>...</button>');
        for (let i = data.pagination.page - 2; i <= data.pagination.page + 2; i++) {
            $("#pagination").append('<button class="page actual" value=' + i + '>' + i + '</button>');
        }
        $("#pagination").append('<button class="page" value="..." disabled>...</button>');
        $("#pagination").append('<button class="page" value=' + data.pagination.pages + '>' + data.pagination.pages + '</button>');
    }

    if (data.pagination.page >= data.pagination.pages - 5 && data.pagination.pages > 9) {
        $("#pagination").append('<button class="page" value="1">1</button>');
        $("#pagination").append('<button class="page" value="..." disabled>...</button>');
        for (let i = data.pagination.pages - 6; i <= data.pagination.pages; i++) {
            $("#pagination").append('<button class="page actual" value=' + i + '>' + i + '</button>');
        }
    }

    if (data.pagination.page < data.pagination.pages) {
        $("#pagination").append('<button class="page" value=' + (data.pagination.page + 1) + '>Next</button>');
        $("#pagination").append('<button class="page" value=' + data.pagination.pages + '>Last</button>');
    } else {
        $("#pagination").append('<button class="page" value=' + (data.pagination.page + 1) + ' disabled>Next</button>');
        $("#pagination").append('<button class="page" value=' + data.pagination.pages + ' disabled>Last</button>');
    }

    $(".actual[value=" + data.pagination.page + "]").attr("class", "actualPage");
}

export {
    generatePagination
}
