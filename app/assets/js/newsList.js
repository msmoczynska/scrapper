import {generatePagination} from "./utils/pagination";
import _ from "underscore";

const limit = 10;
const url = Routing.generate("api_news_list", {});

$(document).ready(function() {
    generateNewsListForPage(1);

    return false;
});

$(document).on("click", ".page", function() {
    generateNewsListForPage($(this).attr('value'));

    return false;
});

$(document).on('click', '#submit-search', function() {
    generateNewsListForPage(1);

    return false;
});

function generateNewsListForPage(page) {
    const search = $("input[name='search']").val();
    const query = {
        limit: limit,
        page: page,
    };

    if ("" !== search) {
        query["search"] = search;
    }

    // TODO: extract to a separate file
    $.ajax({
        url: url,
        type: 'get',
        data: $.param(query),
        processData: false,

        success: function (data) {
            generateNewsList(data);
            generatePagination(data);
        },
        error: function (data) {
            alert(data.responseJSON.message);
        }
    });

    return false;
}

function generateNewsList(data) {
    const newsRowTemplate = _.template($("#news-row").html());
    let newsOrdinalNumber = data.pagination.firstOrdinalNumber;

    $("#news-list-table-body").html("");
    for (let i = 0; i < data.items.length; i++) {
        const recentNews = (true === data.items[i].recent) ? "recent-news" : "";
        const outdatedNews = (false === data.items[i].enabled) ? "outdated-news" : "";
        const titleRowContent = prepareTooltips(data.items[i].title);

        $("#news-list-table-body").append(
            newsRowTemplate({
                recent: recentNews,
                outdated: outdatedNews,
                ordinal_number : newsOrdinalNumber,
                news_title: titleRowContent,
                news: data.items[i]
            })
        );

        $('[data-toggle="tooltip"]').tooltip();

        newsOrdinalNumber++;
    }
}

function prepareTooltips(title) {
    const newsTooltipTemplate = _.template($("#news-tooltip").html());
    if (title.length > 80) {
        return newsTooltipTemplate({ title: title });
    }

    return title;
}
